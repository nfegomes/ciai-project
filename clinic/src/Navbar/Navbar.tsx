import React, {ChangeEvent, FormEvent, useState} from "react";
import {connect} from "react-redux";
import {GlobalState} from "../App";
import {logout, requestLogout} from "../Login/actions";
import {Link} from "react-router-dom";
import {faSignOutAlt} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Navbar} from 'react-bootstrap';
import {fab} from "@fortawesome/free-brands-svg-icons";

const ProtoNavbar = (props: {
    isLoggedIn: boolean,
    requestLogout: () => void
}) => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    let submitHandler = () => {
        console.log('entrou no submit');
        props.requestLogout();
        setUsername("");
        setPassword("");
    };

    let navbar = (
        <div>
            <Navbar
                className="bg-light d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 border-bottom box-shadow">
                <Link to="/" className="my-0 mr-md-auto font-weight-normal">
                    <Navbar.Brand>Vet Clinic</Navbar.Brand></Link>
                <div className="my-2 my-md-0 mr-md-3">
                    {!props.isLoggedIn ?
                        <div>
                            <Link to="/login" className="btn darkbutton"
                                  style={{marginRight: 5}}>Login</Link>
                            <Link to="/signup" className="btn" id="signupbutton">Sign up</Link>
                        </div>
                        :
                        <Link to="/" className="btn loginbutton" onClick={() => submitHandler()}>
                            <FontAwesomeIcon icon={faSignOutAlt} size="lg"/><a className="far ml-1">Logout</a></Link>
                    }
                </div>
            </Navbar>
        </div>
    );

    return (<> {navbar} </>);
    // add a message space for alerts (you were signed out, expired session)
};
const mapStateToProps = (state: GlobalState) => ({isLoggedIn: state.login.isLoggedIn});
const mapDispatchToProps =
    (dispatch: any) =>
        ({
            requestLogout: () => dispatch(requestLogout())
        });
const FinalNavbar = connect(mapStateToProps, mapDispatchToProps)(ProtoNavbar);

export default FinalNavbar;




