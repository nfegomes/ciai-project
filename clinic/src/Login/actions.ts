import {Action} from "redux";
import {postData} from "../Utils/NetworkUtils";

import jwt from "jsonwebtoken";

export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const REQUEST_LOGIN = 'REQUEST_LOGIN';
export const RECEIVE_LOGIN = 'RECEIVE_LOGIN';

export interface LoginAction extends Action {
    token: string
    username: string
    role: string
}

export const login = (token: string, role: string, username: string) => ({
    type: LOGIN,
    token: token,
    role: role,
    username: username
});
export const logout = () => ({type: LOGOUT});

export const requestLogin = () => ({type: REQUEST_LOGIN});
export const receiveLogin = () => ({type: RECEIVE_LOGIN});

export function requestLoginD(username: string, password: string) {
    return (dispatch: any) => {
        dispatch(requestLogin);
        performLogin(username, password)
            .then(token => {

                if (token != null) {

                    let decodedJwt = jwt.decode((token as string).replace("Bearer ", ""));
                    console.log(decodedJwt);


                    dispatch(login(token as string, decodedJwt.role, decodedJwt.username))
                } else
                    dispatch(receiveLogin())
            })
    }
}

export function requestLogout() {
    return (dispatch: any) =>
        dispatch(logout());
}

async function performLogin(username: string, password: string) {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    return fetch("/login",
        {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify({username: username, password: password})
        })
        .then(response => {
            if (response.ok)
                return response.headers.get('Authorization');
            else {
                console.log(`Error: ${response.status}: ${response.statusText}`);
                return null;
                // and add a message to the Ui: wrong password ?? other errors?
            }
        })
        .catch(err => {
            console.log(err);
            return null
        })
}
