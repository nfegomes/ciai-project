import {Action} from "redux";
import {LOGIN, LOGOUT, LoginAction, REQUEST_LOGIN, RECEIVE_LOGIN} from "./actions";
import * as jwt from "jsonwebtoken";
import {LoginState} from "./index";

const secret = "este é um grande segredo que tem que ser mantido escondido"

function checkIfTokenIsValid() {
    return localStorage.getItem('jwt') != null;
}

const initialState = {isLoggedIn: checkIfTokenIsValid(), token: "", role: "", username: "", isFetching: true};

function loginReducer(state: LoginState = initialState, action: Action): LoginState {
    switch (action.type) {
        case LOGIN:
            let loginAction = (action as LoginAction);
            if (loginAction.token) {
                localStorage.setItem('jwt', loginAction.token);
                return {
                    ...state,
                    isLoggedIn: true,
                    token: loginAction.token,
                    role: loginAction.role,
                    username: loginAction.username,
                    isFetching: true
                };
            } else {
                return state;
            }
        case LOGOUT:
            localStorage.removeItem('jwt');
            console.log('entrou no logout');
            return {...state, isLoggedIn: false};

        case REQUEST_LOGIN:
            return {...state, isFetching: true};
        case RECEIVE_LOGIN:
            return {...state, isFetching: false};
        default:
            return state;
    }
}

export default loginReducer;
