import React, {ChangeEvent, FormEvent, useState} from "react";
import {connect} from "react-redux";
import {GlobalState} from "../App";
import {requestLoginD} from "./actions";
import {Redirect} from "react-router-dom";
import {changeHandlerString} from "../Utils/Handlers";
import {Alert, Button, Form, Row} from "react-bootstrap";

export interface LoginState {
    isLoggedIn: boolean,
    token: string,
    role: string,
    username: string,
    isFetching: boolean
}

const ProtoLoginForm = (props: {
    isLoggedIn: boolean,
    performLogin: (username: string, password: string) => void,
    performSignOut: () => void,
    isFetching: boolean
}) => {
    console.log(props.isLoggedIn,props.isFetching);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    let submitHandler = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        props.performLogin(username, password);
    };

    let errorMessage = <Alert variant="danger" className="mt-2 center-h" style={{width: "300px"}}>Wrong
        Credentials</Alert>;

    let loginForm = (
        <div className="text-center">

            <form onSubmit={submitHandler}>
                <div><label>Username: <input type="text" className="form-control" value={username}
                                             onChange={event => changeHandlerString(setUsername, event)}/></label></div>
                <div><label>Password: <input type="password" className="form-control" value={password}
                                             onChange={event => changeHandlerString(setPassword, event)}/></label></div>
                <button type="submit" className="btn btn-primary">Login</button>
            </form>
            {props.isLoggedIn && <Redirect to="/"/>} {/*to={"/user/"+username}*/}

            {(!props.isFetching && !props.isLoggedIn) ?
                errorMessage : <></>
            }
        </div>
    );

    return (<> {loginForm} </>);
    // add a message space for alerts (you were signed out, expired session)
};
const mapStateToProps = (state: GlobalState) => ({
    isLoggedIn: state.login.isLoggedIn,
    isFetching: state.login.isFetching
});
const mapDispatchToProps =
    (dispatch: any) =>
        ({
            performLogin: (username: string, password: string) => {
                dispatch(requestLoginD(username, password))
            }
        });
const LoginForm = connect(mapStateToProps, mapDispatchToProps)(ProtoLoginForm);

export default LoginForm;
