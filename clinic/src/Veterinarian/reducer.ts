import {Action} from "redux";
import {
    ADD_VET,
    AddVetAction,
    RECEIVE_VET,
    RECEIVE_VETS,
    ReceiveVetAction,
    ReceiveVetsAction,
    REQUEST_VET, REQUEST_VETS, REQUEST_FAILED
} from './actions';
import {Vet, VetState} from "./index";

const initialState = {
    vets: [],
    vet: {id: 0, email: '', username: '', picture: '', name: '', address: '', cellphone: 0, isFrozen: false},
    isFetching: false,
    formFailed:true
};

function vetReducer(state: VetState = initialState, action: Action): VetState {
    switch (action.type) {
        case ADD_VET:
            let addVetAction = (action as AddVetAction);
            if(addVetAction.type) {
                return {...state, formFailed: false, vets: state.vets.concat(addVetAction.data)};
            } else {
                return state;
            }
        case REQUEST_VET:
            return {...state, isFetching: true};
        case REQUEST_VETS:
            return {...state, isFetching: true};
        case RECEIVE_VET:
            let receiveVetAction = (action as ReceiveVetAction);
            return {...state, isFetching: false, vet: receiveVetAction.data};
        case RECEIVE_VETS:
            let receiveVetsAction = (action as ReceiveVetsAction);
            return {...state, isFetching: false, vets: receiveVetsAction.data};
        case REQUEST_FAILED:
            return {...state, formFailed:true};
        default:
            return state
    }
}

export default vetReducer