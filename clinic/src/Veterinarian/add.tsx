import React, {FormEvent, useState} from "react";
import {connect} from "react-redux";
import {addVetDis} from "./actions";
import {GlobalState} from "../App";
import {LoginInfo, Vet} from "./index";
import {changeHandlerNumber, changeHandlerString} from "../Utils/Handlers";
import {Link, Redirect} from "react-router-dom";
import {Alert} from "react-bootstrap";


const ProtoAddVet = (props: { addVet: (vet: Vet, login: LoginInfo) => void, formFailed:boolean, isFetching: boolean }) => {
    const [submited, setSubmited] = useState(false);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("@email.com");
    const [name, setName] = useState("");
    const [picture, setPicture] = useState("");
    const [cellphone, setCellphone] = useState(96223344556);
    const [address, setAddress] = useState("Rua da Faculdade");

    // eslint-disable-next-line
    let submitHandler = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        let login: LoginInfo = {
            username: username,
            password: password,
            email: email

        };

        let vet: Vet = {
            id: 0,
            username: username,
            name: name,
            picture: picture,
            cellphone: cellphone,
            address: address,
            isFrozen: false
        };

        props.addVet(vet, login);
        setSubmited(true);

        setUsername('');
        setPassword('');
        setEmail('@email.com');
        setName('');
        setPicture('');
        setCellphone(96223344556);
        setAddress('Rua da Faculdade');
    };

    let errorMessage = <Alert variant="danger" className="center-h text-center" style={{width: "300px"}}>Wrong
        Credentials</Alert>;

    return (
        <div>
            <h4 id="form-title" className="border-bottom border-gray pb-2 mb-5">Fill the form to add a new
                veterinarian</h4>
            <div id="content">
                <form onSubmit={submitHandler} id="form-left">
                    <div className="form-row mb-3">
                        <div className="col-md-4 mb-3">
                            <label htmlFor="validationDefault01">Username</label>
                            <input type="text" className="form-control"
                                   placeholder="Username" required value={username}
                                   onChange={event => changeHandlerString(setUsername, event)}/>
                        </div>
                        <div className="col-md-4 mb-3">
                            <label htmlFor="validationDefault01">Password</label>
                            <input type="password" className="form-control"
                                   placeholder="Password" required value={password}
                                   onChange={event => changeHandlerString(setPassword, event)}/>
                        </div>
                        <div className="col-md-4 mb-3">
                            <label htmlFor="validationDefault01">Email</label>
                            <input className="form-control" placeholder="Email" type="text"
                                   value={email} required
                                   onChange={(event) => changeHandlerString(setEmail, event)}/>
                        </div>
                    </div>

                    <div className="form-row mb-3">
                        <div className="col-md-9 mb-3">
                            <label htmlFor="validationDefault01">Name</label>
                            <input className="form-control" placeholder="Name" type="text"
                                   value={name} required
                                   onChange={(event) => changeHandlerString(setName, event)}/>
                        </div>
                        <div className="col-md-3 mb-3">
                            <label htmlFor="validationDefault01">Cellphone</label>
                            <input className="form-control" placeholder="Cellphone" type="number"
                                   value={cellphone} required min={9}
                                   onChange={(event) => changeHandlerNumber(setCellphone, event)}/>
                        </div>
                    </div>

                    <div className="form-row mb-3">
                        <label htmlFor="validationDefault01">Picture</label>
                        <input className="form-control" placeholder="Picture" type="text" required
                               value={picture} onChange={(event) => changeHandlerString(setPicture, event)}/>
                    </div>

                    <div className="form-row mb-3">
                        <label htmlFor="validationDefault01">Address</label>
                        <input className="form-control" placeholder="Picture" type="text" required
                               value={address} onChange={(event) => changeHandlerString(setAddress, event)}/>
                    </div>

                    <div className="form-row justify-content-center mt-4 mb-3">
                        <button className="btn btn-primary" type="submit">Submit form</button>
                        {console.log('fetching', props.isFetching, ' failed', props.formFailed)}
                    </div>
                    {!props.formFailed && submited && <Redirect to="/"/>} {/*to={"/user/"+username}*/}

                    {(submited && props.formFailed) ?
                        errorMessage : <></>
                    }
                </form>
            </div>
            <small className="d-block text-center mt-3 center-h" id="vet-form-leftPlus">
                <Link to="/profile">Go Back</Link>
            </small>
        </div>
    );
};

const mapStateToProps = (state: GlobalState) => ({isFetching: state.vet.isFetching, formFailed:state.vet.formFailed});
const mapDispatchToProps = (dispatch: any) => ({
    addVet: (vet: Vet, login: LoginInfo) => {
        dispatch(addVetDis(vet, login))
    }
});
export const AddVet = connect(mapStateToProps, mapDispatchToProps)(ProtoAddVet);
