import React, {useEffect} from "react";
import {connect} from "react-redux";
import {fetchVets} from "./actions";
import {GlobalState} from "../App";
import {Link} from "react-router-dom";
import {Employee} from "../Employees";
import {Col, Container, Image, ListGroupItem, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrash} from "@fortawesome/free-solid-svg-icons";

export interface Vet {
    id: number,
    username: string,
    name: string,
    picture: string,
    cellphone: number,
    address: string,
    isFrozen: boolean
}

export interface LoginInfo {
    username: string,
    password: string,
    email: string
}

export interface VetState {
    vets: Vet[],
    vet: Vet,
    isFetching: boolean,
    formFailed: boolean
}


const ProtoVetList = (props: { vets: Vet[], loadVet: () => void, isFetching: boolean }) => {
    // eslint-disable-next-line
    useEffect(() => props.loadVet(), []);

    console.log(props.vets);

    return <>
        {props.vets.length > 0 ?
            <>
                <div className="text-center">
                    <Link to={"/vet/add"}>
                        <button className="btn btn-primary">Add Veterinarian</button>
                    </Link>

                    <Link to={"/schedule"}>
                        <button className="btn btn-info ml-4">Manage Schedules</button>
                    </Link>
                </div>
                <div className="album py-4">
                    <div className="container">
                        <div className="row">
                            {props.vets.map((vet: Employee) =>
                                <ListGroupItem key={vet.id} as={Container}>
                                    <Row>
                                        <Col sm={2} className="center-v center-h">
                                            <Image className="card-image" roundedCircle width="68"
                                                   height="68" src={vet.picture == "" ? "" : vet.picture}
                                                   alt=""/>
                                        </Col>
                                        <Col className="center-v">
                                            <strong
                                                className="d-block text-gray-dark">&nbsp;{vet.name.toUpperCase()}</strong>
                                        </Col>
                                        <Col className="center-v">
                                            <u>Cellphone:</u>&emsp;&emsp;{vet.cellphone}<br/>
                                            <u>Address:</u>&emsp;&emsp;&emsp;{vet.address}
                                        </Col>
                                        <Col sm={1} className="center-v">
                                            {/*TODO onClick={() => props.deletePet(vet.id)}*/}
                                            <FontAwesomeIcon
                                                className="darkbutton" icon={faTrash} size="lg"/>
                                        </Col>
                                    </Row>
                                </ListGroupItem>
                            )}
                        </div>
                    </div>
                </div>
            </>
            :
            <></>
        }
    </>;
};

const mapStateToProps = (state: GlobalState) => ({
    vets: state.vet.vets,
    isFetching: state.schedules.isFetching
});
const mapDispatchToProps = (dispatch: any) => ({
    loadVet: () => {
        dispatch(fetchVets())
    }
});
export const VetsList = connect(mapStateToProps, mapDispatchToProps)(ProtoVetList);
