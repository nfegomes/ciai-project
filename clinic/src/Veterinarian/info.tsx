import React, {ChangeEvent, FormEvent, useEffect, useState} from 'react';
import {GlobalState} from "../App";
import {connect} from "react-redux";
import {fetchVet, fetchVets} from "./actions";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faSignOutAlt} from "@fortawesome/free-solid-svg-icons";
import {Container, Navbar, Image, Row, Col} from "react-bootstrap";
import {Vet} from "./index";

const VetInfo = (props: { vet: Vet }) =>
    <>
        <Navbar
            className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 border-bottom box-shadow">
            <Container>
                <Row className="align-items-center">
                    <Col>
                        <Image className="card-image" roundedCircle
                               src={props.vet.picture}
                               width="68" height="68"/>
                    </Col>
                    <Col className="align-items-center">
                        <Navbar.Brand className="">Welcome, {props.vet.name.toUpperCase()}!</Navbar.Brand>
                    </Col>
                    {/*<Link to="/editProfile" className="btn pull-right" id="signupbutton">*/}
                        {/*<div><FontAwesomeIcon icon={faEdit}/></div>*/}
                    {/*</Link>*/}
                </Row>
                <Navbar.Brand className="mb-0 lh-100 font-weight-bold">Dashboard</Navbar.Brand>
            </Container>
        </Navbar>
    </>;


const ProtoVetInfo = (props: { username: string, vet: Vet, loadVet: (username: string) => void }) => {
    useEffect(() => props.loadVet(props.username), []);
    console.log('o username e', props.username);
    console.log(props.vet);

    /* const [client, setClient] = useState(null as unknown as Client);
     const editClient = (user: Client) => {
         props.editUser(user);
         setClient(user);
     };*/

    return <>
        <VetInfo vet={props.vet}/>
    </>;
};


const mapStateToProps = (state: GlobalState) => ({username: state.login.username, vet: state.vet.vet});
const mapDispatchToProps = (dispatch: any) => ({
    loadVet: (username: string) => {
        dispatch(fetchVet(username))
    }
});
export const VetInfoComp = connect(mapStateToProps, mapDispatchToProps)(ProtoVetInfo);