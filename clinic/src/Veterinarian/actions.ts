import {Action} from "redux";
import {LoginInfo, Vet} from "./index";
import {getData, postData} from "../Utils/NetworkUtils";

export const ADD_VET = 'ADD_VET';
export const REQUEST_VET = 'REQUEST_VET';
export const REQUEST_VETS = 'REQUEST_VETS';
export const RECEIVE_VET = 'RECEIVE_VET';
export const RECEIVE_VETS = 'RECEIVE_VETS';
export const REQUEST_FAILED = 'REQUEST_FAILED';

export interface AddVetAction extends Action {
    data: Vet
}

export interface ReceiveVetsAction extends Action {
    data: Vet[]
}

export interface ReceiveVetAction extends Action {
    data: Vet
}

export const addVet = (vet: Vet) => ({type: ADD_VET, data: vet});

export const requestVet = () => ({type: REQUEST_VET});
export const requestVets = () => ({type: REQUEST_VETS});
export const receiveVet = (vet: Vet[]) => ({type: RECEIVE_VET, data: vet});
export const receiveVets = (vet: Vet[]) => ({type: RECEIVE_VETS, data: vet});
export const requestFailed = () => ({type:REQUEST_FAILED});


export function fetchVets() {
    return (dispatch: any) => {
        dispatch(requestVets());

        getData('/vets/', [])
            .then(data => {
                data && dispatch(receiveVets(data))
            })
    }
}

export function fetchVet(username: string) {
    return (dispatch: any) => {
        dispatch(requestVet());
        return getData('/vets/' + username, [])
            .then(data => {
                data && dispatch(receiveVet(data));
                }
            )
    }
}

export function addVetDis(vet: Vet, login: LoginInfo) {
    return (dispatch: any) => {

        postData('/users/signup', "", JSON.stringify({
            "username": login.username,
            "password": login.password,
            "email": login.email
        })).then(
            result => {
                if (result != "")
                    postData('/vets/', "", JSON.stringify({
                        "id": 0,
                        "username": vet.username,
                        "name": vet.name,
                        "picture": vet.picture,
                        "cellphone": vet.cellphone,
                        "address": vet.address,
                        "isFrozen": false
                    })).then(
                        result => {
                            console.log(result);
                            if(result != "") {
                                dispatch(addVet(vet));
                            }else{
                                dispatch(requestFailed());
                            }
                        }
                    );
                else
                    dispatch(requestFailed());

            }
        );


    }
}