import React from 'react';
import {connect, Provider} from 'react-redux';
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom';
import {applyMiddleware, createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import {load, save} from 'redux-localstorage-simple';
import reduxLogger from 'redux-logger';
import thunk from 'redux-thunk';
import './App.css';
import rootReducer from './rootReducer';
import {EmployeeList, EmployeeState} from "../Employees";
import {FilteredPetList, PetState} from '../Pets';
import LoginForm, {LoginState} from "../Login";
import {AppointmentState, FilteredAppointmentList} from "../Appointments";
import {AddPetForm} from "../Pets/addPet";
import {FilteredScheduleList, ScheduleState} from "../Schedules";
import FinalNavbar from "../Navbar/Navbar";
import SignUpForm from "../SignUp";
import {ClientInfoComp, ClientState} from "../Client";
import {AddAppointmentForm} from "../Appointments/addApt";
import {VetsList, VetState} from "../Veterinarian";
import {AddVet} from "../Veterinarian/add";
import {AdminInfoComp, AdminState} from "../Admin";
import {VetInfoComp} from "../Veterinarian/info";

export interface GlobalState {
    login: LoginState,
    pets: PetState,
    employees: EmployeeState,
    appointments: AppointmentState,
    schedules: ScheduleState,
    client: ClientState,
    vet: VetState,
    admin: AdminState
}

const signInRoute = (props: { token: string, role: string, isFetchingVet: boolean }) => {
    return (<>
            <Route exact path="/">
                <Redirect to="/profile"/>
            </Route>
            <Route path="/profile">
                {ProfilePage({token: props.token, role: props.role, isFetchingVet: props.isFetchingVet})}
            </Route>

            <Route exact path="/pets" component={FilteredPetList}/>
            <Route exact path="/addpet" component={AddPetForm}/>
            <Route exact path="/vet" component={VetsList}/>
            <Route exact path="/vet/add" component={AddVet}/>
            <Route exact path="/schedule" component={FilteredScheduleList}/>
            <Route path="/addAppointment" component={AddAppointmentForm}/>
        </>
    );
};
const ProfilePage = (props: { token: string, role: string, isFetchingVet: boolean }) => {


    const ClientProfile = () => {
        return (<>
            <ClientInfoComp/>
            <div className="container">
                <div className="row">
                    <div className="col-sm">
                        <FilteredPetList isClient={true}/>
                    </div>
                    <div className="col-sm">
                        <FilteredAppointmentList isClient={true}/>
                    </div>
                </div>
            </div>
        </>)
    };

    const AdminProfile = () => {
        return (<>
            <AdminInfoComp/>
            <div className="container">
                <VetsList/>
            </div>
        </>)
    };

    const VetProfile = () => {
        return (<>
            <VetInfoComp/>
            <div className="container">
                {!props.isFetchingVet && <div className="row">
                    <div className="col-sm">
                        <FilteredPetList isClient={false}/>
                    </div>
                    <div className="col-sm">
                        <FilteredAppointmentList isClient={false}/>
                    </div>
                </div>}
            </div>
        </>)
    };


    return (<>
            {props.role == "ROLE_CLIENT" && ClientProfile()}
            {props.role == "ROLE_ADMIN" && AdminProfile()}
            {props.role == "ROLE_VET" && VetProfile()}
        </>

    )
};

const ProtoPage = (props: { isLoggedIn: boolean, token: string, role: string, isFetchingVet: boolean }) => {
    return (
        <Router>
            <div className="App">
                <Route exact component={FinalNavbar}/>
                <Switch>
                    {!props.isLoggedIn && <Route exact path="/" component={EmployeeList}/>}

                    <Route exact path="/login" component={LoginForm}/>
                    <Route exact path="/signup" component={SignUpForm}/>

                    {/*if is logged in it shows the follow*/}
                    {props.isLoggedIn && signInRoute({
                        token: props.token,
                        role: props.role,
                        isFetchingVet: props.isFetchingVet
                    })}
                </Switch>
            </div>
        </Router>);
};
const mapStateToProps = (state: GlobalState) => ({
    isLoggedIn: state.login.isLoggedIn,
    token: state.login.token,
    role: state.login.role,
    isFetchingVet: state.vet.isFetching
});
const Page = connect(mapStateToProps)(ProtoPage);

const middleware = [reduxLogger, thunk];

const store = createStore(rootReducer, load(), composeWithDevTools(applyMiddleware(...middleware, save())));

const App = () => (
    <Provider store={store}>
        <Page/>
    </Provider>
);

export default App;
