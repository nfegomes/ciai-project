import {combineReducers} from 'redux';
import appointmentReducer from '../Appointments/reducers';
import employeeReducer from '../Employees/reducer';
import petReducer from '../Pets/reducers';
import loginReducer from "../Login/reducers";
import scheduleReducer from "../Schedules/reducers";
import clientReducer from "../Client/reducers";
import vetReducer from "../Veterinarian/reducer";
import adminReducer from "../Admin/reducers";

const rootReducer = combineReducers({
    login: loginReducer,
    pets: petReducer,
    employees: employeeReducer,
    appointments: appointmentReducer,
    schedules: scheduleReducer,
    client: clientReducer,
    vet: vetReducer,
    admin: adminReducer
});

export default rootReducer;
