import React, {FormEvent, useState} from "react";
import {connect} from "react-redux";
import {GlobalState} from "../App";
import {requestSignUp} from "./actions";
import {Link, Redirect} from "react-router-dom";
import {changeHandlerNumber, changeHandlerString} from "../Utils/Handlers";

export interface UserData {
    name: string,
    picture: string,
    cellphone: number,
    address: string,
    email: string,
    username: string,
    password: string
}

const ProtoSignUpForm = (
    props: {
        performSignUp: (userData: UserData) => void
    }) => {

    const [name, setName] = useState("");
    const [picture, setPicture] = useState("");
    const [cellphone, setCellphone] = useState(961234567);
    const [address, setAddress] = useState("");
    const [email, setEmail] = useState("@email.com");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [goBack, setGoBack] = useState(false);


    let submitHandler = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        let userdata: UserData = {
            name: name,
            picture: picture,
            cellphone: cellphone,
            address: address,
            email: email,
            username: username,
            password: password
        };
        props.performSignUp(userdata);
        setUsername("");
        setPassword("");
        setEmail(email);
        setGoBack(true);
    };

    let signUpForm =
        (<div>
            <h4 id="form-title" className="border-bottom border-gray pb-2 mb-5">Fill the form in order to register</h4>
            <div id="content">
                <form onSubmit={submitHandler}>
                    <div className="form-row mb-3">
                        <div className="col-md-3 mb-3">
                            <label htmlFor="validationDefault01">Username</label>
                            <input type="text" className="form-control"
                                   placeholder="Username" required value={username}
                                   onChange={event => changeHandlerString(setUsername, event)}/>
                        </div>
                        <div className="col-md-3 mb-3">
                            <label htmlFor="validationDefault01">Password</label>
                            <input type="password" className="form-control"
                                   placeholder="Password" required value={password}
                                   onChange={event => changeHandlerString(setPassword, event)}/>
                        </div>
                        <div className="col-md-3 mb-3">
                            <label htmlFor="validationDefault01">Email</label>
                            <input className="form-control" placeholder="Email" type="text"
                                   value={email} required
                                   onChange={(event) => changeHandlerString(setEmail, event)}/>
                        </div>
                        <div className="col-md-3 mb-3">
                            <label htmlFor="validationDefault01">Cellphone</label>
                            <input className="form-control" placeholder="Cellphone" type="number"
                                   value={cellphone} required minLength={9} min={0}
                                   onChange={(event) => changeHandlerNumber(setCellphone, event)}/>
                        </div>
                    </div>

                    <div className="form-row mb-3">
                        <div className="col-md-6 mb-3">
                            <label htmlFor="validationDefault01">Name</label>
                            <input type="text" className="form-control"
                                   placeholder="Name" required value={name}
                                   onChange={event => changeHandlerString(setName, event)}/>
                        </div>
                        <div className="col-md-6 mb-3">
                            <label htmlFor="validationDefault01">Address</label>
                            <input type="text" className="form-control"
                                   placeholder="Address" required value={address}
                                   onChange={event => changeHandlerString(setAddress, event)}/>
                        </div>
                    </div>

                    <div className="form-row mb-3">
                        <div className="col-md-12 mb-3">
                            <label htmlFor="validationDefault01">Picture</label>
                            <input type="text" className="form-control"
                                   placeholder="Picture" required value={picture}
                                   onChange={event => changeHandlerString(setPicture, event)}/>
                        </div>
                    </div>

                    <div className="form-row justify-content-center mt-4">
                        <button className="btn btn-primary" type="submit">Submit form</button>
                    </div>
                </form>
            </div>
            <small className="d-block text-center mt-3">
                <Link to="/">Go Back</Link>
            </small>
            {goBack && <Redirect to={"/login"}/>}
        </div>);

    return <div>{signUpForm}</div>;
    // add a message space for alerts (you were signed out, expired session)
};
const mapStateToProps = (state: GlobalState) => ({isSignedIn: state.login.isLoggedIn});
const mapDispatchToProps =
    (dispatch: any) =>
        ({
            performSignUp: (userdata: UserData) => {
                dispatch(requestSignUp(userdata))
            },
        });
const SignUpForm = connect(mapStateToProps, mapDispatchToProps)(ProtoSignUpForm);

export default SignUpForm

