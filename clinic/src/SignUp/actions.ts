import {Action} from "redux";
import {UserData} from "./index";

export const SIGN_IN = 'SIGN_IN';
export const SIGN_OUT = 'SIGN_OUT';

export interface SignUpAction extends Action { data:string | null }

export const signUp = (token:string|null) => ({type:SIGN_IN, data:token});
export const signOut = () => ({type:SIGN_OUT});

export function requestSignUp(userdata: UserData)  {
    return (dispatch:any) =>
        performSignUp(userdata)
            .then(token => dispatch(signUp(token)))
}

async function performSignUp(userdata: UserData) {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    return fetch("/signup",
        {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify({
                name: userdata.name,
                picture: userdata.picture,
                cellphone: userdata.cellphone,
                address: userdata.address,
                email: userdata.email,
                username: userdata.username,
                password: userdata.password
            })
        })
        .then(response => {
            if (response.ok)
                return response.headers.get('Authorization');
            else {
                console.log(`Error: ${response.status}: ${response.statusText}`);
                return null;
                // and add a message to the Ui: wrong password ?? other errors?
            }
        })
        .catch(err => {
            console.log(err);
            return null
        })
}