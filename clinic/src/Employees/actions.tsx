import {getData} from "../Utils/NetworkUtils";
import {Action} from "redux";
import {Employee} from "./index";

export const REQUEST_EMPLOYEES = 'REQUEST_EMPLOYEES';
export const RECEIVE_EMPLOYEES = 'RECEIVE_EMPLOYEES';

export interface ReceiveEmployeeAction extends Action {
    data: Employee[]
}

export const requestEmployees = () => ({type: REQUEST_EMPLOYEES});
export const receiveEmployees = (data: Employee[]) => ({type: RECEIVE_EMPLOYEES, data: data});


export function fetchEmployees() {
    return (dispatch: any) => {
        dispatch(requestEmployees());

        getData(`/employees/`, [])
            .then(data => {
                data && dispatch(receiveEmployees(data))
            });
    }
}