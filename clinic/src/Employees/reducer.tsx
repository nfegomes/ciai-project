import {Action} from "redux";
import {
    RECEIVE_EMPLOYEES,
    REQUEST_EMPLOYEES,
    ReceiveEmployeeAction
} from './actions';
import {EmployeeState} from "./index";

const initialState = {
    employees: [],
    isFetching: false,
};

function employeeReducer(state: EmployeeState = initialState, action: Action): EmployeeState {
    switch (action.type) {
        case REQUEST_EMPLOYEES:
            return {...state, isFetching: true};
        case RECEIVE_EMPLOYEES:
            return {...state, isFetching: false, employees: (action as ReceiveEmployeeAction).data};
        default:
            return state
    }
}

export default employeeReducer;
