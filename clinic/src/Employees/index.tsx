import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import Header from "../Header";
import {GlobalState} from "../App";
import {fetchEmployees} from "./actions";

export interface Employee {
    id: number;
    username: string;
    name: string;
    picture: string;
    address: string;
    cellphone: number;
}

export interface EmployeeState {
    employees: Employee[],
    isFetching: boolean
}

const ProtoEmployeeList = (props: { employees: Employee[], loadEmployees: () => void }) => {
    useEffect(() => props.loadEmployees(), []);
    return (<>
        <div>
            <Header/>
            <h2> Our Team </h2>
            <div className="album py-5 bg-light">
                <div className="container">
                    <div className="row">
                        {props.employees.map((employee: Employee) =>
                            <>
                                {/*<Link to={`/${employee.username}`}>*/}
                                <div className="col-md-4">
                                    <div className="card mb-4 box-shadow">
                                        <img className="card-img-top card-image" src={employee.picture}
                                             height="200" width="100" alt="Card cap"/>
                                        <div className="card-body">
                                            <p className="card-text"> {employee.name}</p>
                                        </div>
                                    </div>
                                </div>
                                {/*</Link>*/}
                            </>
                        )}
                    </div>
                </div>
            </div>
        </div>
    </>);
};

const mapStateToProps = (state: GlobalState) => ({employees: state.employees.employees});
const mapDispatchToProps = (dispatch: any) => ({
    loadEmployees: () => {
        dispatch(fetchEmployees())
    }
});
export const EmployeeList = connect(mapStateToProps, mapDispatchToProps)(ProtoEmployeeList);


/*{ this.state.listFilter == 'all' ?*/
/*   : <div></div> }*/
/*   <div className="d-flex flex-row">
    <div className="btn-group p-2 ml-auto p-2" role="group" aria-label="Basic example">
       <button type="button" onClick={this.onClickAll} className="btn btn-primary">All</button>
       <button type="button" onClick={this.onClickAdmins} className="btn btn-secondary">Admins</button>
      <button type="button" onClick={this.onClickVets} className="btn btn-secondary">Vets</button>
   </div>
  </div>*/