import {deleteData, getData, postData, updateData} from "../Utils/NetworkUtils";
import {Action} from "redux";
import {Appointment} from "./index";
import {DELETE_PET, deletePet, EDIT_PET, editPet} from "../Pets/actions";
import {Pet} from "../Pets";

export const ADD_APPOINTMENT = 'ADD_APPOINTMENT';
export const REQUEST_APPOINTMENTS = 'REQUEST_APPOINTMENTS';
export const RECEIVE_APPOINTMENTS = 'RECEIVE_APPOINTMENTS';
export const DELETE_APPOINTMENT = 'DELETE_APPOINTMENT';
export const EDIT_APPOINTMENT = 'EDIT_APPOINTMENT';

export interface AddAppointmentAction extends Action {
    data: Appointment
}

export interface ReceiveAppointmentAction extends Action {
    data: Appointment[]
}

export interface DeleteAppointmentAction extends Action {
    data: number
}

export const addAppointment = (appointment: Appointment) => ({type: ADD_APPOINTMENT, data: appointment});
export const requestAppointments = () => ({type: REQUEST_APPOINTMENTS});
export const receiveAppointments = (data: Appointment[]) => ({type: RECEIVE_APPOINTMENTS, data: data});
export const deleteAppointment = (aptId: number) => ({type: DELETE_APPOINTMENT, data: aptId});
export const editAppointment = (apt: Appointment) => ({type: EDIT_APPOINTMENT, data: apt});


export function fetchAppointmentsOfVetActive(id:number) {
    return (dispatch: any) => {
        dispatch(requestAppointments());

        return getData('vets/'+id+'/appointments/active', [])
            .then(data => {
                data && dispatch(receiveAppointments(data))
            })
    }
}

export function fetchAppointmentsOfUser(username:string) {
    return (dispatch: any) => {
        dispatch(requestAppointments());

        return getData('clients/'+username+'/appointments', [])
            .then(data => {
                data && dispatch(receiveAppointments(data))
            })
    }
}

export function deleteAptDis(aptId: number) {
    return (dispatch: any) => {
        dispatch(deleteAppointment(aptId));
        return deleteData("/appointments/" + aptId);
    }
}

export function addAppointmentDis(appointment: Appointment) {
    return (dispatch: any) => {
        postData('/appointments/', "", JSON.stringify({
            "id": 0,
            "petId": appointment.petId,
            "shiftId": appointment.shiftId,
            "vetId": appointment.vetId,
            "startingTime": appointment.startingTime,
            "endingTime": appointment.endingTime,
            "description": appointment.description,
            "duration": appointment.duration,
            "completed": false //always false while adding a new one
        })).then(
            result => {
                console.log(result);
               // dispatch(fetchAppointments());
            }
        );

        dispatch(addAppointment(appointment));
    }
}


export function completeAptDis(appointment: Appointment, description: string) {
    return (dispatch: any) => {
        dispatch(editAppointment(appointment));
        console.log(appointment);
        return updateData("vets/" + appointment.vetId + "/appointments/" + appointment.id, "",
            description
        ).then(result =>
            dispatch(fetchAppointmentsOfVetActive(appointment.vetId))
        )
    }
}