import {Action} from "redux";
import {ADD_APPOINTMENT, AddAppointmentAction, RECEIVE_APPOINTMENTS, ReceiveAppointmentAction, REQUEST_APPOINTMENTS} from './actions';
import {AppointmentState} from "./index";
import {AddPetAction} from "../Pets/actions";

const initialState = {
    appointments: [],
    isFetching:false,
};

function appointmentReducer(state:AppointmentState = initialState, action:Action):AppointmentState {
    switch (action.type) {
        case ADD_APPOINTMENT:
            return {...state, appointments: [...state.appointments, (action as AddAppointmentAction).data]};
        case REQUEST_APPOINTMENTS:
            return {...state, isFetching:true};
        case RECEIVE_APPOINTMENTS:
            return {...state, isFetching:false, appointments: (action as ReceiveAppointmentAction).data};
        default:
            return state
    }
}

export default appointmentReducer