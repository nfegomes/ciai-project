import React, {ChangeEvent, Component, useEffect, useState} from "react";
import {connect} from "react-redux";
import {
    completeAptDis,
    deleteAptDis,
    fetchAppointmentsOfUser,
    fetchAppointmentsOfVetActive
} from "./actions";
import {GlobalState} from "../App";
import {Link} from "react-router-dom";
import {faEdit, faPlusSquare} from "@fortawesome/free-solid-svg-icons";
import {faCalendar, faCalendarCheck} from "@fortawesome/free-regular-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


import {Col, Container, Image, ListGroup, ListGroupItem, Row} from "react-bootstrap";
import {Pet} from "../Pets";
import {faCalendarAlt} from "@fortawesome/free-regular-svg-icons/faCalendarAlt";
import {changeHandlerString} from "../Utils/Handlers";
import {Client} from "../Client";
import {Vet} from "../Veterinarian";

export interface Appointment {
    id: number,
    petId: number,
    shiftId: number,
    vetId: number,
    startingTime: Date,
    endingTime: Date,
    description: string,
    duration: number,
    completed: boolean
}

export interface AppointmentState {
    appointments: Appointment[],
    isFetching: boolean
}

const DateToString = (props: { date: Date }) => {
    let dateS = "" + props.date;
    console.log(dateS);
    let year = dateS.slice(0, 4);
    let month = dateS.slice(5, 7);
    let day = dateS.slice(8, 10);
    let hour = dateS.slice(11, 13);
    let min = dateS.slice(14, 16);

    return <>
        <strong>{day}/{month}/{year}</strong><br/>
        <a>{hour}h{min}</a>
    </>;

};


const NewComponent = (props: { appointment: Appointment, client: boolean, completeApt: (aptId: Appointment, description: string) => void }) => {

    const [description, setDescription] = useState("");
    return <ListGroupItem as={Container}>
        <Row className="text-center bg-info text-light " style={{borderRadius: "7px"}}>
            <Col>
                <DateToString date={props.appointment.startingTime}/>
            </Col>
            <Col className="border-dark bg-white p-2 text-dark" style={{
                borderRight: "4px solid",
                borderLeft: "4px solid",
                borderBottom: "4px solid",
                borderRadius: "7px"
            }}>
                {props.appointment.completed && <FontAwesomeIcon icon={faCalendarCheck} size="2x"/>}
                {!props.appointment.completed && <FontAwesomeIcon icon={faCalendarAlt} size="2x"/>}
            </Col>
            <Col>
                <DateToString date={props.appointment.endingTime}/>
            </Col>
        </Row>
        <Row>
            <Col>
                <div className="">
                    <strong>Pet: </strong> <span>{props.appointment.petId}</span>
                </div>
                <div className="">
                    <strong>Vet: </strong> <span>{props.appointment.vetId}</span>
                </div>
            </Col>
            <Col sm={4}>
                <h2>{props.appointment.duration.toPrecision(4)}H</h2>
            </Col>
        </Row>
        <Row>
            <Col className="border-top box-shadow">
                {props.appointment.completed && <p>{props.appointment.description}</p>}
                {!props.appointment.completed && !props.client && <textarea className="form-control"
                                                                            rows={7}
                                                                            placeholder="Description"
                                                                            value={description}
                                                                            onChange={event => changeHandlerString(setDescription, event)}/>}
            </Col>
            {!props.appointment.completed && !props.client && <Col sm={4}>
                <button onClick={() => props.completeApt(props.appointment, description)}>
                    set as completed
                </button>
            </Col>}
        </Row>
    </ListGroupItem>;

};

const AppointmentList = (props: {
    appointments: Appointment[], isFetching: boolean,
    completeApt: (aptId: Appointment, description: string) => void, isClient: boolean
}) => {


    return <div className="my-3 p-3 bg-white box-shadow ">
        <div>
            <div className="border-bottom border-gray d-flex justify-content-between mb-2">
                <h6 className="d-inline font-weight-bold">Upcoming Appointments</h6>
                {props.isClient &&
                <Link to="/addAppointment" className="d-inline"><FontAwesomeIcon className="lightbutton"
                                                                                 icon={faPlusSquare}
                                                                                 size="lg"/></Link>}
            </div>
        </div>
        <ListGroup className="center-h" style={{
            maxWidth: '750px',
            minWidth: '350px',

        }}>
            {console.log('o pets e', props.appointments)}
            {!props.isFetching && props.appointments.map((appointment: Appointment) =>
                <NewComponent key={appointment.id} appointment={appointment} client={props.isClient}
                              completeApt={props.completeApt}
                />
            )}
        </ListGroup>
    </div>
}

const ProtoFilteredAppointmentList = (props: {
    appointments: Appointment[], isFetching: boolean, loadAppointmentsOfClient: (username: string) => void, loadAppointmentsOfVet: (id: number) => void,
    completeApt: (apt: Appointment, description: string) => void, isClient: boolean, username: string, vet: Vet
}) => {
    const [filter, setFilter] = useState("");
    let handle = (e: ChangeEvent<HTMLInputElement>) => setFilter(e.target.value);
    // eslint-disable-next-line

    useEffect(() => {
        if (props.isClient)
            props.loadAppointmentsOfClient(props.username);
        else
            props.loadAppointmentsOfVet(props.vet.id);
    }, ["ola"]);

    return <>
        <AppointmentList appointments={props.appointments} isFetching={props.isFetching} completeApt={props.completeApt}
                         isClient={props.isClient}/>
        {/*<input onChange={handle} value={filter}/>*/}
    </>;
};

//TODO deixei o filtro aqui para o caso de termos tempo de o meter na pagina

const mapStateToProps = (state: GlobalState, props: OwnProp) => ({
    appointments: state.appointments.appointments,
    isFetching: state.appointments.isFetching,
    isClient: props.isClient,
    username: state.login.username,
    vet: state.vet.vet,
});
const mapDispatchToProps = (dispatch: any) => ({

    loadAppointmentsOfClient: (username: string) => {
        dispatch(fetchAppointmentsOfUser(username))
    },
    loadAppointmentsOfVet: (id: number) => {
        dispatch(fetchAppointmentsOfVetActive(id))
    },
    completeApt: (apt: Appointment, description: string) => {
        dispatch(completeAptDis(apt, description))
    }
});


interface OwnProp {
    isClient: boolean
}

export const FilteredAppointmentList = connect(mapStateToProps, mapDispatchToProps)(ProtoFilteredAppointmentList);