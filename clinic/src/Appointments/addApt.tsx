import React, {FormEvent, useEffect, useState} from "react";
import {connect} from "react-redux";
import {addAppointmentDis} from "./actions";
import {GlobalState} from "../App";
import {Appointment} from "./index";
import {Link, Redirect} from "react-router-dom";
import {changeHandlerNumber} from "../Utils/Handlers";
import ReactLightCalendar from '@lls/react-light-calendar'
import '@lls/react-light-calendar/dist/index.css'
import {Vet} from "../Veterinarian";
import {Pet} from "../Pets";
import {fetchVets} from "../Veterinarian/actions";

const ProtoAddAppointmentForm = (props: { addAppointment: (appointment: Appointment) => void, isFetching: boolean, pets: Pet[], loadVets: () => void, vets: Vet[] }) => {
    const [petId, setPetId] = useState(0);
    // const [shiftId, setShiftId] = useState(7);
    const [vetId, setVetId] = useState(0);
    const [startingTime, setStartingTime] = useState(new Date());
    const [endingTime, setEndingTime] = useState(new Date());
    const [description, setDescription] = useState("Bobby is very large and has black hair");
    const [duration, setDuration] = useState(0);
    const [goBack, setGoBack] = useState(false);

    // eslint-disable-next-line
    let submitHandler = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        let appointment: Appointment = {
            id: 0,
            petId: petId,
            shiftId: 8,
            vetId: vetId,
            startingTime: startingTime,
            endingTime: endingTime,
            description: description,
            duration: (endingTime.getTime() - startingTime.getTime()) / 3600000,
            completed: false
        };

        props.addAppointment(appointment);

        setPetId(0);
        // setShiftId(7);
        setVetId(0);
        setStartingTime(new Date());
        setEndingTime(new Date());
        setDescription("Bobby is very large and has black hair");
        setDuration(0);
        setGoBack(true);
    };

    let changeHandlerDatesAndDuration = (startDate: number, endDate: number) => {
        let s = new Date(startDate);
        let e = new Date(endDate);
        s.setSeconds(0, 0);
        if (endDate == null) {

            e = s;
            console.log(e);
            console.log(s);

        }
        e.setSeconds(0, 0);

        if ((e.getTime() - s.getTime()) < 0) {
            setStartingTime(e);
            setEndingTime(s);

        } else {
            setStartingTime(s);
            setEndingTime(e);
        }
        setDuration(endDate == null ? 0 : Math.abs(e.getTime() - s.getTime()) / 3600000);
    };

    useEffect(() => props.loadVets(), []);

    return (
        <div>
            <h4 id="form-title" className="border-bottom border-gray pb-2 mb-5">Fill the form to add a new
                appointment</h4>
            <div id="content">
                <form onSubmit={submitHandler} id="form-left">
                    <div className="form-row">
                        <div className="col-md-5 mb-4">
                            <ReactLightCalendar startDate={startingTime.getTime()}
                                                endDate={endingTime.getTime()}
                                                onChange={
                                                    (startDate: any, endDate: any) => {
                                                        changeHandlerDatesAndDuration(startDate, endDate);
                                                    }}
                                //disables dates before today
                                                disableDates={(date: number) => date < new Date().getTime() - 86400000}
                                                range
                                                displayTime
                            />
                        </div>

                        <div className="col">
                            <div className="form-row">
                                <div className="col-md-4 mb-3">
                                    <label htmlFor="validationDefault01">Pet Chip Number</label>
                                    {!props.isFetching && (
                                        <select id="inputState" className="form-control w-auto" required value={petId}
                                                onChange={(event) => {
                                                    changeHandlerNumber(setPetId, event)
                                                }}>

                                            <option value="0">Select a Pet...</option>
                                            {props.pets.map((pet: Pet) =>
                                                <option key={pet.id} value={pet.id}>
                                                    ({pet.id})&nbsp;{pet.chipNumber}</option>
                                            )
                                            }
                                        </select>
                                    )}
                                </div>
                                <div className="col-md-4 mb-3" id="form-left">
                                    <label htmlFor="validationDefault01">Duration</label>
                                    <input type="number" className="form-control"
                                           placeholder="Description" value={duration} min="0" disabled
                                           onChange={event => changeHandlerNumber(setDuration, event)}/>
                                </div>
                                {/*<div className="col-md-4 mb-3" id="form-left">*/}
                                {/*    <label htmlFor="validationDefault01">Shift Id</label>*/}
                                {/*    <input type="number" className="form-control"*/}
                                {/*           placeholder="Shift Id" required value={shiftId} min="0"*/}
                                {/*           onChange={event => changeHandlerNumber(setShiftId, event)}/>*/}
                                {/*</div>*/}
                            </div>

                            <div className="form-row">
                                <div className="col-md-4 mb-3">
                                    <label htmlFor="validationDefault01">Veterinarian</label>
                                    {!props.isFetching && (
                                        <select id="inputState" className="form-control w-auto" required value={vetId}
                                                onChange={(event) => {
                                                    changeHandlerNumber(setVetId, event)
                                                }}>

                                            <option value="0">Select a Vet...</option>
                                            {props.vets.map((vet: Vet) =>
                                                <option key={vet.id} value={vet.id}>
                                                    ({vet.id})&nbsp;{vet.name}</option>
                                            )
                                            }
                                        </select>
                                    )}
                                </div>
                            </div>
                            <div className="form-row justify-content-center" id="apt-form-leftMinus">
                                <button className="btn btn-primary" type="submit">Submit form</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <small className="d-block text-center mt-3">
                <Link to="/profile">Go Back</Link>
            </small>
            {goBack && <Redirect to={"/profile"}/>}
        </div>

    );
};

const mapStateToProps = (state: GlobalState) => ({
    isFetching: state.pets.isFetching,
    pets: state.pets.pets,
    vets: state.vet.vets
});
const mapDispatchToProps = (dispatch: any) => ({
    addAppointment: (appointment: Appointment) => {
        dispatch(addAppointmentDis(appointment))
    },
    loadVets: () => {
        dispatch(fetchVets())
    }
});
export const AddAppointmentForm = connect(mapStateToProps, mapDispatchToProps)(ProtoAddAppointmentForm);
