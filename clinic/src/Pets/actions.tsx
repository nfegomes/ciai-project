import {deleteData, getData, postData, updateData} from "../Utils/NetworkUtils";
import {Action} from "redux";
import {Pet} from "./index";
import React from "react";

export const ADD_PET = 'ADD_PET';
export const REQUEST_PETS = 'REQUEST_PETS';
export const RECEIVE_PETS = 'RECEIVE_PETS';
export const DELETE_PET = 'DELETE_PET';
export const EDIT_PET = 'EDIT_PET';

export interface AddPetAction extends Action {
    data: Pet
}

export interface ReceivePetAction extends Action {
    data: Pet[]
}

export interface DeletePetAction extends Action {
    data: number
}

export interface EditPetAction extends Action {
    data: Pet
}

export const addPet = (pet: Pet) => ({type: ADD_PET, data: pet});
export const requestPets = () => ({type: REQUEST_PETS});
export const receivePets = (data: Pet[]) => ({type: RECEIVE_PETS, data: data});
export const deletePet = (petId: number) => ({type: DELETE_PET, data: petId});
export const editPet = (pet: Pet) => ({type: EDIT_PET, data: pet});

export function fetchPets(username: string | null) {
    return (dispatch: any) => {
        dispatch(requestPets());
        let url: string
        if (username != null)
            url = 'clients/' + username + "/pets/";
        else
            url = 'pets/';

        return getData(url, [])
            .then(data => {
                console.log('lista no fetch', data);
                data && dispatch(receivePets(data))
            })
    }
}

export function addPetDis(pet: Pet, username: string) {
    return (dispatch: any) => {
        postData('/pets/', "", JSON.stringify({
            "id": 0,
            "chipNumber": pet.chipNumber,
            "species": pet.species,
            "age": pet.age,
            "picture": pet.picture,
            "ownerUsername": username,
            "physicalDescription": pet.physicalDescription,
            "healthStatus": pet.healthStatus,
            "medicalRecord": pet.medicalRecord,
            "isFrozen": false //always false while adding a new one
        })).then(
            result =>
                console.log(result)
        );

        dispatch(addPet(pet));
    }
}

export function deletePetDis(petId: number) {
    return (dispatch: any) => {
        dispatch(deletePet(petId));
        return deleteData("/pets/" + petId);
    }
}

export function editPetDis(pet: Pet) {
    return (dispatch: any) => {


        dispatch(editPet(pet));
        return updateData("/pets/" + pet.id, "",
            JSON.stringify({
                "id": pet.id,
                "chipNumber": pet.chipNumber,
                "species": pet.species,
                "age": pet.age,
                "picture": pet.picture,
                "ownerUsername": pet.ownerUsername,
                "physicalDescription": pet.physicalDescription,
                "healthStatus": pet.healthStatus,
                "medicalRecord": pet.medicalRecord,
                "isFrozen": false //always false while adding a new one
            }))
    }
}