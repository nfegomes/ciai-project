import React, {FormEvent, useState} from "react";
import {connect} from "react-redux";
import {addPetDis} from "./actions";
import {GlobalState} from "../App";
import {Pet} from "./index";
import {Link, Redirect} from "react-router-dom";
import {changeHandlerNumber, changeHandlerString} from "../Utils/Handlers";

const ProtoPetForm = (props: { addPet: (pet: Pet, username: string) => void, username: string }) => {
    const [chipNumber, setChipNumber] = useState(1);
    const [species, setSpecies] = useState("Dog");
    const [age, setAge] = useState(0);
    const [picture, setPicture] = useState("");
    const [physicalDesc, setPhysicalDesc] = useState("");
    const [healthStatus, setHealthStatus] = useState("Healthy");
    const [medicalRecord, setMedicalRecord] = useState("");
    const [goBack, setGoBack] = useState(false);


    let submitHandler = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        let pet: Pet = {
            id: 0,
            chipNumber: chipNumber,
            species: species,
            age: age,
            picture: picture,
            ownerUsername: props.username,
            physicalDescription: physicalDesc,
            healthStatus: healthStatus,
            medicalRecord: medicalRecord
        };

        props.addPet(pet, props.username);
        setGoBack(true);

        setChipNumber(1);
        setSpecies('Dog');
        setAge(0);
        setPicture('');
        setPhysicalDesc('');
        setHealthStatus('Healthy');
        setMedicalRecord('');
    };

    return (
        <div>
            <h4 id="form-title" className="border-bottom border-gray pb-2 mb-5">Fill the form to add a new
                pet</h4>
            <div id="content">
                <form onSubmit={submitHandler} id="form-left">
                    <div className="form-row">
                        <div className="col-md-3 mb-3">
                            <label htmlFor="validationDefault01">Chip Number</label>
                            <input type="number" className="form-control"
                                   placeholder="Chip Number" required value={chipNumber} min="1"
                                   onChange={event => changeHandlerNumber(setChipNumber, event)}/>
                        </div>
                        <div className="col-md-3 mb-3">
                            <label htmlFor="validationDefault01">Age</label>
                            <input type="number" className="form-control"
                                   placeholder="Age" required value={age} min="0"
                                   onChange={event => changeHandlerNumber(setAge, event)}/>
                        </div>
                        <div className="col-md-3 mb-3">
                            <label htmlFor="inputState">Species</label>
                            <select id="inputState" className="form-control" required value={species}
                                    onChange={(event) => changeHandlerString(setSpecies, event)}>
                                <option value="Dog">Dog</option>
                                <option value="Cat">Cat</option>
                                <option value="Fish">Fish</option>
                                <option value="Horse">Horse</option>
                                <option value="Bird">Bird</option>
                            </select>
                        </div>
                        <div className="col-md-3 mb-3">
                            <label htmlFor="inputState">Health Status</label>
                            <select id="inputState" className="form-control" required value={healthStatus}
                                    onChange={(event) => changeHandlerString(setHealthStatus, event)}>
                                <option value="Healthy">Healthy</option>
                                <option value="Sick">Sick</option>
                            </select>
                        </div>
                    </div>

                    <div className="form-row mb-3">
                        <label htmlFor="validationDefault01">Physical Description</label>
                        <input name="" className="form-control" placeholder="Physical Description" type="text"
                               value={physicalDesc} required
                               onChange={(event) => changeHandlerString(setPhysicalDesc, event)}/>
                    </div>

                    <div className="form-row mb-3">
                        <label htmlFor="validationDefault01">Picture</label>
                        <input name="" className="form-control" placeholder="Picture" type="text" required
                               value={picture} onChange={(event) => changeHandlerString(setPicture, event)}/>
                    </div>

                    <div className="form-row mb-3">
                        <label htmlFor="validationDefault01">Medical Record</label>
                        <input name="" className="form-control" placeholder="Medical Record" type="text" required
                               value={medicalRecord}
                               onChange={(event) => changeHandlerString(setMedicalRecord, event)}/>
                    </div>

                    <div className="form-row justify-content-center mt-4" id="pet-form-leftMinus">
                        <button className="btn btn-primary" type="submit">Submit form</button>
                    </div>
                </form>
            </div>
            <small className="d-block text-center mt-3">
                <Link to="/profile">Go Back</Link>
            </small>
            {goBack && <Redirect to={"/profile"}/>}
        </div>);
};

const mapStateToProps = (state: GlobalState) => ({pets: state.pets.pets, username: state.login.username});
const mapDispatchToProps = (dispatch: any) => ({
    addPet: (pet: Pet, username: string) => {
        dispatch(addPetDis(pet, username))
    }
});
export const AddPetForm = connect(mapStateToProps, mapDispatchToProps)(ProtoPetForm);