import {Action} from "redux";
import {
    ADD_PET,
    AddPetAction,
    DELETE_PET,
    DeletePetAction, EDIT_PET, EditPetAction,
    RECEIVE_PETS,
    ReceivePetAction,
    REQUEST_PETS
} from './actions';
import {PetState} from "./index";

const initialState = {
    pets: [],
    isFetching: false,
};

function petReducer(state: PetState = initialState, action: Action): PetState {
    switch (action.type) {
        case ADD_PET:
            let addPetAction = (action as AddPetAction);
            return {...state, pets: [...state.pets, addPetAction.data]};
        case REQUEST_PETS:
            return {...state, isFetching: true};
        case RECEIVE_PETS:
            return {...state, isFetching: false, pets: (action as ReceivePetAction).data};
        case DELETE_PET:
            return {...state, pets: state.pets.filter(pet => pet.id !== (action as DeletePetAction).data)};
        case EDIT_PET:
            let editAction = (action as EditPetAction);
            return {
                ...state, pets: state.pets.map(pet => {

                    if (pet.id === editAction.data.id)
                        return editAction.data;
                    else
                        return pet;
                })
            };
        default:
            return state
    }
}

export default petReducer