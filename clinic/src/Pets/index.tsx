import React, {ChangeEvent, FormEvent, useEffect, useState} from "react";
import {connect} from "react-redux";
import {deletePetDis, editPetDis, fetchPets} from "./actions";
import {GlobalState} from "../App";
import {Link} from "react-router-dom";
import {faEdit, faPlusSquare, faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {changeHandlerNumber, changeHandlerString} from "../Utils/Handlers";
import {Col, Container, ListGroup, Row, Image, ListGroupItem} from "react-bootstrap";

export interface Pet {
    id: number,
    chipNumber: number,
    species: string,
    age: number,
    picture: string,
    ownerUsername: string,
    physicalDescription: string,
    healthStatus: string,
    medicalRecord: string
}

export interface PetState {
    pets: Pet[],
    isFetching: boolean
}

const PetList = (props: {
    pets: Pet[], isFetching: boolean,
    deletePet: (petId: number) => void, editPet: (pet: Pet, username: string) => void, username: string, isClient: boolean
}) => {

    return (
        <div className="my-3 p-3 bg-white box-shadow ">
            <div>
                <div className="border-bottom border-gray d-flex justify-content-between mb-2">
                    <h6 className="d-inline font-weight-bold">Your Pets</h6>
                    {props.isClient &&
                    <Link to="/addPet" className="d-inline"><FontAwesomeIcon className="lightbutton" icon={faPlusSquare}
                                                                             size="lg"/></Link>}
                </div>
            </div>
            <ListGroup className="center-h" style={{
                maxWidth: '750px',
                minWidth: '350px',

            }}>
                {console.log('o pets e', props.pets)}
                {!props.isFetching && props.pets.map((pet: Pet) =>
                    <ListGroupItem key={pet.id} as={Container}>
                        <Row>
                            <Col sm={3} className="center-v center-h">
                                <Image className="card-image" roundedCircle width="68"
                                       height="68" src={pet.picture == "" ? "https://tinyurl.com/w99xy4v" : pet.picture}
                                       alt=""/>
                            </Col>
                            <Col>
                                <strong className="d-block text-gray-dark">Pet {pet.chipNumber}</strong>
                                <u>Age:</u> {pet.age}<br/>
                                <u>Species:</u> {pet.species}<br/>
                                <u>Health status:</u> {pet.healthStatus}
                            </Col>
                            {props.isClient && <Col sm={1} className="center-v text-left">

                                <Row className="mb-2">
                                    <Col>
                                        <FontAwesomeIcon onClick={() => props.editPet(pet, props.username)}
                                                         className="darkbutton" icon={faEdit} size="lg"/>
                                    </Col>
                                </Row>
                                <Row className="mt-2">
                                    <Col>
                                        <FontAwesomeIcon onClick={() => props.deletePet(pet.id)}
                                                         className="darkbutton" icon={faTrash} size="lg"/>
                                    </Col>
                                </Row>
                            </Col>}
                        </Row>
                    </ListGroupItem>
                )}
            </ListGroup>
        </div>);
};

const EditPetForm = (props: {
    pet: Pet, editPet: (pet: Pet) => void, setPetNull: () => void,
    username: string
}) => {
    const [chipNumber, setChipNumber] = useState(props.pet.chipNumber);
    const [species, setSpecies] = useState(props.pet.species);
    const [age, setAge] = useState(props.pet.age);
    const [picture, setPicture] = useState(props.pet.picture);
    const [physicalDesc, setPhysicalDesc] = useState(props.pet.physicalDescription);
    const [healthStatus, setHealthStatus] = useState(props.pet.healthStatus);
    const [medicalRecord, setMedicalRecord] = useState(props.pet.medicalRecord);

    let submitHandler = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        let pet: Pet = {
            id: props.pet.id,
            chipNumber: chipNumber,
            species: species,
            age: age,
            ownerUsername: props.pet.ownerUsername,
            picture: picture,
            physicalDescription: physicalDesc,
            healthStatus: healthStatus,
            medicalRecord: medicalRecord
        };

        props.editPet(pet);

        setChipNumber(0);
        setSpecies('');
        setAge(0);
        setPicture('');
        setPhysicalDesc('');
        setHealthStatus('');
        setMedicalRecord('');
    };

    let goBackHandler = () => {
        props.setPetNull();
    };

    return <div className="my-3 p-3 bg-white box-shadow">
        <h6 className="border-bottom border-gray pb-2 mb-0">Edit Pet</h6>
        <div id="content" className="media text-muted pt-3">
            <form onSubmit={submitHandler}>
                <div className="form-row">
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault01">Chip Number</label>
                        <input type="number" className="form-control" disabled
                               placeholder="Chip Number" required value={chipNumber} min="1"
                               onChange={event => changeHandlerNumber(setChipNumber, event)}/>
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="inputState">Species</label>
                        <select id="inputState" className="form-control" required value={species} disabled
                                onChange={(event) => changeHandlerString(setSpecies, event)}>
                            <option value="Dog">Dog</option>
                            <option value="Cat">Cat</option>
                            <option value="Fish">Fish</option>
                            <option value="Horse">Horse</option>
                            <option value="Bird">Bird</option>
                        </select>
                    </div>
                </div>
                <div className="form-row">
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault01">Age</label>
                        <input type="number" className="form-control"
                               placeholder="Age" required value={age} min="0"
                               onChange={event => changeHandlerNumber(setAge, event)}/>
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="inputState">Health Status</label>
                        <select id="inputState" className="form-control" required value={healthStatus}
                                onChange={(event) => changeHandlerString(setHealthStatus, event)}>
                            <option value="Healthy">Healthy</option>
                            <option value="Sick">Sick</option>
                        </select>
                    </div>
                </div>

                <div className="form-row mb-3">
                    <label htmlFor="validationDefault01">Physical Description</label>
                    <input name="" className="form-control" placeholder="Physical Description" type="text"
                           value={physicalDesc} required
                           onChange={(event) => changeHandlerString(setPhysicalDesc, event)}/>
                </div>

                <div className="form-row mb-3">
                    <label htmlFor="validationDefault01">Picture</label>
                    <input name="" className="form-control" placeholder="Picture" type="text" required
                           value={picture} onChange={(event) => changeHandlerString(setPicture, event)}/>
                </div>

                <div className="form-row mb-3">
                    <label htmlFor="validationDefault01">Medical Record</label>
                    <input name="" className="form-control" placeholder="Medical Record" type="text" required
                           value={medicalRecord}
                           onChange={(event) => changeHandlerString(setMedicalRecord, event)}/>
                </div>

                <div className="form-row">
                    <button className="btn btn-primary ml-auto mr-0" type="submit">Submit</button>
                    &emsp;
                    <button className="btn btn-outline-danger" onClick={() => goBackHandler()}>Cancel</button>
                </div>
            </form>
        </div>
    </div>
};

const ProtoFilteredPetList = (props: {
    pets: Pet[], isFetching: boolean, loadPets: (filter: string | null) => void,
    deletePet: (petId: number) => void, editPet: (pet: Pet) => void, username: string, isClient: boolean
}) => {
    const [filter, setFilter] = useState("");
    const [pet, setPet] = useState(null as unknown as Pet);
    let handle = (e: ChangeEvent<HTMLInputElement>) => setFilter(e.target.value);
    // eslint-disable-next-line
    useEffect(() => {
        if (props.isClient)
            props.loadPets(props.username);
        else
            props.loadPets(null);
    }, [filter]);

    const setToEditPet = (pet: Pet) => {
        setPet(pet);
    };

    const setPetNull = () => {
        setPet(null as unknown as Pet);
    };

    const editPet = (pet: Pet) => {
        props.editPet(pet);
        setPet(null as unknown as Pet);
    };

    return <>
        {pet == null ?
            <PetList pets={props.pets} isFetching={props.isFetching} deletePet={props.deletePet}
                     editPet={setToEditPet}
                     username={props.username} isClient={props.isClient}/>
            :
            <EditPetForm pet={pet} editPet={editPet} setPetNull={setPetNull} username={props.username}/>
        }
    </>;
};

const mapStateToProps = (state: GlobalState, props: OwnProp) => ({
    pets: state.pets.pets, isFetching: state.pets.isFetching,
    isClient: props.isClient,
    token: state.login.token, username: state.login.username

});
const mapDispatchToProps = (dispatch: any) => ({
    loadPets: (username: string | null) => {
        dispatch(fetchPets(username))
    },
    deletePet: (petId: number) => {
        dispatch(deletePetDis(petId))
    },
    editPet: (pet: Pet) => {
        dispatch(editPetDis(pet))
    }
});

interface OwnProp {
    isClient: boolean
}


export const FilteredPetList = connect(mapStateToProps, mapDispatchToProps)(ProtoFilteredPetList);
