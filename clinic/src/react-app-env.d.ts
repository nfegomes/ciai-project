/// <reference types="react-scripts" />
declare module '@trendmicro/react-sidenav'
declare module 'jsonwebtoken'
declare module '@lls/react-light-calendar'
declare module 'base-64'
declare module 'react-overdrive' {
    import { Component, CSSProperties } from 'react';
    export interface Props {
        id: string;
        duration?: number;
        easing?: string;
        element?: string;
        animationDelay?: number;
        onAnimationEnd?: () => void;
        style?: CSSProperties;
    }
    export interface State {
        loading: boolean;
    }
    export default class Overdrive extends Component<Props, State> {}
}
