import {Action} from "redux";
import {EDIT_CLIENT, EditClientAction, RECEIVE_CLIENT, ReceiveClientAction, REQUEST_CLIENT} from './actions';
import {Client, ClientState} from "./index";
import {RECEIVE_EMPLOYEES, ReceiveEmployeeAction, REQUEST_EMPLOYEES} from "../Employees/actions";
import {ADD_APPOINTMENT, AddAppointmentAction, ReceiveAppointmentAction} from "../Appointments/actions";
import {AppointmentState} from "../Appointments";

const initialState = {
    client: {    email:'', username:'', picture:'', name: '', address:'', cellphone:''},
    isFetching: false
};

function clientReducer(state:ClientState = initialState, action:Action): ClientState {
    switch (action.type) {
      /*  case EDIT_CLIENT:
            return <AppointmentState>{
                isFetching: false, ...state,
                appointments: [...state.appointments, {id: 0, description: (action as AddAppointmentAction).name}]
            };*/
        case REQUEST_CLIENT:
            return {...state, isFetching: true};
        case RECEIVE_CLIENT:
            return {client: (action as ReceiveClientAction).data, isFetching:false};
        default:
            return state
    }
}
export default clientReducer;