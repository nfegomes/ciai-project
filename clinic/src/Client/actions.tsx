import {getData} from "../Utils/NetworkUtils";
import {Action} from "redux";
import {Client} from "./index";
import {login} from "../Login/actions";

export const EDIT_CLIENT = 'EDIT_CLIENT';
export const REQUEST_CLIENT = 'REQUEST_CLIENT';
export const RECEIVE_CLIENT = 'RECEIVE_CLIENT';

export interface EditClientAction extends Action { data:Client }
export interface ReceiveClientAction extends Action { data:Client }

export const editClient = (client:Client) => ({type:EDIT_CLIENT, data:client});
export const requestClient = () => ({type: REQUEST_CLIENT});
export const receiveClient = (data: any[]) => ({type: RECEIVE_CLIENT, data:data});
//TODO este any[] tem de ser mudado

export function fetchClient(username:string) {
    return (dispatch:any) => {

        dispatch(requestClient());
        return getData(`/clients/`+username, [])
            .then(data => {console.log(data); data && dispatch(receiveClient(data))})
        // notice that there is an extra "appointment" in the path above which is produced
        // in this particular implementation of the service. {appointment: Appointment, Appointments:List<AppointmentDTO>}
    }
}

export function editClientDis(client:Client) {
    return (dispatch:any) => {
        dispatch(editClient(client));
    }
}