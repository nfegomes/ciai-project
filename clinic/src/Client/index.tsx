import React, {ChangeEvent, FormEvent, useEffect, useState} from 'react';
import {GlobalState} from "../App";
import {connect} from "react-redux";
import {editClientDis, fetchClient} from "./actions";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import {Container, Navbar, Image, Row, Col} from "react-bootstrap";
import {Simulate} from "react-dom/test-utils";

export interface ClientState {
    client:Client,
    isFetching: boolean
}

export interface Client {
    email:string,
    username:string,
    picture:string,
    name: string,
    address: string,
    cellphone: string
}

const ClientInfo = (props: { client: Client, editClient:(client:Client)=>void }) =>
    <>
        <Navbar
            className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 border-bottom box-shadow">
            <Container>
                <Row className="align-items-center">
                    <Col>
                        <Image className="card-image" roundedCircle
                               src={props.client.picture}
                               width="68" height="68"/>
                    </Col>
                    <Col className="align-items-center">
                        <Navbar.Brand className="">Welcome, {props.client.name.toUpperCase()}!</Navbar.Brand>
                    </Col>
                    {/*<Link to="/editProfile" className="btn pull-right" id="signupbutton"><div><FontAwesomeIcon icon={faEdit} /></div></Link>*/}
                </Row>
                <Navbar.Brand className="mb-0 lh-100 font-weight-bold">Dashboard</Navbar.Brand>
            </Container>
        </Navbar>
</>;

const EditInfoForm = (props: {client: Client, editClient: (client:Client) => void, setClientNull: () => void}) => {

    const [ name, setName ] = useState("");
    const [ picture, setPicture ] = useState("");
    const [ cellphone, setCellphone ] = useState("");
    const [ address, setAddress ] = useState("");
    const [ email, setEmail ] = useState("");
    const [ username, setUsername ] = useState("");

    let submitHandler = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        let client: Client = {
            email: email,
            username: username,
            picture: picture,
            name: name,
            address: address,
            cellphone: cellphone
        };

        props.editClient(client);

        setName('');
        setAddress('');
        setEmail('');
        setPicture('');
        setUsername('');
        setCellphone('');
    };

    return <div className="text-center">
            <form onSubmit={submitHandler}>
                <div><label>Name: <input type="text" className="form-control"/></label></div>
                <div><label>Picture: <input type="text" className="form-control"
                                            /></label></div>
                <div><label>Cellphone: <input type="number" className="form-control"
                                              /></label></div>
                <div><label>Address: <input type="text" className="form-control"
                                           /></label></div>
                <div><label>Email: <input type="email" className="form-control"
                                          /></label></div>
                <div><label>Username: <input type="text" className="form-control"
                                             /></label></div>
                <Link to="/profile"><button type="submit" className="btn btn-primary">Sign Up</button></Link>
            </form>
        </div>
}

const ProtoClientInfo = (props: { username:string, client: Client, loadClient: (username:string) => void,
    editClient: (client:Client)=>void }) => {
    useEffect(() => props.loadClient(props.username),[]);
    console.log('o username e', props.username);

    const [client, setClient] = useState(null as unknown as Client);
    const editClient = (client: Client) => {
        props.editClient(client);
        setClient(client);
    };

    const setToEditClient = (client: Client) => {
        setClient(client);
        console.log('o client e', );
    };

    const setClientNull = () => {
        setClient(null as unknown as Client);
    };

    return <>
            <ClientInfo client={props.client} editClient={setToEditClient}/>
        {client != null ?
            <EditInfoForm client={client} editClient={editClient} setClientNull={setClientNull}/>
            : <div></div>
        }
    </>;
};


const mapStateToProps = (state: GlobalState) => ({username: state.login.username, client: state.client.client});
const mapDispatchToProps = (dispatch: any) => ({
    loadClient: (username:string) => {
        dispatch(fetchClient(username))
    },
    editClient: (client: Client) => {
        dispatch(editClientDis(client))
    }
});
export const ClientInfoComp = connect(mapStateToProps, mapDispatchToProps)(ProtoClientInfo);

