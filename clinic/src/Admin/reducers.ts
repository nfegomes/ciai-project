import {Action} from "redux";
import {EDIT_ADMIN, EditAdminAction, RECEIVE_ADMIN, ReceiveAdminAction, REQUEST_ADMIN} from './actions';
import {Admin, AdminState} from "./index";
import {RECEIVE_EMPLOYEES, ReceiveEmployeeAction, REQUEST_EMPLOYEES} from "../Employees/actions";
import {ADD_APPOINTMENT, AddAppointmentAction, ReceiveAppointmentAction} from "../Appointments/actions";
import {AppointmentState} from "../Appointments";

const initialState = {
    admin: {email: '', username: '', picture: '', name: '', address: '', cellphone: 0},
    isFetching: false
};

function adminReducer(state: AdminState = initialState, action: Action): AdminState {
    switch (action.type) {
        // case EDIT_ADMIN:
        //     return {
        //         ...state,
        //         isFetching: false,
        //         appointments:
        //             [...state.appointments, {id: 0, description: (action as AddAppointmentAction).name}]
        //     };

        case REQUEST_ADMIN:
            return {...state, isFetching: true};
        case RECEIVE_ADMIN:
            return {admin: (action as ReceiveAdminAction).data, isFetching: false};
        default:
            return state
    }
}

export default adminReducer;