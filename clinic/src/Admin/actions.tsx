import {getData} from "../Utils/NetworkUtils";
import {Action} from "redux";
import {Admin} from "./index";
import {login} from "../Login/actions";

export const EDIT_ADMIN = 'EDIT_ADMIN';
export const REQUEST_ADMIN = 'REQUEST_ADMIN';
export const RECEIVE_ADMIN = 'RECEIVE_ADMIN';

export interface EditAdminAction extends Action { data:Admin }
export interface ReceiveAdminAction extends Action { data:Admin }

export const editAdmin = (admin:Admin) => ({type:EDIT_ADMIN, data:admin});
export const requestAdmin = () => ({type: REQUEST_ADMIN});
export const receiveAdmin = (data: any[]) => ({type: RECEIVE_ADMIN, data:data});
//TODO este any[] tem de ser mudado

export function fetchAdmin(username:string) {
    return (dispatch:any) => {
        dispatch(requestAdmin());
        return getData('/admins/'+username, [])
            .then(data => { data && dispatch(receiveAdmin(data))})
        // notice that there is an extra "appointment" in the path above which is produced
        // in this particular implementation of the service. {appointment: Appointment, Appointments:List<AppointmentDTO>}
    }
}

export function editAdminDis(admin:Admin) {
    return (dispatch:any) => {
        dispatch(editAdmin(admin));
    }
}