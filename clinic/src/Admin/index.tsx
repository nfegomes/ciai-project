import React, {ChangeEvent, useEffect, useState} from 'react';
import {GlobalState} from "../App";
import {connect} from "react-redux";
import {fetchAdmin} from "./actions";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import {Client} from "../Client";
import {Col, Container, Image, Navbar, Row} from "react-bootstrap";

export interface Admin {
    email: string,
    username: string,
    picture: string,
    name: string,
    address: string,
    cellphone: number
}

export interface AdminState {
    admin: Admin,
    isFetching: boolean
}

const AdminInfo = (props: { admin: Admin }) =>
    <>
        <Navbar
            className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 border-bottom box-shadow">
            <Container>
                <Row className="align-items-center">
                    <Col>
                        <Image className="card-image" roundedCircle
                               src={props.admin.picture}
                               width="68" height="68"/>
                    </Col>
                    <Col className="align-items-center">
                        <Navbar.Brand className="">Welcome, {props.admin.name.toUpperCase()}!</Navbar.Brand>
                    </Col>
                    {/*<Link to="/editProfile" className="btn pull-right" id="signupbutton">*/}
                        {/*<div><FontAwesomeIcon icon={faEdit}/></div>*/}
                    {/*</Link>*/}
                </Row>
                <Navbar.Brand className="mb-0 lh-100 font-weight-bold">Dashboard</Navbar.Brand>
            </Container>
        </Navbar>
    </>;

const ProtoAdminInfo = (props: { username: string, admin: Admin, loadAdmin: (username: string) => void }) => {
    useEffect(() => props.loadAdmin(props.username), []);
    console.log('o username e', props.username);

    return <>
        <AdminInfo admin={props.admin}/>
    </>;
};


const mapStateToProps = (state: GlobalState) => ({username: state.login.username, admin: state.admin.admin});
const mapDispatchToProps = (dispatch: any) => ({
    loadAdmin: (username: string) => {
        dispatch(fetchAdmin(username))
    }
});
export const AdminInfoComp = connect(mapStateToProps, mapDispatchToProps)(ProtoAdminInfo);

