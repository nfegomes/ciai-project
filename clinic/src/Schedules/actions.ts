import {Action} from "redux";
import {Schedule} from "./index";
import {getData, postData} from "../Utils/NetworkUtils";

export const ADD_SCHEDULES = 'ADD_SCHEDULES';
export const REQUEST_SCHEDULES = 'REQUEST_SCHEDULES';
export const RECEIVE_SCHEDULES = 'RECEIVE_SCHEDULES';

export interface AddScheduleAction extends Action {
    data: Schedule
}

export interface ReceiveScheduleAction extends Action {
    data: Schedule[]
}

export const addSchedule = (schedule: Schedule) => ({type: ADD_SCHEDULES, data: schedule});

export const requestSchedules = () => ({type: REQUEST_SCHEDULES});
export const receiveSchedules = (data: Schedule[]) => ({type: RECEIVE_SCHEDULES, data: data});


export function fetchSchedules(id : number) {
    return (dispatch: any) => {
        dispatch(requestSchedules());
        return getData('/vets/'+id+'/shifts/', [])
            .then(data => {
                data && dispatch(receiveSchedules(data.map((p: Schedule) => {
                    return p
                })))
            })
    }
}

export function addScheduleDis(schedule: Schedule) {
    return (dispatch: any) => {
        postData('/shifts/', "", JSON.stringify([{
            "id": 0,
            "vetID": schedule.vetId,
            "startDateTime": schedule.startDateTime.toISOString(),
            "endDateTime": schedule.endDateTime.toISOString()
        }])).then(
            result => {
                console.log(result);
                dispatch(fetchSchedules(schedule.vetId));
            }
        );

        // dispatch(addSchedule(schedule));
    }
}