import React, {ChangeEvent, FormEvent, useEffect, useState} from "react";
import {connect} from "react-redux";
import {addScheduleDis, fetchSchedules} from "./actions";
import {GlobalState} from "../App";
import {Vet} from "../Veterinarian";
import {fetchVets} from "../Veterinarian/actions";
import ReactLightCalendar from "@lls/react-light-calendar";
import {Col, Container, Image, ListGroupItem, Row} from "react-bootstrap";
import {Link} from "react-router-dom";

export interface Schedule {
    id: number,
    vetId: number,
    startDateTime: Date,
    endDateTime: Date,
}

export interface ScheduleState {
    schedules: Schedule[],
    isFetching: boolean
}

const ProtoAddSchedules = (props: { addSchedule: (schedule: Schedule) => void, vetId: number }) => {

    const [startDateTime, setStartDateTime] = useState(new Date());
    const [endDateTime, setEndDateTime] = useState(new Date());

    // eslint-disable-next-line
    let submitHandler = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        let schedule: Schedule = {
            id: 0,
            vetId: props.vetId,
            startDateTime: startDateTime,
            endDateTime: endDateTime
        };

        props.addSchedule(schedule);

        setStartDateTime(new Date());
        setEndDateTime(new Date());
    };

    let changeHandlerDates = (startDate: number, endDate: number) => {
        setStartDateTime(new Date(startDate));
        setEndDateTime(new Date(endDate));
    };

    return (
        <div>
            <div id="content" className="center">
                <form onSubmit={submitHandler} id="form-left">
                    <div className="form-row">
                        <div className="col-md-5 mb-4">
                            <ReactLightCalendar startDate={startDateTime.getTime()}
                                                endDate={endDateTime.getTime()}
                                                onChange={
                                                    (startDate: any, endDate: any,) => {
                                                        changeHandlerDates(startDate, endDate);
                                                    }}
                                //disables dates before today
                                                disableDates={(date: number) => date < new Date().getTime() - 86400000}
                                                range
                                                displayTime
                            />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="col">
                            <div className="form-row mt-2 justify-content-center" id="pet-form-leftMinus">
                                <button className="btn btn-primary" type="submit">Add Shift</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
};

const SchedulesList = (props: { vetId: number, schedules: Schedule[], isFetching: boolean, loadSchedule: (id: number) => void }) => {
    useEffect(() => props.loadSchedule(props.vetId), []);

    return <>
        <Row className="album ml-1">
            <Container>
                <Row>
                    {!props.isFetching && props.schedules.map((schedule: Schedule) =>
                        <ListGroupItem key={schedule.id}>
                            <Row>
                                <u>Start:</u>&nbsp;&nbsp;
                                <DateToString date={schedule.startDateTime}/>
                                &emsp;
                                <u>End:</u>&nbsp;&nbsp;
                                <DateToString date={schedule.endDateTime}/>
                            </Row>
                        </ListGroupItem>
                    )}
                </Row>
            </Container>
        </Row>
    </>;
};

const DateToString = (props: { date: Date }) => {
    let dateS = "" + props.date;
    let year = dateS.slice(0, 4);
    let month = dateS.slice(5, 7);
    let day = dateS.slice(8, 10);
    let hour = dateS.slice(11, 13);
    let min = dateS.slice(14, 16);

    return <>
        <strong>{day}/{month}/{year}</strong>
        &nbsp;
        {hour}h{min}
    </>;
};

const ProtoFilteredSchedulesList = (props: { addSchedule: (schedule: Schedule) => void, schedules: Schedule[], loadVets: () => void, loadSchedule: (id: number) => void, isFetching: boolean, vets: Vet[], isVetsFetching: boolean }) => {
    const [vetId, setVetId] = useState(0);

    let changeHandlerNumber = (field: { (value: React.SetStateAction<number>): void }, e: ChangeEvent<any>) => {
        let newValue = parseInt(e.target.value);
        field(newValue);
        props.loadSchedule(newValue);
    };
    // eslint-disable-next-line
    useEffect(() => props.loadVets(), []);
    return <>
        <Container>
            <Row>
                <h4 id="form-title" className="border-bottom border-gray pb-2 mb-5 center-h">Click the calendar
                    to add a new shift to the selected veterinarian</h4>
            </Row>
            <Row>
                <Col>
                    <ProtoAddSchedules addSchedule={props.addSchedule} vetId={vetId}/>
                </Col>
                <Col>
                    <Row>
                        {!props.isFetching && (
                            <select id="inputState" className="form-control w-auto" required value={vetId}
                                    onChange={(event) => {
                                        changeHandlerNumber(setVetId, event)
                                    }}>

                                <option value="0">Select a Veterinarian...</option>
                                {props.vets.map((vet: Vet) =>
                                    <option key={vet.id} value={vet.id}>
                                        {vet.name}&emsp;({vet.username})</option>
                                )
                                }
                            </select>
                        )}
                    </Row>
                    <Row className="mt-5">
                        <SchedulesList vetId={vetId} schedules={props.schedules} isFetching={props.isFetching}
                                       loadSchedule={props.loadSchedule}/>
                    </Row>
                </Col>
            </Row>
            <Row>
                <small className="d-block center-h mt-3">
                    <Link to="/profile">Go Back</Link>
                </small>
            </Row>
        </Container>
    </>
        ;
};

const mapStateToProps = (state: GlobalState) => ({
    schedules: state.schedules.schedules,
    isFetching: state.schedules.isFetching,
    vets: state.vet.vets,
    isVetsFetching: state.vet.isFetching
});
const mapDispatchToProps = (dispatch: any) => ({
    loadSchedule: (id: number) => {
        dispatch(fetchSchedules(id))
    },
    loadVets: () => {
        dispatch(fetchVets())
    },
    addSchedule: (schedule: Schedule) => {
        dispatch(addScheduleDis(schedule))
    }
});
export const FilteredScheduleList = connect(mapStateToProps, mapDispatchToProps)(ProtoFilteredSchedulesList);
