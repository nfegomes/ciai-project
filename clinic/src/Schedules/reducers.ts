import {Action} from "redux";
import {ADD_SCHEDULES, AddScheduleAction, RECEIVE_SCHEDULES, ReceiveScheduleAction, REQUEST_SCHEDULES} from './actions';
import {ScheduleState} from "./index";

const initialState = {
    schedules: [],
    isFetching: false,
};

function scheduleReducer(state: ScheduleState = initialState, action: Action): ScheduleState {
    switch (action.type) {
        case ADD_SCHEDULES:
            let addScheduleAction = (action as AddScheduleAction);
            return {...state, schedules: state.schedules.concat(addScheduleAction.data)};
        case REQUEST_SCHEDULES:
            return {...state, isFetching: true};
        case RECEIVE_SCHEDULES:
            let receiveScheduleAction = (action as ReceiveScheduleAction);
            return {...state, isFetching: false, schedules: receiveScheduleAction.data};
        default:
            return state
    }
}

export default scheduleReducer