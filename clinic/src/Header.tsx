import React from 'react';

class Header extends React.Component {
    render() {
        return (
            <div id="header" className="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center">
                <div className="col-md-7 p-lg-7 mx-auto my-5" >
                    <h1 className="display-4 font-weight-normal">Vet Clinic</h1>
                </div>
                <div className="product-device box-shadow d-none d-md-block"></div>
                <div className="product-device product-device-2 box-shadow d-none d-md-block"></div>
            </div>
        )}
};
export default Header;