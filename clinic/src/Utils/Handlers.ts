import React, {ChangeEvent} from "react";

export let changeHandlerNumber = (field: { (value: React.SetStateAction<number>): void }, e: ChangeEvent<any>) => {
    field(parseInt(e.target.value))
};

export let changeHandlerString = (field: { (value: React.SetStateAction<string>): void }, e: ChangeEvent<any>) => {
    field(e.target.value)
};
