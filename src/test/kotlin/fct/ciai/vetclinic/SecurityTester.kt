package fct.ciai.vetclinic

import fct.ciai.vetclinic.api.services.PetService
import fct.ciai.vetclinic.config.UserAuthToken
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class SecurityTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var pets: PetService

    companion object {
        const val petsURL = "/pets"
    }

    @Test
    fun `Test GET all pets (a user)`() {

        Mockito.`when`(pets.getAllPets()).thenReturn(emptyList())
        mvc.perform(MockMvcRequestBuilders.get("$petsURL/").with(user("user").roles("ADMIN")))
                .andExpect(status().isOk)
    }

    @Test
    fun `Test GET all pets (no user)`() {
        Mockito.`when`(pets.getAllPets()).thenReturn(emptyList())

        mvc.perform(MockMvcRequestBuilders.get("$petsURL/"))
                .andExpect(status().is4xxClientError) // 403
    }
}