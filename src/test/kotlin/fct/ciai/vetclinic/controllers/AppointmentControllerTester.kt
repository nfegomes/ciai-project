package fct.ciai.vetclinic.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.dto.AppointmentDTO
import fct.ciai.vetclinic.api.services.AppointmentService
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.Matchers.hasSize
import org.springframework.http.MediaType
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import java.util.*


@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class AppointmentControllerTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var aptService: AppointmentService

    companion object Constants {
        val mapper: ObjectMapper = ObjectMapper().registerModule(KotlinModule())

        var firstClient = ClientDAO("gomesnuno", LoginInfoDAO("gomesnuno", "nuno@sapo.pt", "password"), "nuno", "www.picture.com",
                961234567, "address", false, emptyList())

        var firstPet = PetDAO(1L, 123L, "dog", 10, "",
                firstClient, emptyList(), "big dog", "healthy", " ", false)

        val vet1 = VetDAO(1L, LoginInfoDAO("micaelbeco", "micael@sapo.pt", "password"), "Micael", "", 919650923,
                "Rua do Serrado", false, emptyList())

        val shift1 = ShiftDAO(1L, vet1,
                Date(2019, 11, 27, 16, 0),
                Date(2018, 11, 27, 20, 0),
                emptyList())
        val apt1 = AppointmentDTO(1L, 1L, 1L, 1L,
                Date(2019, 11, 27, 16, 0),
                Date(2018, 11, 27, 20, 0), "consulta de rotina",
                1, false)

        val apt2 = AppointmentDTO(2L, 1L, 1L, 1L,
                Date(2019, 11, 27, 16, 0),
                Date(2018, 11, 27, 20, 0), "consulta de rotina",
                1, true)

        val aptDAO1 = AppointmentDAO(apt1.id, firstPet, vet1, shift1,
                apt1.startingTime, apt1.endingTime, "consulta de rotina",
                apt1.duration, apt1.completed)

        val aptDAO2 = AppointmentDAO(apt2.id, firstPet, vet1, shift1,
                apt2.startingTime, apt2.endingTime, "consulta de rotina",
                apt2.duration, apt2.completed)

        val aptsDAO = listOf(aptDAO1, aptDAO2)

        const val aptsURL = "/appointments"
    }

    @Test
    fun `Test GET all appointments`() {
        val aptsDTO = listOf(apt1, apt2)

        Mockito.`when`(aptService.getAllAppointments()).thenReturn(aptsDAO)

        val result = mvc.perform(get("$aptsURL/").with(SecurityMockMvcRequestPostProcessors.user("user").roles("ADMIN")))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$", hasSize<Any>(aptsDTO.size)))
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<List<AppointmentDTO>>(responseString)
        assertThat(responseDTO, equalTo(aptsDTO))
    }

    @Test
    fun `Test GET all completed appointments`() {
        val aptsDTO = listOf(apt1, apt2)

        Mockito.`when`(aptService.getAllCompletedAppointments()).thenReturn(listOf(aptDAO2))

        val result = mvc.perform(get("$aptsURL/completed/").with(SecurityMockMvcRequestPostProcessors.user("user").roles("ADMIN")))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$", hasSize<Any>(1)))
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<List<AppointmentDTO>>(responseString)
        assertThat(responseDTO[0], equalTo(aptsDTO[1]))
    }

    @Test
    fun `Test GET all active appointments`() {
        val aptsDTO = listOf(apt1, apt2)

        Mockito.`when`(aptService.getAllActiveAppointments()).thenReturn(listOf(aptDAO1))

        val result = mvc.perform(get("$aptsURL/active/").with(SecurityMockMvcRequestPostProcessors.user("user").roles("ADMIN")))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$", hasSize<Any>(1)))
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<List<AppointmentDTO>>(responseString)
        assertThat(responseDTO[0], equalTo(aptsDTO[0]))
    }

    @Test
    fun `Test POST One Appointment`() {

        val petJSON = mapper.writeValueAsString(apt1)

        Mockito.`when`(aptService.getPetById(1L)).thenReturn(firstPet)

        Mockito.`when`(aptService.getVetById(1L)).thenReturn(vet1)

        Mockito.`when`(aptService.getScheduleById(1L)).thenReturn(shift1)

        Mockito.`when`(aptService.addNewAppointment(nonNullAny(AppointmentDAO::class.java)))
                .then { assertThat(it.getArgument(0), equalTo(aptDAO1)); it.getArgument(0) }

        mvc.perform(post("$aptsURL/").with(SecurityMockMvcRequestPostProcessors.user("user").roles("CLIENT"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(petJSON))
                .andExpect(status().isOk)
    }

    fun <T> nonNullAny(t: Class<T>): T = Mockito.any(t)
}