package fct.ciai.vetclinic.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.dto.AdminDTO
import fct.ciai.vetclinic.api.dto.AppointmentDTO
import fct.ciai.vetclinic.api.dto.VetDTO
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.services.AdminService
import fct.ciai.vetclinic.api.services.AppointmentService
import fct.ciai.vetclinic.api.services.VetService
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.Matchers.hasSize
import org.springframework.http.MediaType
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class VetControllerTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var vetService: VetService

    @MockBean
    lateinit var aptService: AppointmentService

    companion object Constants {
        val mapper: ObjectMapper = ObjectMapper().registerModule(KotlinModule())

        var vet1 = VetDAO(0L, LoginInfoDAO("antonio", "antonio@gmail.com", "password"),"Antonio", "www.google.com",
                919293946, "Rua das Flores",  false, emptyList())
        var vet2 = VetDAO(-1L, LoginInfoDAO("anasilva", "anasilva@sapo.pt", "password"),"Ana", "www.google.com",
                9192953946, "Rua das Flores",  false, emptyList())

        var vetsDAO = listOf(vet1, vet2)

        val vetsDTO = vetsDAO.map { VetDTO(it.id, it.user!!.username, it.name, it.picture, it.cellphone,
                it.address, it.frozen)
        }

        var firstClient = ClientDAO("gomesnuno", LoginInfoDAO("gomesnuno", "nuno@sapo.pt", "password"), "nuno", "www.picture.com",
                961234567, "address", false, emptyList())

        val apt1 = AppointmentDTO(1L, 1L, 1L, 1L,
                Date(2019, 11, 27, 16, 0),
                Date(2018, 11, 27, 20, 0), "consulta de rotina",
                1, false)

        var firstPet = PetDAO(1L, 123L, "dog", 10, "",
                firstClient, emptyList(), "big dog", "healthy", " ", false)

        val shift1 = ShiftDAO(0L, vet1,
                Date(2019, 11, 27, 16, 0),
                Date(2018, 11, 27, 20, 0),
                emptyList())

        val aptDAO = AppointmentDAO(apt1.id, firstPet, vet1,  shift1,
                apt1.startingTime, apt1.endingTime, "consulta de rotina",
                apt1.duration, apt1.completed)


        const val vetsURL = "/vets"
    }

    @Test
    fun `Test GET all vets`() {
        Mockito.`when`(vetService.getAllVets()).thenReturn(vetsDAO)

        val result = mvc.perform(get("$vetsURL/").with(SecurityMockMvcRequestPostProcessors.user("user").roles("ADMIN")))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$", hasSize<Any>(vetsDTO.size)))
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<List<VetDTO>>(responseString)
        assertThat(responseDTO, equalTo(vetsDTO))
    }

    @Test
    fun `Test GET One Vet`() {
        Mockito.`when`(vetService.getVetById(1)).thenReturn(vet1)

        val result = mvc.perform(get ("$vetsURL/1").with(SecurityMockMvcRequestPostProcessors.user("user").roles("CLIENT")))
                .andExpect(status().isOk)
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<VetDTO>(responseString)
        assertThat(responseDTO, equalTo(vetsDTO[0]))
    }


    @Test
    fun `Test GET One Vet(Not Found)`() {
        Mockito.`when`(vetService.getVetById(2)).thenThrow(NotFoundException("not found"))

        mvc.perform(get("$vetsURL/2").with(SecurityMockMvcRequestPostProcessors.user("user").roles("CLIENT")))
                .andExpect(status().is4xxClientError)
    }

    @Test
    fun `Test POST One Vet`() {
        val petJSON = mapper.writeValueAsString(vetsDTO[0])

        Mockito.`when`(vetService.getVetById(0L)).thenReturn(vet1)

        Mockito.`when`(vetService.addNewVet(nonNullAny(VetDAO::class.java)))
                .then { assertThat(it.getArgument(0), equalTo(vet1)); it.getArgument(0) }

        Mockito.`when`(vetService.getUserByUsername("antonio")).then { Optional.of(vet1.user!!) }
        mvc.perform(post("$vetsURL/").with(SecurityMockMvcRequestPostProcessors.user("user").roles("ADMIN"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(petJSON))
                .andExpect(status().isOk)
    }

    @Test
    fun `Test DELETE One Vet`() {
        Mockito.`when`(vetService.getVetById(0)).thenReturn(vet1)

        mvc.perform(delete("$vetsURL/1").with(SecurityMockMvcRequestPostProcessors.user("user").roles("ADMIN")))
                .andExpect(status().isOk)
    }

    fun <T> nonNullAny(t: Class<T>): T = Mockito.any(t)

}