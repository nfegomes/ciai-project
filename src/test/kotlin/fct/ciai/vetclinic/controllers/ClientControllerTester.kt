package fct.ciai.vetclinic.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.dto.*
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.services.ClientService
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.Matchers.hasSize
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*


@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class ClientControllerTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var clientService: ClientService

    companion object Constants {
        val mapper: ObjectMapper = ObjectMapper().registerModule(KotlinModule())

        val client1 = ClientDAO("anasilva", LoginInfoDAO("anasilva", "anasilva@sapo.pt", "password"), "Ana Silva", "asd",
                916858310, "Rua das Flores", false, emptyList())

        val client2 = ClientDAO("josesilva", LoginInfoDAO("josesilva", "josesilva@sapo.pt", "password"), "José Silva", "",
                956858699, "Rua das Papoilas",  false, emptyList())

        val clientsDAO = ArrayList(listOf(client1, client2))

        val clientsDTO = clientsDAO.map {
            ClientDTO(it.username, it.name, it.picture, it.cellphone, it.address, it.frozen)
        }

        const val clientsURL = "/clients"
    }

    @Test
    fun `Test GET all clients`() {
        Mockito.`when`(clientService.getAllClients()).thenReturn(clientsDAO)

        val result = mvc.perform(get("$clientsURL/").with(SecurityMockMvcRequestPostProcessors.user("user").roles("ADMIN")))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$", hasSize<Any>(clientsDTO.size)))
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<List<ClientDTO>>(responseString)
        assertThat(responseDTO, equalTo(clientsDTO))
    }

    @Test
    fun `Test GET One Client`() {
        Mockito.`when`(clientService.getClientByUsername("anasilva")).thenReturn(client1)

        val result = mvc.perform(get("$clientsURL/anasilva").with(SecurityMockMvcRequestPostProcessors.user("user").roles("CLIENT")))
                .andExpect(status().isOk)
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<ClientDTO>(responseString)
        assertThat(responseDTO, equalTo(clientsDTO[0]))
    }

    @Test
    fun `Test GET One Client NOT FOUND`() {
        Mockito.`when`(clientService.getClientByUsername("josesilva")).thenThrow(NotFoundException("client not found"))

        val result = mvc.perform(get("$clientsURL/josesilva").with(SecurityMockMvcRequestPostProcessors.user("user").roles("CLIENT")))
                .andExpect(status().is4xxClientError)
                .andReturn()

    }

    fun <T> nonNullAny(t: Class<T>): T = Mockito.any(t)

}