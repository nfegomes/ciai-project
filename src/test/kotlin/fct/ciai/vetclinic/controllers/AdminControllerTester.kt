package fct.ciai.vetclinic.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.dto.AdminDTO
import fct.ciai.vetclinic.api.exceptions.MainAdminException
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.services.AdminService
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.Matchers.hasSize
import org.springframework.http.MediaType
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import java.util.*
import kotlin.collections.ArrayList

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class AdminControllerTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var adminService: AdminService

    companion object Constants {
        val mapper: ObjectMapper = ObjectMapper().registerModule(KotlinModule())

        val admin1 = AdminDAO(2L, LoginInfoDAO("anasilva", "anasilva@sapo.pt", "password"), "Ana Silva", "",
                916858310, "Rua das Flores")

        val admin2 = AdminDAO(3L, LoginInfoDAO("josesilva", "josesilva@sapo.pt", "password"), "José Silva", "",
                956858699, "Rua das Papoilas")

        val adminsDAO = ArrayList(listOf(admin1, admin2))

        val adminsDTO = adminsDAO.map {
            AdminDTO(it.id, it.user!!.username, it.name, it.picture, it.cellphone, it.address)
        }

        const val adminsURL = "/admins"
    }

    @Test
    fun `Test GET all admins`() {
        Mockito.`when`(adminService.getAllAdmins()).thenReturn(adminsDAO)

        val result = mvc.perform(get("$adminsURL/").with(SecurityMockMvcRequestPostProcessors.user("user").roles("ADMIN")))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$", hasSize<Any>(adminsDTO.size)))
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<List<AdminDTO>>(responseString)
        assertThat(responseDTO, equalTo(adminsDTO))
    }

    @Test
    fun `Test GET One Admin`() {
        Mockito.`when`(adminService.getAdminById(2)).thenReturn(admin1)

        val result = mvc.perform(get("$adminsURL/2").with(SecurityMockMvcRequestPostProcessors.user("user").roles("ADMIN")))
                .andExpect(status().isOk)
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<AdminDTO>(responseString)
        assertThat(responseDTO, equalTo(adminsDTO[0]))
    }

    @Test
    fun `Test GET One Admin(Not Found)`() {
        Mockito.`when`(adminService.getAdminById(2)).thenThrow(NotFoundException("not found"))

        mvc.perform(get("$adminsURL/2").with(SecurityMockMvcRequestPostProcessors.user("user").roles("ADMIN")))
                .andExpect(status().is4xxClientError)
    }

    @Test
    fun `Test POST One Admin`() {

        val admin1DTO = AdminDTO(admin1.id, admin1.user!!.username, admin1.name, admin1.picture,
                admin1.cellphone, admin1.address)

        val petJSON = mapper.writeValueAsString(admin1DTO)

        Mockito.`when`(adminService.getAdminById(2)).thenReturn(admin1)

        Mockito.`when`(adminService.addNewAdmin(nonNullAny(AdminDAO::class.java)))
                .then { assertThat(it.getArgument(0), equalTo(admin1)); it.getArgument(0) }
        Mockito.`when`(adminService.getUserByUsername("anasilva")).then { Optional.of(admin1.user!!) }

        mvc.perform(post("$adminsURL/").with(SecurityMockMvcRequestPostProcessors.user("user").roles("ADMIN"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(petJSON))
                .andExpect(status().isOk)
    }

    @Test
    fun `Test DELETE One Admin`() {
        Mockito.`when`(adminService.getAdminById(2)).thenReturn(admin1)

        mvc.perform(delete("$adminsURL/2").with(SecurityMockMvcRequestPostProcessors.user("user").roles("ADMIN")))
                .andExpect(status().isOk)
    }

    @Test
    fun `Test DELETE One Admin error - main admin`() {
        val mainAdmin = AdminDAO(1L,
                LoginInfoDAO("admin", "admin@email.com", "password"),
                "Admin", "www.adminImg.com", 912345678, "admin address")

        Mockito.`when`(adminService.getAdminById(1)).thenReturn(mainAdmin)

        mvc.perform(delete("$adminsURL/1").with(SecurityMockMvcRequestPostProcessors.user("user").roles("ADMIN")))
                .andExpect { MainAdminException::class.java }
    }

    fun <T> nonNullAny(t: Class<T>): T = Mockito.any(t)

}