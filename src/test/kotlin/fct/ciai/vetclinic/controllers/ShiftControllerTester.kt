package fct.ciai.vetclinic.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.dto.AppointmentDTO
import fct.ciai.vetclinic.api.dto.ShiftDTO
import fct.ciai.vetclinic.api.services.ShiftService
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.Matchers.hasSize
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import java.util.*


@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class ShiftControllerTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var shiftService: ShiftService

    companion object Constants {
        val mapper: ObjectMapper = ObjectMapper().registerModule(KotlinModule())

        var firstClient = ClientDAO("gomesnuno", LoginInfoDAO("gomesnuno", "nuno@sapo.pt", "password"), "nuno", "www.picture.com",
                961234567, "address", false, emptyList())

        var firstPet = PetDAO(1L, 123L, "dog", 10, "",
                firstClient, emptyList(), "big dog", "healthy", " ", false)

        val vet1 = VetDAO(1L, LoginInfoDAO("micaelbeco", "micael@sapo.pt", "password"), "Micael", "", 919650923,
                "Rua do Serrado", false, emptyList())

        val shift1 = ShiftDAO(1L, vet1,
                Date(2019, 11, 27, 16, 0),
                Date(2018, 11, 27, 20, 0),
                emptyList())

        val shift2 = ShiftDAO(2L, vet1,
                Date(2019, 11, 28, 16, 0),
                Date(2018, 11, 28, 20, 0),
                emptyList())

        val apt1 = AppointmentDTO(1L, 1L, 1L, 1L,
                Date(2019, 11, 27, 16, 0),
                Date(2018, 11, 27, 20, 0), "consulta de rotina",
                1, false)

        val aptDAO = AppointmentDAO(apt1.id, firstPet, vet1, shift1,
                apt1.startingTime, apt1.endingTime, "consulta de rotina",
                apt1.duration, apt1.completed)

        val listApts = listOf(aptDAO)

        val shiftsDAO = listOf(shift1, shift2)

        const val shiftsURL = "/shifts"
    }

    @Test
    fun `Test GET all shifts`() {
        val shiftsDTO = shiftsDAO.map {
            ShiftDTO(it.id, it.vet.id, it.startDateTime, it.endDateTime)
        }
        Mockito.`when`(shiftService.getAllShifts()).thenReturn(shiftsDAO)

        val result = mvc.perform(get("$shiftsURL/").with(SecurityMockMvcRequestPostProcessors.user("user").roles("ADMIN")))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$", hasSize<Any>(shiftsDTO.size)))
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<List<ShiftDTO>>(responseString)
        assertThat(responseDTO, equalTo(shiftsDTO))
    }

    /*
    @Test
    fun `Test POST One Schedule`() {

        val scheduleJSON = mapper.writeValueAsString(schedule1)

        Mockito.`when`(scheduleService.getVetById(1L)).thenReturn(vet1)

        Mockito.`when`(scheduleService.getAppointmentOfId(1L)).thenReturn(listApts)

        Mockito.`when`(scheduleService.addNewSchedule(nonNullAny(ScheduleDAO::class.java)))
                .then { assertThat(it.getArgument(0), equalTo(schedule1)); it.getArgument(0) }

        mvc.perform(post("$schedulesURL/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(scheduleJSON))
                .andExpect(status().isOk)
    }

*/
    fun <T> nonNullAny(t: Class<T>): T = Mockito.any(t)

}