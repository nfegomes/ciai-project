package fct.ciai.vetclinic.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import fct.ciai.vetclinic.api.controllers.appointments.AppointmentController
import fct.ciai.vetclinic.api.controllers.pets.PetController
import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.dto.AppointmentDTO
import fct.ciai.vetclinic.api.dto.PetDTO
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.exceptions.PreconditionFailedException
import fct.ciai.vetclinic.api.services.PetService
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.Matchers.hasSize
import org.springframework.http.MediaType
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import java.util.*
import kotlin.collections.ArrayList


@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class PetControllerTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var petService: PetService

    companion object Constants {
        val mapper: ObjectMapper = ObjectMapper().registerModule(KotlinModule())

        var firstClient = ClientDAO("gomesnuno", LoginInfoDAO("gomesnuno", "nuno@sapo.pt", "password") ,"nuno", "www.picture.com",
                961234567, "address", false, emptyList())

        var firstPet = PetDAO(1L, 123L, "dog", 10, "",
                firstClient, emptyList(), "big dog", "healthy",
                "medical record", false)

        var secondPet = PetDAO(2L, 321L, "cat", 2, "", firstClient,
                emptyList(), "black cat", "healthy", "medical record", false)

        var firstPetDTO = PetDTO(firstPet.id, firstPet.chipNumber, firstPet.species,
                firstPet.age, firstPet.picture, firstPet.owner.username,
                firstPet.physicalDescription, firstPet.healthStatus,
                firstPet.medicalRecord, firstPet.frozen)

        val petsDAO = ArrayList(listOf(firstPet, secondPet))

        val petsDTO = petsDAO.map {
            PetDTO(it.id, it.chipNumber, it.species, it.age, it.picture, it.owner.username, it.physicalDescription,
                    it.healthStatus, it.medicalRecord, false)
        }

        val vet1 = VetDAO(1L, LoginInfoDAO("micaelbeco", "micael@sapo.pt", "password"), "Micael", "", 919650923,
                "Rua do Serrado",  false, emptyList())

        val shift1 = ShiftDAO(1L, vet1,
                Date(2019, 11, 27, 16, 0),
                Date(2018, 11, 27, 20, 0),
                emptyList())

        val apt = AppointmentDTO(2L, 1L, 1L, 1L,
                Date(2019, 11, 27, 16, 0),
                Date(2019, 11, 27, 17, 0),
                "consulta de rotina",
               1, false)

        val aptDAO = AppointmentDAO(apt.id, firstPet, vet1, shift1,
                apt.startingTime, apt.endingTime, "consulta de rotina",
               apt.duration, apt.completed)


       /* val petsAptsDTO =
                petsDAO.map {
                    PetAptsDTO(
                            PetDTO(it.id, it.chipNumber, it.species,
                                    it.age, it.picture, it.owner.username,
                                    it.physicalDescription,
                                    it.healthStatus, it.medicalRecord, it.frozen),
                            it.appointments.map {
                                AppointmentDTO(it.id, it.pet.id, it.shift.id,
                                        it.vet.id, it.startingTime, it.endingTime, it.description,
                                        it.startingTimeSchedule, it.endingTimeSchedule, it.duration, it.completed)
                            }
                    )
                }*/

        const val petsURL = "/pets"
    }

    @Test
    fun `Test GET all pets`() {
        val firstPet1 = PetDAO(1L, 123L, "dog", 10, "",
                firstClient, emptyList(), "big dog", "healthy",
                "medical record", false)

        val secondPet1 = PetDAO(2L, 321L, "cat", 2, "", firstClient,
                emptyList(), "black cat", "healthy", "medical record", false)

        val petsDAO1 = ArrayList(listOf(firstPet1, secondPet1))

        val petsDTO1 =
                petsDAO1.map {
                    PetDTO(it.id, it.chipNumber, it.species,
                            it.age, it.picture, it.owner.username,
                            it.physicalDescription,
                            it.healthStatus, it.medicalRecord, it.frozen)
                }



        Mockito.`when`(petService.getAllPets()).thenReturn(petsDAO1)

        val result = mvc.perform(get("$petsURL/").with(SecurityMockMvcRequestPostProcessors.user("user").roles("ADMIN")))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$", hasSize<Any>(petsDTO1.size)))
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<List<PetDTO>>(responseString)
        assertThat(responseDTO, equalTo(petsDTO1))
    }

    @Test
    fun `Test GET One Pet`() {
        Mockito.`when`(petService.getPetByID(1)).thenReturn(firstPet)

        val result = mvc.perform(get("$petsURL/1").with(SecurityMockMvcRequestPostProcessors.user("user").roles("CLIENT")))
                .andExpect(status().isOk)
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<PetDTO>(responseString)
        assertThat(responseDTO, equalTo(petsDTO[0]))
    }


    @Test
    fun `Test GET One Pet(Not Found)`() {
        Mockito.`when`(petService.getPetByID(2)).thenThrow(NotFoundException("not found"))

        mvc.perform(get("$petsURL/2").with(SecurityMockMvcRequestPostProcessors.user("user").roles("CLIENT")))
                .andExpect(status().is4xxClientError)

    }

    @Test
    fun `Test POST One Pet`() {

        val louro = PetDTO(0L, 123L, "dog", 10, "",
                "gomesnuno", "big dog", "healthy",
                "medical record", false)

        val louroDAO = PetDAO(louro.id, louro.chipNumber, louro.species, louro.age, louro.picture,
                firstClient, emptyList(), louro.physicalDescription, louro.healthStatus,
                louro.medicalRecord, louro.isFrozen)

        val petJSON = mapper.writeValueAsString(louro)

        Mockito.`when`(petService.getOwnerById("gomesnuno")).thenReturn(firstClient)

        Mockito.`when`(petService.addNewPet(nonNullAny(PetDAO::class.java)))
                .then { assertThat(it.getArgument(0), equalTo(louroDAO)); it.getArgument(0) }

        mvc.perform(post("$petsURL/").with(SecurityMockMvcRequestPostProcessors.user("user").roles("CLIENT"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(petJSON))
                .andExpect(status().isOk)
    }

    @Test
    fun `Test update Pet By Id`() {

        val louro = PetDTO(1L, 123L, "dog", 10, "",
                "gomesnuno", "big dog", "healthy",
                "medical record", false)

        val louroDAO = PetDAO(louro.id, louro.chipNumber, louro.species, louro.age, louro.picture,
                firstClient, emptyList(), louro.physicalDescription, louro.healthStatus,
                louro.medicalRecord, louro.isFrozen)

        val petJSON = mapper.writeValueAsString(louro)

        Mockito.`when`(petService.getOwnerById("gomesnuno")).thenReturn(firstClient)

        Mockito.`when`(petService.updatePetByID(louroDAO, louro.id))
                .then { assertThat(it.getArgument(0), equalTo(louroDAO)); it.getArgument(0) }

        mvc.perform(put("$petsURL/1").with(SecurityMockMvcRequestPostProcessors.user("user").roles("CLIENT"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(petJSON))
                .andExpect(status().isOk)
    }


    @Test
    fun `Test DELETE One Pet`() {
        Mockito.`when`(petService.getPetByID(1)).thenReturn(firstPet)

        mvc.perform(delete("$petsURL/1").with(SecurityMockMvcRequestPostProcessors.user("user").roles("CLIENT")))
                .andExpect(status().isOk)
    }

    fun <T> nonNullAny(t: Class<T>): T = Mockito.any(t)

}