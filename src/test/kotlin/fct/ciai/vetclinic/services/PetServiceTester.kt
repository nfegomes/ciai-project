package fct.ciai.vetclinic.services

import fct.ciai.vetclinic.controllers.AppointmentControllerTester
import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.dto.AppointmentDTO
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.exceptions.PreconditionFailedException
import fct.ciai.vetclinic.api.repository.AppointmentRepository
import fct.ciai.vetclinic.api.repository.PetRepository
import fct.ciai.vetclinic.api.services.PetService
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
class PetServiceTester {

    @Autowired
    lateinit var pets: PetService

    @MockBean
    lateinit var repo: PetRepository

    @MockBean
    lateinit var aptRepo: AppointmentRepository

    companion object Constants {
        var firstClient = ClientDAO("gomesnuno", LoginInfoDAO("gomesnuno", "nuno@sapo.pt", "password"), "nuno", "www.picture.com",
                961234567, "address", false, emptyList())

        var firstPet = PetDAO(1L, 123L, "dog", 10, "",
                AppointmentControllerTester.firstClient, emptyList(), "big dog", "healthy", " ", false)

        var secondPet = PetDAO(2L, 321L, "cat", 2, "", firstClient,
                emptyList(), "black cat", "healthy", "medical record", false)

        val petsDAO = mutableListOf(firstPet, secondPet)

    }

    @Test
    fun `basic test on getAll`() {
        Mockito.`when`(repo.findByFrozenFalse()).thenReturn(petsDAO)

        assertThat(pets.getAllPets(), equalTo(petsDAO as List<PetDAO>))
    }

    @Test
    fun `basic test on getOne`() {
        Mockito.`when`(repo.findById(1L)).thenReturn(Optional.of(firstPet))

        assertThat(pets.getPetByID(1L), equalTo(firstPet))
    }

    @Test(expected = NotFoundException::class)
    fun `test on getOne() exception`() {
        //did not find the desired pet on the DB hence an empty Optional
        Mockito.`when`(repo.findById(anyLong())).thenReturn(Optional.empty())

        pets.getPetByID(0L)
    }

    @Test
    fun `test on adding a new pet`() {
        Mockito.`when`(repo.save(Mockito.any(PetDAO::class.java)))
                .then {
                    val pet: PetDAO = it.getArgument(0)
                    assertThat(pet.id, equalTo(0L))
                    assertThat(pet.chipNumber, equalTo(firstPet.chipNumber))
                    assertThat(pet.species, equalTo(firstPet.species))
                    assertThat(pet.appointments, equalTo(firstPet.appointments))
                    pet
                }

        pets.addNewPet(PetDAO(0L, firstPet.chipNumber, firstPet.species, firstPet.age, firstPet.picture, firstPet.owner,
                firstPet.appointments, firstPet.physicalDescription, firstPet.healthStatus, firstPet.medicalRecord,
                firstPet.frozen))
    }

    @Test(expected = PreconditionFailedException::class)
    fun `test on adding a new pet (Error)`() {
        pets.addNewPet(firstPet) // firstPet has a non-0 id
    }

}