package fct.ciai.vetclinic.services

import fct.ciai.vetclinic.controllers.AppointmentControllerTester
import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.exceptions.PreconditionFailedException
import fct.ciai.vetclinic.api.repository.AppointmentRepository
import fct.ciai.vetclinic.api.repository.ClientRepository
import fct.ciai.vetclinic.api.repository.PetRepository
import fct.ciai.vetclinic.api.services.ClientService
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
class ClientServiceTester {

    @Autowired
    lateinit var clients: ClientService

    @MockBean
    lateinit var repo: ClientRepository

    @MockBean
    lateinit var aptRepo: AppointmentRepository

    @MockBean
    lateinit var petRepo: PetRepository

    companion object Constants {
        var client1 = ClientDAO("gomesnuno", LoginInfoDAO("gomesnuno", "nuno@sapo.pt", "password"), "nuno", "www.picture.com",
                961234567, "address", false, emptyList())

        var client2 = ClientDAO("marianabexiga", LoginInfoDAO("marianabexiga", "mariana@sapo.pt", "password"), "mariana", "www.picture.com",
                921231549, "address", false, emptyList())

        var firstPet = PetDAO(1L, 123L, "dog", 10, "",
                client1, emptyList(), "big dog", "healthy", " ", false)

        var secondPet = PetDAO(2L, 321L, "cat", 2, "", client1,
                emptyList(), "black cat", "healthy", "medical record", false)

        val clientsDAO = mutableListOf(client1, client2)

    }

    @Test
    fun `basic test on getAll`() {
        Mockito.`when`(repo.findByFrozenFalse()).thenReturn(clientsDAO)
        assertThat(clients.getAllClients(), equalTo(clientsDAO as List<ClientDAO>))
    }

    @Test
    fun `basic test on getOne`() {
        Mockito.`when`(repo.findByUsernameAndFrozenIsFalse(client1.username)).thenReturn(Optional.of(client1))

        assertThat(clients.getClientByUsername(client1.username), equalTo(client1))
    }

    @Test(expected = NotFoundException::class)
    fun `test on getOne() exception`() {
        //did not find the desired pet on the DB hence an empty Optional
        Mockito.`when`(repo.findByUsernameAndFrozenIsFalse(client1.username)).thenThrow(NotFoundException("not found"))

        clients.getClientByUsername(client1.username)
    }

    @Test
    fun `test on adding a new client`() {
        Mockito.`when`(repo.save(Mockito.any(ClientDAO::class.java)))
                .then {
                    val client: ClientDAO = it.getArgument(0)
                    assertThat(client.username, equalTo(client1.username))
                    client
                }

        clients.addNewClient(client1)
    }


    @Test(expected = PreconditionFailedException::class)
    fun `test on adding a new client (Error)`() {
        val client = ClientDAO(client1.username,null, client1.name,client1.picture,client1.cellphone,client1.address,client1.frozen, client1.pets)
        clients.addNewClient(client)
    }

}