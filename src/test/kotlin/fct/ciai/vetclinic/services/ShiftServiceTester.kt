package fct.ciai.vetclinic.services

import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.exceptions.*
import fct.ciai.vetclinic.api.repository.*
import fct.ciai.vetclinic.api.services.ShiftService
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
class ShiftServiceTester {

    @Autowired
    lateinit var shifts: ShiftService

    @MockBean
    lateinit var repo: ShiftRepository

    @MockBean
    lateinit var vetRepo: VetRepository

    companion object Constants {

        val vet1 = VetDAO(0L, LoginInfoDAO("micaelbeco", "micael@gmail.com", "password"),
                "Micael", "", 919650923, "Rua do Serrado", false, emptyList())

        val shift1 = ShiftDAO(0L, vet1,
                Date(2020, 11, 27, 16, 0),
                Date(2020, 11, 27, 20, 0),
                emptyList())

        val vetFrozen = VetDAO(0L, LoginInfoDAO("micaelbeco", "micael@gmail.com", "password"),
                "Micael", "", 919650923, "Rua do Serrado", true, emptyList())

        val shiftsDAO = listOf(shift1)
    }

    @Test
    fun `basic test on getAll`() {
        Mockito.`when`(repo.findAll()).thenReturn(shiftsDAO)
        assertThat(shifts.getAllShifts(), equalTo(shiftsDAO))
    }

    @Test
    fun `basic test on getOne`() {
        Mockito.`when`(repo.findById(shift1.id)).thenReturn(Optional.of(shift1))

        assertThat(shifts.getShiftById(shift1.id), equalTo(shift1))
    }


    @Test(expected = NotFoundException::class)
    fun `test on getOne() exception`() {
        //did not find the desired pet on the DB hence an empty Optional
        Mockito.`when`(repo.findById(anyLong())).thenReturn(Optional.empty())

        shifts.getShiftById(shift1.id)
    }

    @Test
    fun `test on adding a schedule - success`() {
        Mockito.`when`(repo.save(Mockito.any(ShiftDAO::class.java)))
                .then {
                    val shift: ShiftDAO = it.getArgument(0)
                    assertThat(shift.id, equalTo(shift1.id))
                    shift
                }

        shifts.updateSchedule(listOf(shift1))
    }

    @Test(expected = PreconditionFailedException::class)
    fun `test on adding a schedule - error - id not 0`() {
        val shift = ShiftDAO(1L, vetFrozen,
                Date(2020, 11, 27, 16, 0),
                Date(2020, 11, 27, 20, 0),
                emptyList())

        Mockito.`when`(repo.save(Mockito.any(ShiftDAO::class.java)))
                .then {
                    val shiftError: ShiftDAO = it.getArgument(0)
                    assertThat(shiftError.id, equalTo(shiftError.id))
                }

        shifts.updateSchedule(listOf(shift))
    }

    @Test(expected = UserIsFrozenException::class)
    fun `test on adding a schedule - error - vet frozen`() {
        val shift = ShiftDAO(0L, vetFrozen,
                Date(2020, 11, 27, 16, 0),
                Date(2020, 11, 27, 20, 0),
                emptyList())

        Mockito.`when`(repo.save(Mockito.any(ShiftDAO::class.java)))
                .then {
                    val shiftError: ShiftDAO = it.getArgument(0)
                    assertThat(shiftError.id, equalTo(shiftError.id))
                }

        shifts.updateSchedule(listOf(shift))
    }

    @Test(expected = ShiftLimitException::class)
    fun `test on adding a schedule - error - shift 12h`() {
        val shift = ShiftDAO(0L, vet1,
                Date(2020, 11, 27, 10, 0),
                Date(2020, 11, 27, 22, 1),
                emptyList())

        Mockito.`when`(repo.save(Mockito.any(ShiftDAO::class.java)))
                .then {
                    val shiftError: ShiftDAO = it.getArgument(0)
                    assertThat(shiftError.id, equalTo(shiftError.id))
                }

        shifts.updateSchedule(listOf(shift))
    }

    @Test(expected = BeforeTodaysDateException::class)
    fun `test on adding a schedule - error - today's date`() {
        val cal: Calendar = Calendar.getInstance()
        cal.set(2018, 10, 27, 10, 0)
        val cal2: Calendar = Calendar.getInstance()
        cal2.set(2018, 10, 27, 22, 0)

        val shift = ShiftDAO(0L, vet1,
                cal.time,
                cal2.time,
                emptyList())

        Mockito.`when`(repo.save(Mockito.any(ShiftDAO::class.java)))
                .then {
                    val shiftError: ShiftDAO = it.getArgument(0)
                    assertThat(shiftError.id, equalTo(shiftError.id))
                }

        shifts.updateSchedule(listOf(shift))
    }

    @Test(expected = WeeklyLimitScheduleException::class)
    fun `test on adding a schedule - error - weekly limit`() {
        // 14 shifts (40 monthly - 12h per shift)

        val list: MutableList<ShiftDAO> = mutableListOf()

        var aux = 1
        for (i in 0..13 step 2) {
            list.add(i, ShiftDAO(0L, vet1,
                    Date(2020, 11, aux, 0, 0),
                    Date(2020, 11, aux, 12, 0),
                    emptyList()))
            list.add(i + 1, ShiftDAO(0L, vet1,
                    Date(2020, 11, aux, 12, 0),
                    Date(2020, 11, aux + 1, 0, 0),
                    emptyList()))
            aux++
        }

        Mockito.`when`(repo.save(Mockito.any(ShiftDAO::class.java)))
                .then {
                    val shiftError: ShiftDAO = it.getArgument(0)
                    assertThat(shiftError.id, equalTo(shiftError.id))
                }

        shifts.updateSchedule(list)
    }

    @Test(expected = MonthlyLimitScheduleException::class)
    fun `test on adding a schedule - error - monthly limit`() {
        val list: MutableList<ShiftDAO> = mutableListOf()

        var aux = 1
        var totalHours = 0.0
        for (i in 0..83 step 3) {
            list.add(i, ShiftDAO(0L, vet1,
                    Date(2020, 11, aux, 0, 0),
                    Date(2020, 11, aux, 2, 0),
                    emptyList()))
            list.add(i + 1, ShiftDAO(0L, vet1,
                    Date(2020, 11, aux, 10, 0),
                    Date(2020, 11, aux, 13, 0),
                    emptyList()))
            list.add(i + 2, ShiftDAO(0L, vet1,
                    Date(2020, 11, aux, 21, 0),
                    Date(2020, 11, aux, 21, 42),
                    emptyList()))
            aux++
            totalHours += 5.71
        }

        list.add(84, ShiftDAO(0L, vet1,
                Date(2020, 11, aux, 0, 0),
                Date(2020, 11, aux, 2, 0),
                emptyList()))


        Mockito.`when`(repo.save(Mockito.any(ShiftDAO::class.java)))
                .then {
                    val shiftError: ShiftDAO = it.getArgument(0)
                    assertThat(shiftError.id, equalTo(shiftError.id))
                }

        shifts.updateSchedule(list)
    }
}