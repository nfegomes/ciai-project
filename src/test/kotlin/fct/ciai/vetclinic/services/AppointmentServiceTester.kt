package fct.ciai.vetclinic.services

import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.dto.AppointmentDTO
import fct.ciai.vetclinic.api.exceptions.AppointmentCompletedException
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.exceptions.PreconditionFailedException
import fct.ciai.vetclinic.api.repository.AppointmentRepository
import fct.ciai.vetclinic.api.services.AppointmentService
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
class AppointmentServiceTester {

    @Autowired
    lateinit var apts: AppointmentService

    @MockBean
    lateinit var repo: AppointmentRepository

    companion object Constants {
        var client1 = ClientDAO("gomesnuno", LoginInfoDAO("gomesnuno", "nuno@gmail.com", "password"),"nuno", "www.picture.com",
                961234567, "address", false, emptyList())

        var firstPet = PetDAO(1L, 123L, "dog", 10, "",
                client1, emptyList(), "big dog", "healthy", " ", false)

        val vet1 = VetDAO(1L, LoginInfoDAO("micaelbeco", "micael@gmail.com", "password"), "Micael", "", 919650923,
                "Rua do Serrado", false, emptyList())

        val shift1 = ShiftDAO(1L, vet1,
                Date(2019, 11, 27, 16, 0),
                Date(2018, 11, 27, 20, 0),
                emptyList())
        val apt1 = AppointmentDTO(0L, 1L, 1L, 1L,
                Date(2019, 11, 27, 16, 0),
                Date(2018, 11, 27, 20, 0), "consulta de rotina",
                1, false)

        val aptDAO = AppointmentDAO(apt1.id, firstPet, vet1, shift1,
                apt1.startingTime, apt1.endingTime, "consulta de rotina",
                apt1.duration, apt1.completed)

        val aptDAO_completed = AppointmentDAO(2L, firstPet, vet1, shift1,
                apt1.startingTime, apt1.endingTime, "consulta de rotina",
                apt1.duration, true)

        val aptsDAO = listOf(aptDAO, aptDAO_completed)
    }

    @Test
    fun `basic test on getAll`() {
        Mockito.`when`(repo.findAll()).thenReturn(aptsDAO)
        assertThat(apts.getAllAppointments(), equalTo(aptsDAO))
    }

    @Test
    fun `basic test on getOne`() {
        Mockito.`when`(repo.findById(apt1.id)).thenReturn(Optional.of(aptDAO))

        assertThat(apts.getAppointmentById(apt1.id), equalTo(aptDAO))
    }

    @Test(expected = NotFoundException::class)
    fun `test on getOne() exception`() {
        //did not find the desired appointment on the DB hence an empty Optional
        Mockito.`when`(repo.findById(anyLong())).thenReturn(Optional.empty())

        apts.getAppointmentById(apt1.id)
    }

    @Test
    fun `test on adding a new appointment`() {
        Mockito.`when`(repo.save(Mockito.any(AppointmentDAO::class.java)))
                .then {
                    val apt:AppointmentDAO = it.getArgument(0)
                    assertThat(apt.description, equalTo(apt1.description))
                    apt
                }

        apts.addNewAppointment(aptDAO)
    }

    @Test(expected = PreconditionFailedException::class)
    fun `test on adding a new appointment (Error)`() {
        val apt =  AppointmentDAO(1L, firstPet, vet1, shift1,
        apt1.startingTime, apt1.endingTime, "consulta de rotina",
       apt1.duration, apt1.completed)
        apts.addNewAppointment(apt)
    }
}