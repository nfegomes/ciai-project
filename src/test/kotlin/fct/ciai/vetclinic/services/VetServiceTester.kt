package fct.ciai.vetclinic.services

import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.dto.VetDTO
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.exceptions.PreconditionFailedException
import fct.ciai.vetclinic.api.repository.VetRepository
import fct.ciai.vetclinic.api.services.LoginService
import fct.ciai.vetclinic.api.services.VetService
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
class VetServiceTester {

    @Autowired
    lateinit var vets: VetService

    @Autowired
    lateinit var loginService: LoginService

    @MockBean
    lateinit var repo: VetRepository

    companion object Constants {
        val vet1 = VetDAO(0L, LoginInfoDAO("micaelbeco", "micael@gmail.com", "password"), "Micael", "", 919650923,
                "Rua do Serrado", false, emptyList())

        var vet2 = VetDAO(1L, LoginInfoDAO("ana", "ana@gmail.com", "password"), "Ana", "www.google.com",
                9192953946, "Rua das Flores", false, emptyList())


        var vetsDAO = listOf(vet1, vet2)

        val vetsDTO = vetsDAO.map {
            VetDTO(it.id, it.user!!.username, it.name, it.picture, it.cellphone,
                    it.address, it.frozen)
        }
    }

    @Test
    fun `basic test on getAll`() {
        Mockito.`when`(repo.findByFrozenFalse()).thenReturn(vetsDAO)
        assertThat(vets.getAllVets(), equalTo(vetsDAO as List<VetDAO>))
    }

    @Test
    fun `basic test on getOne`() {
        Mockito.`when`(repo.findByIdAndFrozenFalse(vet1.id)).thenReturn(Optional.of(vet1))

        assertThat(vets.getVetById(vet1.id), equalTo(vet1))
    }

    @Test(expected = NotFoundException::class)
    fun `test on getOne() exception`() {
        //did not find the desired pet on the DB hence an empty Optional
        Mockito.`when`(repo.findByIdAndFrozenFalse(anyLong())).thenReturn(Optional.empty())

        vets.getVetById(vet1.id)
    }

    @Test
    fun `test on adding a new vet`() {
        Mockito.`when`(repo.save(Mockito.any(VetDAO::class.java)))
                .then {
                    val vet: VetDAO = it.getArgument(0)
                    vet
                }
        vets.addNewVet(vet1)
    }


    @Test(expected = PreconditionFailedException::class)
    fun `test on adding a new vet (Error)`() {
        vets.addNewVet(vet2)
    }


}