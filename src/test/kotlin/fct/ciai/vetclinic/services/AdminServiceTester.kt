package fct.ciai.vetclinic.services

import fct.ciai.vetclinic.controllers.AppointmentControllerTester
import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.dto.AdminDTO
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.exceptions.PreconditionFailedException
import fct.ciai.vetclinic.api.repository.AdminRepository
import fct.ciai.vetclinic.api.repository.AppointmentRepository
import fct.ciai.vetclinic.api.repository.ClientRepository
import fct.ciai.vetclinic.api.repository.PetRepository
import fct.ciai.vetclinic.api.services.AdminService
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
class AdminServiceTester {

    @Autowired
    lateinit var admins: AdminService

    @MockBean
    lateinit var repo: AdminRepository

    companion object Constants {
        val admin1 = AdminDAO(0L, LoginInfoDAO("anasilva", "anasilva@sapo.pt", "password"), "Ana Silva", "",
                916858310, "Rua das Flores")

        val admin2 = AdminDAO(2L,  LoginInfoDAO("josesilva", "josesilva@gmail.com", "password"), "José Silva", "",
                956858699, "Rua das Papoilas")

        val adminsDAO = ArrayList(listOf(admin1, admin2))

        val adminsDTO = adminsDAO.map {
            AdminDTO(it.id, it.user!!.username, it.name, it.picture, it.cellphone, it.address)
        }
    }

    @Test
    fun `basic test on getAll`() {
        Mockito.`when`(repo.findAll()).thenReturn(adminsDAO)
        assertThat(admins.getAllAdmins(), equalTo(adminsDAO as List<AdminDAO>))
    }

    @Test
    fun `basic test on getOne`() {
        Mockito.`when`(repo.findById(admin1.id)).thenReturn(Optional.of(admin1))

        assertThat(admins.getAdminById(admin1.id), equalTo(admin1))
    }

    @Test(expected = NotFoundException::class)
    fun `test on getOne() exception`() {
        //did not find the desired pet on the DB hence an empty Optional
        Mockito.`when`(repo.findById(anyLong())).thenReturn(Optional.empty())

        admins.getAdminById(admin1.id)
    }

    @Test
    fun `test on adding a new admin`() {
        Mockito.`when`(repo.save(Mockito.any(AdminDAO::class.java)))
                .then {
                    val admin: AdminDAO = it.getArgument(0)
                    assertThat(admin.id, equalTo(admin1.id))
                    admin
                }

        admins.addNewAdmin(admin1)
    }

    @Test(expected = PreconditionFailedException::class)
    fun `test on adding a new admin (Error)`() {
        admins.addNewAdmin(admin2)
    }


}