package fct.ciai.vetclinic.repositories

import fct.ciai.vetclinic.api.dao.AdminDAO
import fct.ciai.vetclinic.api.dao.LoginInfoDAO
import fct.ciai.vetclinic.api.dao.VetDAO
import fct.ciai.vetclinic.api.repository.AdminRepository
import fct.ciai.vetclinic.api.repository.EmployeeRepository
import fct.ciai.vetclinic.api.repository.VetRepository
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.not
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class EmployeeRepositoryTester {

    @Autowired
    lateinit var employees: EmployeeRepository

    @Autowired
    lateinit var vets: VetRepository

    @Autowired
    lateinit var admins: AdminRepository

    companion object Constants {
        var vet1 = VetDAO(-1L,  LoginInfoDAO("joao", "joao@gmail.com", "password"),"Joao", "www.google.com",
                919233946, "Rua das Flores",  false, emptyList())
        var vet2 = VetDAO(-1L, LoginInfoDAO("ana", "ana@gmail.com", "password"),"Ana", "www.google.com",
                9112953946, "Rua das Flores",  false, emptyList())
        var admin1 = AdminDAO(-1L, LoginInfoDAO("antonio", "antonio@gmail.com", "password") ,"Antonio", "www.google.com",
                919293246, "Rua das Flores")
        var admin2 = AdminDAO(-1L,  LoginInfoDAO("celia", "celia@gmail.com", "password"),"Celia", "www.google.com",
                9192953946, "Rua das Flores")
    }


    @Test
    @DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
    fun `basic test on findAll`() {
        assertThat(employees.findAll(), equalTo(emptyList()))
    }


    @Test
    @DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
    fun `Test on Inheritance Save and delete`() {
        val vet = vets.save(vet1)
        assertThat(vet.id, not(equalTo(vet1.id)))
        assertThat(employees.findAll().size, equalTo(1))

        vets.delete(vet)

        assertThat(vets.findByFrozenFalse(), equalTo(emptyList()))
        assertThat(employees.findAll().size, equalTo(0))
    }

    @Test
    @DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
    fun `Test on Inheritance 2`() {
        val vet = vets.save(vet1)
        val admin = admins.save(admin1)
        assertThat(vet.id, not(equalTo(vet1.id)))
        assertThat(employees.findAll().size, equalTo(2))

        vets.delete(vet)

        assertThat(vets.findByFrozenFalse(), equalTo(emptyList()))
        assertThat(employees.findAll().size, equalTo(1))

        admins.delete(admin)
        assertThat(employees.findAll().size, equalTo(0))
    }
}