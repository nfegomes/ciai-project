package fct.ciai.vetclinic.repositories

import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.repository.*
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
@WithMockUser(username = "user", password = "password", roles = ["USER"])
class AppointmentRepositoryTester {

    @Autowired
    lateinit var apts: AppointmentRepository

    @Autowired
    lateinit var clients: ClientRepository

    @Autowired
    lateinit var pets: PetRepository

    @Autowired
    lateinit var vets: VetRepository

    @Autowired
    lateinit var shifts: ShiftRepository

    companion object Constants {
        var firstClient = ClientDAO("gomesnuno", LoginInfoDAO("gomesnuno", "nuno@gmail.com", "password"), "nuno", "www.picture.com",
                961234567, "address", false, emptyList())

        var firstPet = PetDAO(0L, 123L, "dog", 10, "",
                firstClient, emptyList(), "big dog", "healthy", " ", false)

        var vet1 = VetDAO(0L, LoginInfoDAO("antonio", "antonio@gmail.com", "password"), "Antonio", "www.google.com",
                919293946, "Rua das Flores", false, emptyList())

        var shift1 = ShiftDAO(0L, vet1, Date(2019, 11, 27, 16, 0),
                Date(2018, 11, 27, 20, 0), emptyList())

        var apt1 = AppointmentDAO(0L, firstPet, vet1, shift1, Date(2019, 11, 27, 16, 0),
                Date(2018, 11, 27, 20, 0), "apt de rotina", 4, false)

        var secondPet = PetDAO(0L, 124L, "dog", 10, "",
                firstClient, emptyList(), "big dog", "healthy", " ", false)

        var vet2 = VetDAO(0L, LoginInfoDAO("antonia", "antonia@gmail.com", "password"), "Antonia", "www.google.com",
                919293949, "Rua das Flores", false, emptyList())

        var shift2 = ShiftDAO(0L, vet1, Date(2019, 11, 28, 16, 0),
                Date(2018, 11, 28, 20, 0), emptyList())

        var apt2 = AppointmentDAO(0L, secondPet, vet2, shift2, Date(2019, 11, 28, 16, 0),
                Date(2018, 11, 28, 20, 0), "apt de rotina", 4, true)
    }

    // IMPORTANT: the database is not cleaned between tests, it means that it will keep the pets
    // saved in previous tests

    @Test
    @DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
    fun `basic test on findAll`() {
        assertThat(apts.findAll(), equalTo(emptyList()))
    }

    @Test
    @DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
    fun `basic test on find pet appointments`() {
        assertThat(apts.findByPetId(1L).toList().size, equalTo(0))
    }

    @Test
    @DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
    fun `basic test on find vet apts 0`() {
        assertThat(apts.findByVetId(vet1.id).toList().size, equalTo(0))
    }

    @Test
    @DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
    fun `basic test on find vet apts 1`() {
        clients.save(firstClient)
        val pet = pets.save(firstPet)
        val vet = vets.save(vet1)
        shift1.vet = vet
        val shift = shifts.save(shift1)
        apt1.shift = shift
        apt1.vet = vet
        apt1.pet = pet
        apts.save(apt1)
        assertThat(apts.findByVetId(vet.id).toList().size, equalTo(1))
    }

    @Test
    @DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
    fun `test on find pet apts 1`() {
        clients.save(firstClient)
        val pet = pets.save(firstPet)
        val vet = vets.save(vet1)
        shift1.vet = vet
        val shift = shifts.save(shift1)
        apt1.shift = shift
        apt1.vet = vet
        apt1.pet = pet
        apts.save(apt1)
        assertThat(apts.findByPetId(pet.id).toList().size, equalTo(1))
    }

    @Test
    @DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
    fun `basic test on save and delete`() {
        clients.save(firstClient)
        val pet = pets.save(firstPet)
        val vet = vets.save(vet1)
        shift1.vet = vet
        val shift = shifts.save(shift1)
        apt1.shift = shift
        apt1.vet = vet
        apt1.pet = pet
        val apt = apts.save(apt1)
        assertThat(apts.findByPetId(pet.id).toList().size, equalTo(1))

        apts.delete(apt)

        assertThat(apts.findAll(), equalTo(emptyList()))
    }

    @Test
    @DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
    fun `basic test on find non-completed and completed appointments`() {
        clients.save(firstClient)
        val pet = pets.save(firstPet)
        val vet = vets.save(vet1)
        shift1.vet = vet
        val shift = shifts.save(shift1)

        val pet2 = pets.save(secondPet)
        val vet2 = vets.save(vet2)
        shift2.vet = vet
        val shift2 = shifts.save(shift2)

        apt1.shift = shift
        apt1.vet = vet
        apt1.pet = pet
        apts.save(apt1)
        apt2.shift = shift2
        apt2.vet = vet2
        apt2.pet = pet2
        apts.save(apt2)

        val nonCompleted: MutableIterator<AppointmentDAO> = apts.findByCompleted(false).iterator()
        val one: AppointmentDAO = nonCompleted.next()
        assertThat(one.id, equalTo(apt1.id))

        val completed: MutableIterator<AppointmentDAO> = apts.findByCompleted(true).iterator()
        val two: AppointmentDAO = completed.next()
        assertThat(two.id, equalTo(apt2.id))
    }

    @Test
    @DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
    fun `basic test on get client appointments`() {
        clients.save(firstClient)
        val pet = pets.save(firstPet)
        val vet = vets.save(vet1)
        shift1.vet = vet
        val shift = shifts.save(shift1)

        val pet2 = pets.save(secondPet)
        val vet2 = vets.save(vet2)
        shift2.vet = vet
        val shift2 = shifts.save(shift2)

        apt1.shift = shift
        apt1.vet = vet
        apt1.pet = pet
        apts.save(apt1)
        apt2.shift = shift2
        apt2.vet = vet2
        apt2.pet = pet2
        apts.save(apt2)

        val nonCompleted: MutableIterator<AppointmentDAO> = apts. findByCompletedAndClientUsername(false, "gomesnuno").iterator()
        val one: AppointmentDAO = nonCompleted.next()
        assertThat(one.description, equalTo(apt1.description))
        assertThat(one.id, equalTo(apt1.id))
    }
}