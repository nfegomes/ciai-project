package fct.ciai.vetclinic.config

import fct.ciai.vetclinic.api.services.LoginService
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service


class CustomUserDetails(
        private val aUsername: String,
        private val aPassword: String,
        private val someAuthorities: MutableCollection<out GrantedAuthority>) : UserDetails {

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> = someAuthorities

    override fun isEnabled(): Boolean = true

    override fun getUsername(): String = aUsername

    override fun isCredentialsNonExpired(): Boolean = true

    override fun getPassword(): String = aPassword

    override fun isAccountNonExpired(): Boolean = true

    override fun isAccountNonLocked(): Boolean = true
}


@Service
class CustomUserDetailsService(
        val loginService: LoginService
) : UserDetailsService {

    override fun loadUserByUsername(username: String?): UserDetails {

        username?.let {
            val userDAO = loginService.findByUsername(it)
            if (userDAO.isPresent) {
                val user = userDAO.get()
                var role: String
                role = if (user.client != null && user.employee == null)
                    "ROLE_CLIENT"
                else if (user.employee != null && user.client == null)
                    "ROLE_" + loginService.getEmployeeType(user.username)  //ROLE_VET or ROLE_ADMIN
                else
                    "ROLE_UNASSIGNED"
                println(role)
                return CustomUserDetails(user.username, user.password, mutableListOf(SimpleGrantedAuthority(role)))
            } else
                throw UsernameNotFoundException(username)
        }
        throw UsernameNotFoundException(username)
    }
}