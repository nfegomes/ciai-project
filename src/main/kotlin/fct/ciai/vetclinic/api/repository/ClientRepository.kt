package fct.ciai.vetclinic.api.repository

import fct.ciai.vetclinic.api.dao.ClientDAO
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*


@Repository
interface ClientRepository: JpaRepository<ClientDAO, String>{

    @Query("select client from ClientDAO client where client.username = :username and client.frozen = false")
    fun findByUsernameAndFrozenIsFalse(username: String) : Optional<ClientDAO>

    fun findByFrozenFalse():List<ClientDAO>

}