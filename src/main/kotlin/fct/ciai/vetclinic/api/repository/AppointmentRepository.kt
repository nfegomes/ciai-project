package fct.ciai.vetclinic.api.repository

import fct.ciai.vetclinic.api.dao.AppointmentDAO
import fct.ciai.vetclinic.api.dao.VetDAO
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository


@Repository
interface AppointmentRepository : JpaRepository<AppointmentDAO, Long> {
    // get all appointments related to the given pet
    @Query("select apt from AppointmentDAO apt left join fetch apt.pet where apt.pet.id = :petId")
    fun findByPetId(petId: Long): MutableIterable<AppointmentDAO>

    @Query("select apt from AppointmentDAO apt left join fetch apt.vet where apt.vet.id = :vetId")
    fun findByVetId(vetId: Long): MutableIterable<AppointmentDAO>

    @Query("select apt from AppointmentDAO apt left join fetch apt.pet where apt.pet.owner.username = :username")
    fun findByClientUsername(username: String): MutableIterable<AppointmentDAO>

    fun findByCompleted(completed: Boolean): MutableIterable<AppointmentDAO>

    @Query("select apt from AppointmentDAO apt left join fetch apt.vet where apt.vet.id = :vetId and apt.completed = :completed")
    fun findByCompletedAndVetId(completed: Boolean, vetId: Long): MutableIterable<AppointmentDAO>

    @Query("select apt from AppointmentDAO apt left join fetch apt.pet where apt.pet.id = :petId and apt.completed = :completed")
    fun findByCompletedAndPetId(completed: Boolean, petId: Long): MutableIterable<AppointmentDAO>

    @Query("select apt from AppointmentDAO apt left join fetch apt.pet where apt.pet.owner.username = :username and apt.completed = :completed")
    fun findByCompletedAndClientUsername(completed: Boolean, username: String): MutableIterable<AppointmentDAO>

}
