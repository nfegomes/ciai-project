package fct.ciai.vetclinic.api.repository

import fct.ciai.vetclinic.api.dao.ShiftDAO
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ShiftRepository : JpaRepository<ShiftDAO, Long> {
    fun findByVetId(vetId : Long) : List<ShiftDAO>
}