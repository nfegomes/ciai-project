package fct.ciai.vetclinic.api.repository

import fct.ciai.vetclinic.api.dao.LoginInfoDAO
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface LoginRepository : JpaRepository<LoginInfoDAO, String>




