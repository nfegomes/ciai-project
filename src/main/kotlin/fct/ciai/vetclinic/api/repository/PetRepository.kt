package fct.ciai.vetclinic.api.repository

import fct.ciai.vetclinic.api.dao.ClientDAO
import fct.ciai.vetclinic.api.dao.PetDAO
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*


@Repository
interface PetRepository : JpaRepository<PetDAO, Long> {
    // A query that loads all Pets with prefetching of the appointments associated
    @Query("select pet from PetDAO pet left join fetch pet.appointments where pet.id = :id and pet.frozen = false")
    fun findByIdWithAppointmentsAndFrozenIsFalse(id: Long): Optional<PetDAO>

    fun findByFrozenFalse(): List<PetDAO>

    fun findByOwner(owner: ClientDAO): List<PetDAO>
}