package fct.ciai.vetclinic.api.repository

import fct.ciai.vetclinic.api.dao.*
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface EmployeeRepository: JpaRepository<EmployeeDAO, Long>{
    @Query("select emp from EmployeeDAO emp where emp.frozen = false or emp.frozen IS NULL ")
    fun findAllActive(): List<EmployeeDAO>

    @Query("select emp_type from employee e where e.username = :username ", nativeQuery = true)
    fun findRoleOfUser (username:String) : Optional<String>
}