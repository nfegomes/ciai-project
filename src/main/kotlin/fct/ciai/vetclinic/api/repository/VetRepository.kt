package fct.ciai.vetclinic.api.repository

import fct.ciai.vetclinic.api.dao.VetDAO
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface VetRepository: JpaRepository<VetDAO, Long> {
    fun findByFrozenFalse(): List<VetDAO>

    fun findByIdAndFrozenFalse(id: Long): Optional<VetDAO>
}