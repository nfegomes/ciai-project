package fct.ciai.vetclinic.api.repository

import fct.ciai.vetclinic.api.dao.AdminDAO
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AdminRepository : JpaRepository<AdminDAO, Long>