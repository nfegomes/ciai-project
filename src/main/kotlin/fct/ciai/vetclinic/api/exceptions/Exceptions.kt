package fct.ciai.vetclinic.api.exceptions

/*
 * This file contains general exceptions
 */

class NotFoundException(s: String) : RuntimeException(s)

class PreconditionFailedException(s: String) : RuntimeException(s)

class UserIsFrozenException(s: String) : RuntimeException(s)

class PetIsFrozenException(s: String) : RuntimeException(s)

class ShiftWithAppointmentsException(s: String) : RuntimeException(s)

class AppointmentCompletedException(s: String) : RuntimeException(s)

class MainAdminException(s: String) : RuntimeException(s)

class VetWithAppointmentsException(s: String) : RuntimeException(s)

/**********************Shifts restrictions exceptions**********************/
class ShiftLimitException(s: String) : RuntimeException(s)

class BeforeTodaysDateException(s: String) : RuntimeException(s)

class MonthlyLimitScheduleException(s: String) : RuntimeException(s)

class TimeBetweenShiftsException(s: String) : RuntimeException(s)

class WeeklyLimitScheduleException(s: String) : RuntimeException(s)
/**************************************************************************/