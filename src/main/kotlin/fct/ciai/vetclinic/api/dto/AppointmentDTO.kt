package fct.ciai.vetclinic.api.dto

import java.util.*

data class AppointmentDTO(val id: Long,
                          val petId: Long,
                          val shiftId: Long,
                          val vetId: Long,
                          val startingTime: Date,
                          val endingTime:  Date,
                          var description: String,
                          var duration: Number,
                          val completed: Boolean)

// Secondary constructor is missing because AppointmentDAO needs
// the objects and not ids/username (applied to the following fields:
// this.peTId, this.vetId and this.clientUsername);
// It's done on the Controller and Service.
