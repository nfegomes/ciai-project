package fct.ciai.vetclinic.api.dto


data class  ClientPetsDTO(
        val client: ClientDTO,
        var pets: List<PetDTO>?
)
