package fct.ciai.vetclinic.api.dto

data class VetDTO(val id: Long,
                  val username: String,
                  val name: String,
                  val picture: String,
                  val cellphone: Number,
                  val address: String,
                  var isFrozen: Boolean)