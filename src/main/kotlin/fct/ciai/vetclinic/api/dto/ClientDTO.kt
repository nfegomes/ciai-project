package fct.ciai.vetclinic.api.dto

data class ClientDTO(val username: String,
                     val name: String,
                     val picture: String?,
                     val cellphone: Number,
                     val address: String,
                     var isFrozen: Boolean
)