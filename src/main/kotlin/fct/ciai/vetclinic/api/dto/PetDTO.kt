package fct.ciai.vetclinic.api.dto

data class PetDTO(val id: Long,
                  val chipNumber: Long,
                  val species: String,
                  var age: Number,
                  var picture: String,
                  val ownerUsername: String,
                  var physicalDescription: String,
                  var healthStatus: String,
                  var medicalRecord: String,
                  var isFrozen: Boolean)
