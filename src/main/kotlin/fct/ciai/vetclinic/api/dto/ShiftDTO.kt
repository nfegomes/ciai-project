package fct.ciai.vetclinic.api.dto

import java.util.*

data class ShiftDTO(val id: Long,
                    val vetID: Long,
                    val startDateTime: Date,
                    val endDateTime: Date
)
