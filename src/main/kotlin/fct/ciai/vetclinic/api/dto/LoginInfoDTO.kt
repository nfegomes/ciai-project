package fct.ciai.vetclinic.api.dto

class LoginInfoDTO(val username: String, val password: String, val email: String? =null, val client:String? =null, val employee: Long?=null)