package fct.ciai.vetclinic.api.dto

import com.fasterxml.jackson.annotation.JsonProperty


data class SignupClientDTO(
        @JsonProperty("username")
        val username: String,
        @JsonProperty("email")
        val email: String,
        @JsonProperty("password")
        val password: String,
        @JsonProperty("name")
        val name: String,
        @JsonProperty("picture")
        val picture: String? = null,
        @JsonProperty("cellphone")
        val cellphone: Long,
        @JsonProperty("address")
        val address: String
)
