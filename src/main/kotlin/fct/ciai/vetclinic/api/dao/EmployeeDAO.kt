package fct.ciai.vetclinic.api.dao

import javax.persistence.*

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Entity
@Table(name = "Employee")
@DiscriminatorColumn(name = "EMP_TYPE")
open class EmployeeDAO(

        @Id
        @GeneratedValue
        @Column(name = "id")
        open val id: Long,

        @OneToOne(cascade = [CascadeType.ALL])
        @JoinColumn(name = "username", unique = true)
        override var user: LoginInfoDAO?,

        @Column(name = "name")
        override val name: String,

        @Column(name = "picture")
        override val picture: String,

        @Column(name = "cellphone", unique = true)
        override var cellphone: Number,

        @Column(name = "address")
        override var address: String


)

    : UserDAO() {

    fun update(other: EmployeeDAO) {
        super.update(other)
    }
}