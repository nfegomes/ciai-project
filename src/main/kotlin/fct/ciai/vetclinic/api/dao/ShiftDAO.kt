package fct.ciai.vetclinic.api.dao

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.util.*
import javax.persistence.*

/**
 * Shift DAO handles the process to communicate with the database of shifts
 */
@Entity
@Table(name = "Shift")
data class ShiftDAO(
        @Id
        @GeneratedValue
        @Column(name = "id")
        val id:Long,

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        @JoinColumn(name = "vetId", nullable = false)
        var vet : VetDAO,

        @Column(name = "startDateTime")
        var startDateTime: Date,

        @Column(name = "endDateTime")
        var endDateTime: Date,

        @OneToMany(mappedBy="shift", fetch = FetchType.LAZY)
        var appointments: List<AppointmentDAO>

) {
        fun update(other: ShiftDAO) {
                this.startDateTime = other.startDateTime
                this.endDateTime = other.endDateTime
        }
}

