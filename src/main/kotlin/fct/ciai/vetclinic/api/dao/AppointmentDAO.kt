package fct.ciai.vetclinic.api.dao

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.util.*
import javax.persistence.*

/**
 * Appointment DAO handles the process to communicate with the database of pets
 */
@Entity
@Table(name = "Appointment")
data class AppointmentDAO(

        @Id
        @GeneratedValue
        @Column(name = "id")
        val id: Long,

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        @JoinColumn(name = "pet", nullable = false)
        var pet: PetDAO,

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        @JoinColumn(name = "vet", nullable = false)
        var vet: VetDAO,

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "shiftId", nullable = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        var shift: ShiftDAO,

        @Column(name = "startingTime")
        val startingTime: Date,

        @Column(name = "endingTime")
        val endingTime: Date,

        @Column(name = "description")
        var description: String,

        @Column(name = "duration")
        val duration: Number,

        @Column(name = "completed")
        var completed: Boolean
) {
    fun update(other: AppointmentDAO) {
        this.description = other.description
        this.completed = other.completed
    }
}