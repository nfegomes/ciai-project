package fct.ciai.vetclinic.api.dao

import java.io.Serializable
import javax.persistence.*
import javax.persistence.FetchType

@Entity
@Table(name = "LoginInfo")
class LoginInfoDAO(

        @Id
        @Column(name = "username")
        val username: String,

        @Column(name = "email", unique = true, nullable = false)
        var email: String,

        @Column(name = "password", nullable = false)
        var password: String,

        @OneToOne(
                mappedBy = "user",
                cascade = [CascadeType.ALL]
        )
        @PrimaryKeyJoinColumn
        var client: ClientDAO? = null,

        @OneToOne(
                mappedBy = "user",
                cascade = [CascadeType.ALL]
        )
        var employee: EmployeeDAO? = null


) : Serializable {
    fun update(other: LoginInfoDAO) {
        this.email = other.email
        this.password = other.password
    }
}
