package fct.ciai.vetclinic.api.dao

import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "Client")
data class ClientDAO(

        @Id
        val username: String,

        @OneToOne(cascade = [CascadeType.ALL])
        @JoinColumn(name="username")
        override var user: LoginInfoDAO?,

        @Column(name = "name")
        override val name: String,

        @Column(name = "picture")
        override val picture: String?,

        @Column(name = "cellphone", unique = true)
        override var cellphone: Number,

        @Column(name = "address")
        override var address: String,


        @Column(name = "frozen")
        var frozen: Boolean,

        @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
        var pets: List<PetDAO>

) : UserDAO(), Serializable