package fct.ciai.vetclinic.api.dao

abstract class UserDAO {
    abstract val user: LoginInfoDAO?
    abstract val name: String
    abstract val picture: String?
    abstract var cellphone: Number
    abstract var address: String


    open fun update(other: UserDAO) {
        this.cellphone = other.cellphone
        this.address = other.address
    }
}