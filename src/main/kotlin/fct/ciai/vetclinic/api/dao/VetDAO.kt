package fct.ciai.vetclinic.api.dao

import javax.persistence.*

@Entity
@Table(name = "Veterinarian")
@DiscriminatorValue("VET")
data class VetDAO(
        override val id: Long,
        override var user: LoginInfoDAO?,
        override val name: String,
        override val picture: String,
        override var cellphone: Number,
        override var address: String,

        @Column(name = "frozen")
        var frozen: Boolean,

        @OneToMany(mappedBy = "vet", fetch = FetchType.LAZY)
        var shifts: List<ShiftDAO>

) : EmployeeDAO(id, user, name, picture, cellphone, address) {

    fun update(other: VetDAO) {
        super.update(other)
        this.frozen = other.frozen
    }
}
