package fct.ciai.vetclinic.api.dao

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import javax.persistence.*

/**
 * Pet DAO handles the process to communicate with the database of pets
 */

@Entity
@Table(name = "Pet")
data class PetDAO(

        @Id
        @GeneratedValue
        @Column(name = "id")
        val id: Long,

        @Column(name = "chipNumber")
        val chipNumber: Long,

        @Column(name = "species")
        val species: String,

        @Column(name = "age")
        var age: Number,

        @Column(name = "picture")
        var picture: String,

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        @JoinColumn(name = "ownerUsername", nullable = false)
        val owner: ClientDAO,

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "pet")
        var appointments: List<AppointmentDAO>,

        @Column(name = "physicalDescription")
        var physicalDescription: String,

        @Column(name = "healthStatus")
        var healthStatus: String,

        @Column(name = "medicalRecord")
        var medicalRecord: String,

        @Column(name = "frozen")
        var frozen: Boolean) {


    fun update(other: PetDAO) {
        this.age = other.age
        this.physicalDescription = other.physicalDescription
        this.healthStatus = other.healthStatus
        this.medicalRecord = other.medicalRecord
        this.picture = other.picture
    }

}