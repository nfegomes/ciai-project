package fct.ciai.vetclinic.api.dao

import javax.persistence.*

@Entity
@Table(name = "Administrator")
@DiscriminatorValue("ADMIN")
data class AdminDAO(
        override val id: Long,
        override var user: LoginInfoDAO?,
        override var name: String,
        override val picture: String,
        override var cellphone: Number,
        override var address: String

) : EmployeeDAO(id, user, name, picture, cellphone, address)
