package fct.ciai.vetclinic.api.services

import fct.ciai.vetclinic.api.dao.AppointmentDAO
import fct.ciai.vetclinic.api.dao.ClientDAO
import fct.ciai.vetclinic.api.dao.LoginInfoDAO
import fct.ciai.vetclinic.api.dao.PetDAO
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.exceptions.PreconditionFailedException
import fct.ciai.vetclinic.api.exceptions.UserIsFrozenException
import fct.ciai.vetclinic.api.repository.ClientRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

@Service
class ClientService(val clientRepository: ClientRepository, val loginService: LoginService,
                    val petService: PetService) {

    @Autowired
    @Lazy
    lateinit var appointmentService: AppointmentService

    fun getAllClients():    List<ClientDAO> = clientRepository.findByFrozenFalse()

    fun getClientByUsername(username: String): ClientDAO {
        return clientRepository.findByUsernameAndFrozenIsFalse(username)
                .orElseThrow { NotFoundException("There is no Client with Username $username") }
    }

    fun getUserByUsername(username: String) =
            loginService.findByUsername(username)

    fun update(username: String, newClientDAO: ClientDAO) {
        val client = getClientByUsername(username)
        if (!client.frozen) {
            client.let {
                it.update(newClientDAO)
                clientRepository.save(it)
            }
        }
    }

    fun addNewClient(client: ClientDAO) {
        val user: LoginInfoDAO? = client.user
        if (user != null && !loginService.isLoginAlreadyOwned(user.username)) {
            clientRepository.save(client)
        } else
            throw PreconditionFailedException("No user associated")
    }

    fun deleteClient(username: String) {
        val client = getClientByUsername(username)
        if (!client.frozen) {
            client.frozen = true
        } else throw UserIsFrozenException("There is no Client with Username $username")
    }

    fun getPetsOfClient(username: String): List<PetDAO> {
        val client = getClientByUsername(username)
        return petService.getClientPets(client)
    }

    fun getAllClientAppointments(username: String): List<AppointmentDAO> =
            appointmentService.getAllClientAppointments(username)

    fun getCompletedClientAppointments(username: String): List<AppointmentDAO> =
            appointmentService.getCompletedClientAppointments(username)

    fun getActiveClientAppointments(username: String): List<AppointmentDAO> =
            appointmentService.getActiveClientAppointments(username)

}



