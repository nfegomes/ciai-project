package fct.ciai.vetclinic.api.services

import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.exceptions.AppointmentCompletedException
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.exceptions.PreconditionFailedException
import fct.ciai.vetclinic.api.repository.AppointmentRepository
import org.springframework.stereotype.Service

@Service
class AppointmentService(val appointmentRepository: AppointmentRepository,
                         val petService: PetService,
                         val shiftService: ShiftService,
                         val vetService: VetService) {

    // get all appointments
    fun getAllAppointments(): List<AppointmentDAO> = appointmentRepository.findAll().toList()

    //get all completed appointments
    fun getAllCompletedAppointments(): List<AppointmentDAO> = appointmentRepository.findByCompleted(true).toList()

    //get all active appointments
    fun getAllActiveAppointments(): List<AppointmentDAO> = appointmentRepository.findByCompleted(false).toList()

    // add new appointment
    fun addNewAppointment(appointment: AppointmentDAO) {
        if (appointment.id != 0L || appointment.pet.frozen) {
            throw PreconditionFailedException("Id must be 0 in insertion")
        } else {
            appointmentRepository.save(appointment)
        }
    }

    // get an appointment for a specified id
    fun getAppointmentById(id: Long): AppointmentDAO =
            appointmentRepository.findById(id)
                    .orElseThrow { NotFoundException("There is no Appointment with Id $id") }

    fun completeAppointmentById(description: String, id: Long) {
        getAppointmentById(id).let {
            if (!it.completed) {
                it.completed = true
                it.description = description
                appointmentRepository.save(it)
            } else {
                throw AppointmentCompletedException("Appointment with Id $id is already completed")
            }
        }
    }

    fun getAllVetAppointments(vetId: Long): List<AppointmentDAO> =
        appointmentRepository.findByVetId(vetId).toList()

    fun getCompletedVetAppointments(vetId: Long): List<AppointmentDAO> =
        appointmentRepository.findByCompletedAndVetId(true,vetId).toList()

    fun getActiveVetAppointments(vetId: Long): List<AppointmentDAO> =
        appointmentRepository.findByCompletedAndVetId(false,vetId).toList()

    fun getPetCompletedAppointments(id: Long): List<AppointmentDAO> =
        appointmentRepository.findByCompletedAndPetId(true, id).toList()

    fun getPetActiveAppointments(id: Long): List<AppointmentDAO> =
            appointmentRepository.findByCompletedAndPetId(false, id).toList()

    fun getAllPetAppointments(id: Long): List<AppointmentDAO> =
        appointmentRepository.findByPetId(id).toList()

    fun getAllClientAppointments(username: String): List<AppointmentDAO> =
            appointmentRepository.findByClientUsername(username).toList()

    fun getCompletedClientAppointments(username: String): List<AppointmentDAO> =
            appointmentRepository.findByCompletedAndClientUsername(true,username).toList()

    fun getActiveClientAppointments(username: String): List<AppointmentDAO> =
            appointmentRepository.findByCompletedAndClientUsername(false,username).toList()


    // delete an appointment for a specified id
    fun deleteAppointmentById(id: Long) {
        getAppointmentById(id).let {
            appointmentRepository.delete(it)
        }
    }


    /*** used in conversions from dao to dto and vice-versa, because we have objects in DAO and ids/username in DTO ***/
    // get appointment's pet by pet id
    fun getPetById(petId: Long): PetDAO =
            petService.getPetByID(petId)

    // get appointment's vet by vet id
    fun getVetById(vetId: Long): VetDAO =
            vetService.getVetById(vetId)

    fun getScheduleById(shiftId: Long): ShiftDAO =
            shiftService.getShiftById(shiftId)
                    //.orElseThrow { NotFoundException("There is no Schedule with Id $scheduleId") }
    /******************************************************************************************************************/
}