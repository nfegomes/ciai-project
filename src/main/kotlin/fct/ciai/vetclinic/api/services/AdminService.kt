package fct.ciai.vetclinic.api.services

import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.exceptions.MainAdminException
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.exceptions.PreconditionFailedException
import fct.ciai.vetclinic.api.repository.*
import org.springframework.stereotype.Service
import java.util.*

@Service
class AdminService(val adminRepository: AdminRepository, val loginService: LoginService) {

    fun getAllAdmins(): List<AdminDAO> = adminRepository.findAll().toList()

    fun getAdminById(id: Long): AdminDAO =
            adminRepository.findById(id)
                    .orElseThrow { NotFoundException("There is no Admin with Id $id") }

    fun getAdminByUsername(id: String): AdminDAO =
            getAdminById(loginService.findByUsername(id).get().employee?.id!!)

    fun addNewAdmin(adminDAO: AdminDAO) {
        if (adminDAO.id != 0L) {
            throw PreconditionFailedException("Id must be 0 in insertion")
        } else {
            val user: LoginInfoDAO? = adminDAO.user
            if (user != null && !loginService.isLoginAlreadyOwned(user.username)) {
                adminRepository.save(adminDAO)
            } else throw PreconditionFailedException("No user associated")
        }
    }

    fun deleteAdmin(id: Long) {
        if (id != 1L) {
            getAdminById(id).let { adminRepository.delete(it) }
        } else throw MainAdminException("This Admin can't be deleted")
    }

    fun getUserByUsername(username: String): Optional<LoginInfoDAO> = loginService.findByUsername(username)

}