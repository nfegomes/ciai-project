package fct.ciai.vetclinic.api.services

import fct.ciai.vetclinic.api.dao.AppointmentDAO
import fct.ciai.vetclinic.api.dao.LoginInfoDAO
import fct.ciai.vetclinic.api.dao.ShiftDAO
import fct.ciai.vetclinic.api.dao.VetDAO
import fct.ciai.vetclinic.api.exceptions.*
import fct.ciai.vetclinic.api.repository.VetRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import java.util.*

@Service
class VetService(val vetRepository: VetRepository, val loginService: LoginService) {

    @Autowired
    @Lazy
    lateinit var appointmentService: AppointmentService

    fun addNewVet(vet: VetDAO) {
        if (vet.id != 0L) {
            throw PreconditionFailedException("Id must be 0 in insertion")
        } else {
            val user: LoginInfoDAO? = vet.user
            if (user != null && !loginService.isLoginAlreadyOwned(user.username)) {
                vetRepository.save(vet)
            } else
                throw PreconditionFailedException("No user associated")
        }
    }

    fun deleteVet(id: Long) {
        val vet = getVetById(id)
        if (!vet.frozen) {
            if (getActiveVetAppointments(id).isEmpty()) {
                vet.frozen = true
                vetRepository.save(vet)
            } else throw VetWithAppointmentsException("Vet with Id $id has active Appointments")
        } else throw UserIsFrozenException("There is no Vet with Id $id")
    }

    fun getVetById(id: Long): VetDAO =
            vetRepository.findByIdAndFrozenFalse(id)
                    .orElseThrow { NotFoundException("There is no Vet with Id $id") }

    fun getVetByUsername(id: String): VetDAO =
            getVetById(loginService.findByUsername(id).get().employee?.id!!)


    fun getAllVets(): List<VetDAO> = vetRepository.findByFrozenFalse()

    fun getAllVetSchedule(id: Long): List<ShiftDAO> {

        val vet = vetRepository.findByIdAndFrozenFalse(id)
                .orElseThrow { NotFoundException("There is no Vet with Id $id") }
        return vet.shifts
    }

    fun getUserByUsername(username: String): Optional<LoginInfoDAO> = loginService.findByUsername(username)


    fun getAllVetAppointments(vetId: Long): List<AppointmentDAO> =
            appointmentService.getAllVetAppointments(vetId)

    fun getCompletedVetAppointments(vetId: Long): List<AppointmentDAO> =
            appointmentService.getCompletedVetAppointments(vetId)

    fun getActiveVetAppointments(vetId: Long): List<AppointmentDAO> =
            appointmentService.getActiveVetAppointments(vetId)

    fun completeAppointmentById(description: String, id: Long, aptId: Long) {
        val vet = getVetById(id) // to check if the vet exists
        appointmentService.completeAppointmentById(description, aptId)
    }
}