package fct.ciai.vetclinic.api.services

import fct.ciai.vetclinic.api.dao.LoginInfoDAO
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.repository.LoginRepository
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.util.*

@Service
class LoginService(val loginInfoRepository: LoginRepository, val employeeService: EmployeeService) {


    fun findAll(): List<LoginInfoDAO> = loginInfoRepository.findAll()

    fun findByUsername(username: String): Optional<LoginInfoDAO> = loginInfoRepository.findById(username)

    fun addNew(login: LoginInfoDAO): Optional<LoginInfoDAO> {
        val aLogin = findByUsername(login.username)

        return if (aLogin.isPresent)
            Optional.empty()
        else {
            login.password = BCryptPasswordEncoder().encode(login.password)
            Optional.of(loginInfoRepository.save(login))
        }
    }

    fun updateLogin(login: LoginInfoDAO): Optional<LoginInfoDAO> {
        val aLogin = findByUsername(login.username)

        if (aLogin.isPresent)
            loginInfoRepository.save(login)

        return aLogin

    }

    fun isLoginAlreadyOwned(username: String): Boolean {
        val aLogin = findByUsername(username)

        return if (aLogin.isPresent)
            return aLogin.get().client != null || aLogin.get().employee != null
        else
            false
    }


    fun getEmployeeType(username: String) = employeeService.getRoleOfEmp(username)


}
