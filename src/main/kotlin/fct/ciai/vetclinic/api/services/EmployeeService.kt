package fct.ciai.vetclinic.api.services

import fct.ciai.vetclinic.api.dao.EmployeeDAO
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.repository.EmployeeRepository
import org.springframework.stereotype.Service

@Service
class EmployeeService(val employeeRepository: EmployeeRepository) {

    fun findAll() = employeeRepository.findAllActive()

    fun getEmployeeById(id: Long) = employeeRepository.findById(id)
            .orElseThrow { NotFoundException("no employee with id $id was found") }

    fun update(id: Long, newEmployeeDAO: EmployeeDAO) {
        val employee = getEmployeeById(id)
        employee.update(newEmployeeDAO)
        employeeRepository.save(employee)
    }

    fun getRoleOfEmp(username: String): String {
        val role = employeeRepository.findRoleOfUser(username)

        if (role.isPresent) {
            return role.get()
        } else
            throw NotFoundException("User $username doesn't exist")
    }
}
