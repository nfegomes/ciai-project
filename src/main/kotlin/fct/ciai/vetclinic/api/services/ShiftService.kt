package fct.ciai.vetclinic.api.services

import fct.ciai.vetclinic.api.dao.AppointmentDAO
import fct.ciai.vetclinic.api.dao.ShiftDAO
import fct.ciai.vetclinic.api.exceptions.*
import fct.ciai.vetclinic.api.repository.ShiftRepository
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.*


@Service
class ShiftService(val shiftRepository: ShiftRepository, val vetService: VetService) {

    fun getAllShifts(): List<ShiftDAO> = shiftRepository.findAll().toList()

    fun getShiftById(id: Long): ShiftDAO = shiftRepository.findById(id)
            .orElseThrow { NotFoundException("There is no Shift with Id $id") }


    fun deleteShiftById(id: Long) {
        getShiftById(id).let {
            if (it.appointments.isEmpty()) {
                shiftRepository.delete(it)
            } else throw ShiftWithAppointmentsException("Shift with id $id has appointments and can't be deleted")
        }
    }

    fun updateSchedule(shifts: List<ShiftDAO>) {
        val hourInMillisecond: Long = 60 * 60 * 1000
        val oneWeekInMillisecond: Long = 7 * 24 * hourInMillisecond

        shifts.sortedBy { it.startDateTime }

        var monthlyHours = 0L
        var weeklyHours = 0L
        val firstDay = shifts[0].startDateTime.time

        for ((current, shift) in shifts.withIndex()) {

            val startTime = shift.startDateTime.time
            val endTime = shift.endDateTime.time

            if (shift.id != 0L) {
                throw PreconditionFailedException("Id must be 0 in insertion")
            }

            if (shift.vet.frozen) {
                throw UserIsFrozenException("Vet Not Found")
            }

            if (endTime - startTime > 12 * hourInMillisecond) {
                throw ShiftLimitException("Shifts cannot be longer than 12 hours")
            }

            if (startTime < Date().time) {
                throw BeforeTodaysDateException("Date before today")
            }

            monthlyHours += endTime - startTime

            if (monthlyHours > 160 * hourInMillisecond) {
                throw MonthlyLimitScheduleException("160 hours per month reached")
            }

            weeklyHours += endTime - startTime

            if (current != shifts.size - 1) {

                val nextShift: ShiftDAO = shifts[current + 1]

                if (nextShift.startDateTime.time > (8 * hourInMillisecond) + shift.endDateTime.time) {
                    throw TimeBetweenShiftsException("Time between shifts violated")
                }

                if (firstDay + oneWeekInMillisecond < nextShift.startDateTime.time) {
                    weeklyHours = 0
                }

                if (weeklyHours > 40 * hourInMillisecond)
                    throw WeeklyLimitScheduleException("40 hours per week reached")
            }
        }

        for (shift in shifts) {
            shiftRepository.save(shift)
        }
    }

    fun getByVetId(vetId: Long): List<ShiftDAO> = shiftRepository.findByVetId(vetId)

    fun getAppointmentOfId(id: Long): List<AppointmentDAO> = shiftRepository.findById(id).get().appointments


    fun getVetById(vetID: Long) = vetService.getVetById(vetID)

}