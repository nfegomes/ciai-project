package fct.ciai.vetclinic.api.services

import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.exceptions.PetIsFrozenException
import fct.ciai.vetclinic.api.exceptions.PreconditionFailedException
import fct.ciai.vetclinic.api.exceptions.UserIsFrozenException
import fct.ciai.vetclinic.api.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

@Service
class PetService(val petRepository: PetRepository,
                 val vetService: VetService,
                 val shiftService: ShiftService) {

    @Autowired
    @Lazy
    lateinit var appointmentService: AppointmentService

    @Autowired
    @Lazy
    lateinit var clientService: ClientService

    fun getAllPets(): List<PetDAO> = petRepository.findByFrozenFalse().toList()

    fun getClientPets(client: ClientDAO) : List<PetDAO> =
        petRepository.findByOwner(client)

    fun getPetByID(id: Long): PetDAO =
            petRepository.findById(id)
                    .orElseThrow { NotFoundException("There is no Pet with Id $id") }

    fun getVetByID(id: Long): VetDAO = vetService.getVetById(id)

    fun getShiftByID(id: Long): ShiftDAO = shiftService.getShiftById(id)

    fun addNewPet(petDAO: PetDAO) {
        if (petDAO.id != 0L) {
            throw PreconditionFailedException("Id must be 0 in insertion")
        } else if (petDAO.owner.frozen) {
            throw UserIsFrozenException("Client is frozen")
        } else {
            petDAO.appointments = emptyList()
            petRepository.save(petDAO)
        }
    }

    fun updatePetByID(newPetDAO: PetDAO, id: Long) {
        getPetByID(id).let {
            if(!it.frozen) {
                it.update(newPetDAO)
                petRepository.save(it)
            } else if (it.owner.frozen){
                throw UserIsFrozenException("Client is frozen")
            } else {
                throw PetIsFrozenException("Id must be 0 in insertion")
            }
        }
    }

    fun deletePetByID(id: Long) {
        var pet = getPetByID(id)
        if(pet.owner.frozen){
            throw UserIsFrozenException("Client is frozen")
        } else if (pet.frozen){
            throw PetIsFrozenException("There is no Pet with Id $id")
        } else {
            getPetByID(id).let {
                it.frozen = true
                petRepository.save(it)
            }
        }
    }

    fun getPetCompletedAppointments(id: Long): List<AppointmentDAO> =
            appointmentService.getPetCompletedAppointments(id)

    fun getPetActiveAppointments(id: Long): List<AppointmentDAO> =
            appointmentService.getPetActiveAppointments(id)

    fun getAllPetAppointments(id: Long): List<AppointmentDAO> =
            appointmentService.getAllPetAppointments(id)



    fun addNewAppointment(apt: AppointmentDAO) =
            if (apt.id != 0L) {
                throw PreconditionFailedException("Id must be 0 in insertion")
            } else {
                appointmentService.addNewAppointment(apt)
            }

    fun getOwnerById(ownerUsername: String): ClientDAO = clientService.getClientByUsername(ownerUsername)

}