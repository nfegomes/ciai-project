package fct.ciai.vetclinic.api

import fct.ciai.vetclinic.api.exceptions.*

fun <T> handle4xx(inner: () -> T): T =
        try {
            inner()
        } catch (e: NotFoundException) {
            throw HTTPNotFoundException(e.message ?: "Not Found")
        } catch (e: UserIsFrozenException) {
            throw HTTPNotFoundException(e.message ?: "Not Found")
        } catch (e: PreconditionFailedException) {
            throw HTTPBadRequestException(e.message ?: "Bad Request")
        } catch (e: ShiftLimitException) {
            throw HTTPBadRequestException(e.message ?: "Bad Request")
        } catch (e: BeforeTodaysDateException) {
            throw HTTPBadRequestException(e.message ?: "Bad Request")
        } catch (e: MonthlyLimitScheduleException) {
            throw HTTPBadRequestException(e.message ?: "Bad Request")
        } catch (e: TimeBetweenShiftsException) {
            throw HTTPBadRequestException(e.message ?: "Bad Request")
        } catch (e: WeeklyLimitScheduleException) {
            throw HTTPBadRequestException(e.message ?: "Bad Request")
        } catch (e: ShiftWithAppointmentsException) {
            throw HTTPBadRequestException(e.message ?: "Bad Request")
        } catch (e: AppointmentCompletedException) {
            throw HTTPBadRequestException(e.message ?: "Bad Request")
        } catch (e: MainAdminException) {
            throw HTTPBadRequestException(e.message ?: "Bad Request")
        } catch (e: VetWithAppointmentsException) {
            throw HTTPBadRequestException(e.message ?: "Bad Request")
        }