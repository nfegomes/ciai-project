package fct.ciai.vetclinic.api.controllers.appointments

import fct.ciai.vetclinic.api.dao.AppointmentDAO
import fct.ciai.vetclinic.api.dto.AppointmentDTO
import fct.ciai.vetclinic.api.handle4xx
import fct.ciai.vetclinic.api.services.AppointmentService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@Api(value = "VetClinic Management System - Appointment API",
        description = "Management operations of Appointments in the IADI 2019 Pet Clinic")
@RestController
@RequestMapping("/appointments")
class AppointmentController(val appointmentService: AppointmentService) {

    @ApiOperation(value = "View a list of all appointments (completed and active)", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )
    @GetMapping("/")
//    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun listAllAppointments(): List<AppointmentDTO> =
            appointmentService.getAllAppointments().map { daoToDto(it) }

    @ApiOperation(value = "View a list of all completed appointments", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )
    @GetMapping("/completed/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun listAllCompletedAppointments(): List<AppointmentDTO> =
            appointmentService.getAllCompletedAppointments().map { daoToDto(it) }

    @ApiOperation(value = "View a list of all active appointments", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )
    @GetMapping("/active/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun listAllActiveAppointments(): List<AppointmentDTO> =
            appointmentService.getAllActiveAppointments().map { daoToDto(it) }

    @ApiOperation(value = "Add a new appointment", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully added an appointment"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )
    @PostMapping("/")
//    @PreAuthorize("hasAnyRole('ROLE_CLIENT', 'ROLE_ADMIN')")
    fun addNewAppointment(@RequestBody appointment: AppointmentDTO) =
            appointmentService.addNewAppointment(dtoToDao(appointment, appointmentService))

    @ApiOperation(value = "Get the details of a specified appointment", response = AppointmentDTO::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved appointment details"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{id}")
    fun getAppointmentByID(@PathVariable id: Long): AppointmentDTO =
            handle4xx { daoToDto(appointmentService.getAppointmentById(id)) }

    @ApiOperation(value = "Delete an appointment", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully deleted an appointment"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun deleteAppointmentByID(@PathVariable id: Long) =
            handle4xx { appointmentService.deleteAppointmentById(id) }


    /*********************************** CONVERSIONS DAO <-> DTO ***********************************/
    companion object Conversions {
        // conversion from Appointment DAO to DTO
        fun daoToDto(it: AppointmentDAO): AppointmentDTO {
            return AppointmentDTO(it.id, it.pet.id, it.shift.id, it.vet.id, it.startingTime, it.endingTime,
                    it.description, it.duration, it.completed)
        }

        // conversion from Appointment DTO to DAO
        fun dtoToDao(appointment: AppointmentDTO, appointmentService: AppointmentService): AppointmentDAO {
            return AppointmentDAO(appointment.id,
                    appointmentService.getPetById(appointment.petId),
                    appointmentService.getVetById(appointment.vetId),
                    appointmentService.getScheduleById(appointment.shiftId),
                    appointment.startingTime, appointment.endingTime, appointment.description,
                    appointment.duration, appointment.completed)
        }
    }

}