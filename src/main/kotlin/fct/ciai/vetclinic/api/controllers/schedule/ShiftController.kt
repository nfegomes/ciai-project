package fct.ciai.vetclinic.api.controllers.schedule

import fct.ciai.vetclinic.api.dao.ShiftDAO
import fct.ciai.vetclinic.api.dto.ShiftDTO
import fct.ciai.vetclinic.api.handle4xx
import fct.ciai.vetclinic.api.services.ShiftService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@Api(value = "VetClinic Management System - Shift API",
        description = "Management operations of Shifts in the IADI 2019 Pet Clinic")
@RestController
@RequestMapping("/shifts")
class ShiftController(val shiftService: ShiftService) {

    @ApiOperation(value = "View a list of all shifts", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )
    @GetMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun getAllShifts(): List<ShiftDTO> =
            shiftService.getAllShifts().map { daoToDto(it) }

    @ApiOperation(value = "Add a new schedule (using a list of shifts)", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully added a schedule"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )
    @PostMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun addSchedule(@RequestBody schedule: List<ShiftDTO>) =
            shiftService.updateSchedule(schedule.map { dtoToDao(it, shiftService) })

    @ApiOperation(value = "Get the details of a specified shift", response = ShiftDTO::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved shift details"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_VET')")
    fun getShiftByID(@PathVariable id: Long) =
            handle4xx { daoToDto(shiftService.getShiftById(id)) }

    @ApiOperation(value = "Delete a shift", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully deleted a shift"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun deleteShiftByID(@PathVariable id: Long) =
            handle4xx { shiftService.deleteShiftById(id) }

    companion object Conversions {
        fun daoToDto(it: ShiftDAO): ShiftDTO {
            return ShiftDTO(it.id, it.vet.id, it.startDateTime, it.endDateTime)
        }

        fun dtoToDao(it: ShiftDTO, shiftService: ShiftService): ShiftDAO {
            return ShiftDAO(it.id,
                    shiftService.getVetById(it.vetID),
                    it.startDateTime,
                    it.endDateTime,
                    emptyList())
        }
    }
}