package fct.ciai.vetclinic.api.controllers.users

import fct.ciai.vetclinic.api.controllers.appointments.AppointmentController
import fct.ciai.vetclinic.api.dao.AppointmentDAO
import fct.ciai.vetclinic.api.dao.ClientDAO
import fct.ciai.vetclinic.api.dto.AppointmentDTO
import fct.ciai.vetclinic.api.dto.ClientDTO
import fct.ciai.vetclinic.api.dto.PetDTO
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.handle4xx
import fct.ciai.vetclinic.api.services.ClientService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@Api(value = "VetClinic Management System - Client API",
        description = "Management operations of Clients in the IADI 2019 Pet Clinic")
@RestController
@RequestMapping("/clients")
class ClientController(val clientService: ClientService) {

    @ApiOperation(value = "View a list of all clients", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )
    @GetMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun listAllClients(): List<ClientDTO> = clientService.getAllClients().map { daoToDto(it) }

    @ApiOperation(value = "Get the details of a specified client", response = ClientDTO::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved client details"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{username}")
    fun getUserByUsername(@PathVariable username: String) =
            handle4xx { daoToDto(clientService.getClientByUsername(username)) }

    @ApiOperation(value = "Update a client", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully updated a client"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @PutMapping("/{username}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_CLIENT')")
    fun updateUserInfoByUsername(@PathVariable username: String, @RequestBody client: ClientDTO) =
            handle4xx { clientService.update(username, dtoToDao(client, clientService)) }

    @ApiOperation(value = "Add a new client", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully added a client"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )
    @PostMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun addNewClient(@RequestBody client: ClientDTO) =
            // pets list is empty and it's not frozen
            handle4xx {
                clientService.addNewClient(
                        ClientDAO(client.username,
                                clientService.getUserByUsername(client.username)
                                        .orElseThrow { NotFoundException("User ${client.username} doesn't exist") },
                                client.name, client.picture, client.cellphone, client.address,
                                client.isFrozen, emptyList()))
            }

    @ApiOperation(value = "Delete a client", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully deleted a client"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @DeleteMapping("/{username}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun deleteClient(@PathVariable username: String) =
            handle4xx { clientService.deleteClient(username) }

     @ApiOperation(value = "List the pets related to a client", response = List::class)
     @ApiResponses(
             ApiResponse(code = 200, message = "Successfully retrieved the list of pets"),
             ApiResponse(code = 401, message = "You are not authorized to view the resource"),
             ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
             ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
     )
     @GetMapping("/{username}/pets")
     @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_CLIENT')")
     fun getPetsOfUser(@PathVariable username: String) =
            handle4xx {
                clientService.getPetsOfClient(username)
                        .map {
                            PetDTO(it.id, it.chipNumber, it.species, it.age, it.picture, it.owner.username,
                                    it.physicalDescription, it.healthStatus, it.medicalRecord, it.frozen)
                        }
            }

    @ApiOperation(value = "List all appointments related to a client", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the list of appointments"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{username}/appointments")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_CLIENT')")
    fun getClientAppointments(@PathVariable username: String): List<AppointmentDTO> =
            handle4xx { clientService.getAllClientAppointments(username).map { AppointmentController.daoToDto(it) } }

    @ApiOperation(value = "List all completed appointments related to a client", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the list of completed appointments"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{id}/appointments/completed")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_CLIENT')")
    fun getClientCompletedAppointments(@PathVariable username: String): List<AppointmentDTO> =
            handle4xx { clientService.getCompletedClientAppointments(username).map { AppointmentController.daoToDto(it) } }

    @ApiOperation(value = "List all active appointments related to a client", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the list of active appointments"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{id}/appointments/active")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_CLIENT')")
    fun getClientActiveAppointments(@PathVariable username: String): List<AppointmentDTO> =
            handle4xx { clientService.getActiveClientAppointments(username).map { AppointmentController.daoToDto(it) } }




    companion object Conversions {
                    // conversion from Client DAO to DTO
                    fun daoToDto(it: ClientDAO): ClientDTO {
                        return ClientDTO(it.username, it.name, it.picture, it.cellphone, it.address, it.frozen)
                    }

                    // conversion from Client DTO to DAO
                    fun dtoToDao(user: ClientDTO, clientService: ClientService): ClientDAO {
                        val client = clientService.getClientByUsername(user.username)
                        return ClientDAO(
                                user.username, clientService.getUserByUsername(user.username).get(),
                                user.name, user.picture,
                                user.cellphone, user.address, client.frozen,
                                clientService.getPetsOfClient(user.username)
                        )
                    }
                }


}