package fct.ciai.vetclinic.api.controllers.login

import fct.ciai.vetclinic.api.dao.LoginInfoDAO
import fct.ciai.vetclinic.api.dto.LoginInfoDTO
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.handle4xx
import fct.ciai.vetclinic.api.services.*
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@Api(value = "VetClinic Management System - Users' Login API",
        description = "Management operations of Users' Login in the IADI 2019 Pet Clinic")
@RestController
@RequestMapping("/users")
class LoginController(val loginService: LoginService) {

    @ApiOperation(value = "List all registered users", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun listAllUsers(): List<LoginInfoDTO> = loginService.findAll().map { loginDaoToDto(it) }

    @ApiOperation(value = "Get the details of a single login for a specified user's username", response = LoginInfoDTO::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved username's login details"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{username}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun getLoginInfo(@PathVariable username: String): LoginInfoDTO =
            handle4xx {
                val login: LoginInfoDAO = loginService.findByUsername(username)
                        .orElseThrow { NotFoundException("User $username doesn't exist") }
                LoginInfoDTO(login.username, login.password, login.email)
            }

    @ApiOperation(value = "Add a new login information", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully added a login information"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )
    @PostMapping("/signup")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun addNewLogin(@RequestBody login: LoginInfoDTO): LoginInfoDTO =
            handle4xx {
                loginDaoToDto(loginService.addNew(loginDtoToDao(login, loginService))
                        .orElseThrow { NotFoundException("User ${login.username} already exists") })
            }


    /*********************************** CONVERSIONS DAO <-> DTO ***********************************/
    companion object Conversions {
        // conversion from Login DAO to DTO
        fun loginDaoToDto(loginInfo: LoginInfoDAO): LoginInfoDTO {
            return LoginInfoDTO(loginInfo.username, loginInfo.password, loginInfo.email,
                    loginInfo.client?.name, loginInfo.employee?.id)
        }

        // conversion from Login DTO to DAO
        fun loginDtoToDao(login: LoginInfoDTO, loginService: LoginService): LoginInfoDAO {

            return LoginInfoDAO(login.username, login.email
                    ?: loginService.findByUsername(login.username).get().username, login.password, null)
        }
    }
}