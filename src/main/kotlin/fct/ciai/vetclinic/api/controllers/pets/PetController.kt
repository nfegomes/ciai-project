package fct.ciai.vetclinic.api.controllers.pets

import fct.ciai.vetclinic.api.controllers.appointments.AppointmentController
import fct.ciai.vetclinic.api.dao.AppointmentDAO
import fct.ciai.vetclinic.api.dao.PetDAO
import fct.ciai.vetclinic.api.dto.AppointmentDTO
import fct.ciai.vetclinic.api.dto.PetDTO
import fct.ciai.vetclinic.api.handle4xx
import fct.ciai.vetclinic.api.services.PetService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@Api(value = "VetClinic Management System - Pet API",
        description = "Management operations of Pets in the IADI 2019 Pet Clinic")
@RestController
@CrossOrigin
@RequestMapping("/pets")
class PetController(val petService: PetService) {

    @ApiOperation(value = "View a list of all registered pets", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )
    @GetMapping("/")
//    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun getAllPets(): List<PetDTO> =
            petService.getAllPets().map { petDaoToDto(it) }

    @ApiOperation(value = "Get the details of a specified pet", response = PetDTO::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved pet details"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{id}")
    fun getPetByID(@PathVariable id: Long) =
            handle4xx { petDaoToDto(petService.getPetByID(id)) }

    @ApiOperation(value = "Add a new pet", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully added a pet"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )
    @PostMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_CLIENT')")
    fun addNewPet(@RequestBody pet: PetDTO) =
            // appointments list is empty and it's not frozen
            petService.addNewPet(PetDAO(0L, pet.chipNumber, pet.species, pet.age, pet.picture,
                    petService.getOwnerById(pet.ownerUsername), emptyList(), pet.physicalDescription,
                    pet.healthStatus, pet.medicalRecord, false))

    @ApiOperation(value = "Update a pet", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully updated a pet"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_CLIENT')")
    fun updatePetInfoByID(@PathVariable id: Long, @RequestBody pet: PetDTO) =
            handle4xx { petService.updatePetByID(petDtoToDao(pet, petService), id) }

    @ApiOperation(value = "Delete a pet", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully deleted a pet"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_CLIENT')")
    fun deletePetByID(@PathVariable id: Long) =
            handle4xx { petService.deletePetByID(id) }


    @ApiOperation(value = "Add a new appointment to a pet", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully added an appointment to a pet"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @PostMapping("/{id}/appointments")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_CLIENT')")
    fun addAppointment(@PathVariable id: Long, @RequestBody appointment: AppointmentDTO) =
            handle4xx {
                petService.addNewAppointment(AppointmentDAO(appointment.id,
                        petService.getPetByID(id),
                        petService.getVetByID(appointment.vetId),
                        petService.getShiftByID(appointment.shiftId),
                        appointment.startingTime, appointment.endingTime,
                        appointment.description, appointment.duration, appointment.completed))
            }

    @ApiOperation(value = "List the appointments related to a pet", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the list of appointments"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{id}/appointments")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_CLIENT')")
    fun getAllPetAppointments(@PathVariable id: Long): List<AppointmentDTO> =
            handle4xx {
                petService.getAllPetAppointments(id).map {
                    AppointmentController.daoToDto(it)
                }
            }

    @ApiOperation(value = "List the completed appointments related to a pet", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the list of completed appointments"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{id}/appointments/completed")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_CLIENT')")
    fun getPetCompletedAppointments(@PathVariable id: Long): List<AppointmentDTO> =
            handle4xx {
                petService.getPetCompletedAppointments(id).map { AppointmentController.daoToDto(it) }
            }

    @ApiOperation(value = "List the active appointments related to a pet", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the list of active appointments"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{id}/appointments/active")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_CLIENT')")
    fun getPetActiveAppointments(@PathVariable id: Long): List<AppointmentDTO> =
            handle4xx {
                petService.getPetActiveAppointments(id).map { AppointmentController.daoToDto(it) }
            }

    /*********************************** CONVERSIONS DAO <-> DTO ***********************************/
    companion object Conversions {

        // conversion from Pet DAO to DTO
        fun petDaoToDto(pet: PetDAO): PetDTO {
            return PetDTO(pet.id, pet.chipNumber, pet.species, pet.age, pet.picture,
                    pet.owner.username, pet.physicalDescription,
                    pet.healthStatus, pet.medicalRecord, pet.frozen)
        }

        // conversion from Pet DTO to DAO
        fun petDtoToDao(pet: PetDTO, petService: PetService): PetDAO {
                return PetDAO(pet.id, pet.chipNumber, pet.species, pet.age, pet.picture,
                        petService.getOwnerById(pet.ownerUsername), emptyList(),
                        pet.physicalDescription, pet.healthStatus, pet.medicalRecord, pet.isFrozen)
        }

    }
}

