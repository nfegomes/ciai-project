package fct.ciai.vetclinic.api.controllers.users

import fct.ciai.vetclinic.api.controllers.appointments.AppointmentController
import fct.ciai.vetclinic.api.controllers.schedule.ShiftController
import fct.ciai.vetclinic.api.dao.AppointmentDAO
import fct.ciai.vetclinic.api.dao.VetDAO
import fct.ciai.vetclinic.api.dto.AppointmentDTO
import fct.ciai.vetclinic.api.dto.ShiftDTO
import fct.ciai.vetclinic.api.dto.VetDTO
import fct.ciai.vetclinic.api.exceptions.NotFoundException
import fct.ciai.vetclinic.api.handle4xx
import fct.ciai.vetclinic.api.services.VetService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@Api(value = "VetClinic Management System - Veterinarian API",
        description = "Management operations of Veterinarians in the IADI 2019 Pet Clinic")
@RestController
@CrossOrigin
@RequestMapping("/vets")
class VetController(val vetService: VetService) {

    @ApiOperation(value = "View a list of all veterinarians", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )
    @GetMapping("/")
  //  @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun listAllVets() = vetService.getAllVets().map { vetDaoToDto(it) }

    @ApiOperation(value = "Get the details of a specified veterinarian", response = VetDTO::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved veterinarian details"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{id}")

    fun getVetByID(@PathVariable id: String) =
            handle4xx { vetDaoToDto(vetService.getVetByUsername(id)) }

    @ApiOperation(value = "Add a new veterinarian", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully added a veterinarian"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )



    @PostMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun addNewVet(@RequestBody vet: VetDTO) =
            // shifts list is empty and it's not frozen
            handle4xx {
                vetService.addNewVet(
                        VetDAO(0L,
                                vetService.getUserByUsername(vet.username)
                                        .orElseThrow { NotFoundException("User ${vet.username} doesn't exist") },
                                vet.name, vet.picture, vet.cellphone, vet.address, vet.isFrozen, emptyList()))
            }

    @ApiOperation(value = "Delete a veterinarian", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully deleted a veterinarian"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun deleteVet(@PathVariable id: Long) =
            handle4xx { vetService.deleteVet(id) }

    @ApiOperation(value = "List all shifts related to a vet with specified username", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the list of vet shifts"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{id}/shifts")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_VET')")
    fun getVetSchedule(@PathVariable id: Long): List<ShiftDTO> =
            handle4xx { vetService.getAllVetSchedule(id).map { ShiftController.daoToDto(it) } }

    @ApiOperation(value = "List all appointments related to a vet", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the list of appointments"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{id}/appointments")
//    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_VET')")
    fun getVetAppointments(@PathVariable id: Long): List<AppointmentDTO> =
            handle4xx { vetService.getAllVetAppointments(id).map { AppointmentController.daoToDto(it) } }

    @ApiOperation(value = "List all completed appointments related to a vet", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the list of completed appointments"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{id}/appointments/completed")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_VET')")
    fun getVetCompletedAppointments(@PathVariable id: Long): List<AppointmentDTO> =
            handle4xx { vetService.getCompletedVetAppointments(id).map { AppointmentController.daoToDto(it) } }

    @ApiOperation(value = "List all active appointments related to a vet", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved the list of active appointments"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{id}/appointments/active")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_VET')")
    fun getVetActiveAppointments(@PathVariable id: Long): List<AppointmentDTO> =
            handle4xx { vetService.getActiveVetAppointments(id).map { AppointmentController.daoToDto(it) } }


    @ApiOperation(value = "Complete an appointment", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully completed an appointment"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @PutMapping("/{id}/appointments/{aptId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_VET')")
    fun completeAppointmentByID(@RequestBody description: String, @PathVariable id: Long, @PathVariable aptId: Long) =
            handle4xx { vetService.completeAppointmentById(description, id, aptId) }


    companion object Conversions {
        // conversion from Vet DAO to DTO
        fun vetDaoToDto(vet: VetDAO): VetDTO {
            return VetDTO(vet.id, vet.user!!.username, vet.name, vet.picture, vet.cellphone, vet.address, vet.frozen)
        }

        // conversion from Vet DTO to DAO
        fun vetDtoToDao(vet: VetDTO, vetService: VetService): VetDAO {
            val tempVet: VetDAO = vetService.getVetById(vet.id)
            return VetDAO(vet.id, tempVet.user, vet.name, vet.picture, vet.cellphone,
                    vet.address, vet.isFrozen, tempVet.shifts)
        }
    }
}
