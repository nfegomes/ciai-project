package fct.ciai.vetclinic.api.controllers.users

import fct.ciai.vetclinic.api.dao.EmployeeDAO
import fct.ciai.vetclinic.api.dto.EmployeeDTO
import fct.ciai.vetclinic.api.handle4xx
import fct.ciai.vetclinic.api.services.EmployeeService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@Api(value = "VetClinic Management System - Employee API",
        description = "Management operations of Employees in the IADI 2019 Pet Clinic")
@RestController
@CrossOrigin
@RequestMapping("/employees")
class EmployeeController(val empService: EmployeeService) {

    @ApiOperation(value = "List all registered employees", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )
    @GetMapping("/")
   // @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun listAllEmployees(): List<EmployeeDTO> =
            empService.findAll().map {
                EmployeeDTO(it.id, it.user!!.username, it.name, it.picture,
                        it.cellphone, it.address)
            }

    @ApiOperation(value = "Update an employee", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully updated an employee"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun updateEmployeeInfoByUsername(@PathVariable id: Long, @RequestBody employee: EmployeeDTO) =
            handle4xx { empService.update(id, dtoToDao(employee)) }

    companion object Conversions {
        // conversion from Employee DTO to DAO
        fun dtoToDao(user: EmployeeDTO): EmployeeDAO {
            return EmployeeDAO(
                    user.id, null, user.name, user.picture, user.cellphone,user.address )
        }
    }

}