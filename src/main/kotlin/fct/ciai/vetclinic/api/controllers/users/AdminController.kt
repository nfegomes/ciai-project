package fct.ciai.vetclinic.api.controllers.users

import fct.ciai.vetclinic.api.dao.AdminDAO
import fct.ciai.vetclinic.api.dto.AdminDTO
import fct.ciai.vetclinic.api.handle4xx
import fct.ciai.vetclinic.api.services.*
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@Api(value = "VetClinic Management System - Administrative API",
        description = "Management operations of Administratives in the IADI 2019 Pet Clinic")
@RestController
@CrossOrigin
@RequestMapping("/admins")
class AdminController(val adminService: AdminService) {

    @ApiOperation(value = "View a list of all administratives", response = List::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved list"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )
    @GetMapping("/")
   // @PreAuthorize("hasRole({'ROLE_ADMIN'})")
    fun listAllAdmins() = adminService.getAllAdmins().map { adminDaoToDto(it) }

    @ApiOperation(value = "Get the details of a specified administrative", response = AdminDTO::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved administrative details"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun getAdminByID(@PathVariable id: String) =
            handle4xx { adminDaoToDto(adminService.getAdminByUsername(id)) }

    @ApiOperation(value = "Add a new administrative", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully added an administrative"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    )
    @PostMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun addNewAdmin(@RequestBody admin: AdminDTO) =
            handle4xx {
                adminService.addNewAdmin(AdminDAO(admin.id, adminService.getUserByUsername(admin.username).get(),
                        admin.name, admin.picture, admin.cellphone, admin.address))
            }

    @ApiOperation(value = "Delete an administrative", response = Unit::class)
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully deleted an administrative"),
            ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    )
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun deleteAdmin(@PathVariable id: Long) =
            handle4xx { adminService.deleteAdmin(id) }

    /*********************************** CONVERSIONS DAO <-> DTO ***********************************/
    companion object Conversions {
        // conversion from Admin DAO to DTO
        fun adminDaoToDto(admin: AdminDAO): AdminDTO {
            return AdminDTO(admin.id, admin.user!!.username, admin.name, admin.picture, admin.cellphone, admin.address)
        }

        // conversion from Admin DTO to DAO
        fun adminDtoToDao(admin: AdminDTO, adminService: AdminService): AdminDAO {
            return AdminDAO(admin.id, adminService.getUserByUsername(admin.username).get(), admin.name, admin.picture,
                    admin.cellphone, admin.address)
        }
    }
}