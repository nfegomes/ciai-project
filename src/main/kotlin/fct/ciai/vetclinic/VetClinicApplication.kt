package fct.ciai.vetclinic

import fct.ciai.vetclinic.api.dao.*
import fct.ciai.vetclinic.api.repository.*
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.util.*

@SpringBootApplication
class VetClinicApplication {

    @Bean
//    @Profile("runtime") // to avoid conflicts with mocked repository components
    fun init(
            adminRepository: AdminRepository,
            petRepository: PetRepository,
            aptsRepository: AppointmentRepository,
            clientRepository: ClientRepository,
            vetRepository: VetRepository,
            shiftRepository: ShiftRepository
    ) =
            CommandLineRunner {
                // this admin cannot be deleted
                val mainAdmin = AdminDAO(0L,
                        LoginInfoDAO("admin", "admin@email.com", BCryptPasswordEncoder().encode("password")),
                        "Admin Manuela", "https://images.unsplash.com/photo-1508214751196-bcfd4ca60f91?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80", 912345678, "admin address")
                adminRepository.save(mainAdmin)

                val admin2 = AdminDAO(0L,
                        LoginInfoDAO("admin1", "admin1@email.com", BCryptPasswordEncoder().encode("password")),
                        "Admin Francisco", "https://images.unsplash.com/photo-1506919258185-6078bba55d2a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=60", 912345778, "admin address")
                adminRepository.save(admin2)

                val admin3 = AdminDAO(0L,
                        LoginInfoDAO("admin3", "admin2@email.com", BCryptPasswordEncoder().encode("password")),
                        "Admin Sharapova", "https://images.unsplash.com/photo-1514448553123-ddc6ee76fd52?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=60", 922345778, "admin address")
                adminRepository.save(admin3)
                
                val client1 = ClientDAO("gomesnuno",
                        LoginInfoDAO("gomesnuno", "nuno@gmail.com", BCryptPasswordEncoder().encode("password")),
                        "nuno", "https://ucc.edu.gh/sites/default/files/default_images/img_avatar.png",
                        961234567, "address", false, emptyList())
                val client2 = ClientDAO("bexigamariana",
                        LoginInfoDAO("bexigamariana", "mariana@gmail.com", BCryptPasswordEncoder().encode("password")),
                        "mariana", "https://www.pingyuepanlab.com/wp-content/uploads/2018/09/avtar.png",
                        961234568, "address", false, emptyList())
                val client3 = ClientDAO("becomicael",
                        LoginInfoDAO("becomicael", "micael@gmail.com", BCryptPasswordEncoder().encode("password")),
                        "micael", "http://material.joshadmin.com/assets/images/authors/avatar3.png",
                        961234569, "address", false, emptyList())
                clientRepository.saveAll(listOf(client1, client2, client3))

                val pet1 = PetDAO(0L, 123L, "Dog", 10, "https://pixel.nymag.com/imgs/fashion/daily/2019/06/18/18-puppy-dog-eyes.w700.h700.jpg",
                        client1, emptyList(), "big dog", "Healthy",
                        "medical record", false)
                val pet2 = PetDAO(0L, 321L, "Cat", 2, "https://webcomicms.net/sites/default/files/clipart/129542/black-cat-129542-1431241.jpg", client2,
                        emptyList(), "black cat", "Healthy",
                        "medical record", false)
                val pet3 = PetDAO(0L, 122L, "Dog", 13, "https://i1.wp.com/omeuanimal.com/wp-content/uploads/2018/02/Podengo-Portugu%C3%AAs-2.jpg?resize=696%2C464&ssl=1",
                        client3, emptyList(), "big dog", "Healthy",
                        "medical record", false)
                petRepository.saveAll(listOf(pet1, pet2, pet3))

                client1.pets = listOf(pet1)
                client2.pets = listOf(pet2)
                client3.pets = listOf(pet3)
                clientRepository.saveAll(listOf(client1, client2, client3))

                val vet1 = VetDAO(0L,
                        LoginInfoDAO("vet1", "vet1@gmail.com", BCryptPasswordEncoder().encode("password")),
                        "Veterinarian Isabel", "https://s.yimg.com/ny/api/res/1.2/oXQDQwdb1xOhKn6ML.zJRw--~A/YXBwaWQ9aGlnaGxhbmRlcjtzbT0xO3c9ODAw/http://media.zenfs.com/en/homerun/feed_manager_auto_publish_494/a7b241ce1b6d8032ee149381033005d5", 919650923,
                        "Rua da Isabel", false, emptyList())
                vetRepository.save(vet1)

                val shift1 = ShiftDAO(0L, vet1,
                        Date(2019, 11, 27, 16, 0),
                        Date(2018, 11, 27, 20, 0),
                        emptyList())
                shiftRepository.save(shift1)

                vet1.shifts = listOf(shift1)
                vetRepository.save(vet1)

                val apt1 = AppointmentDAO(0L, pet1, vet1, shift1,
                        Date(2019, 11, 27, 16, 0),
                        Date(2019, 11, 27, 17, 0),
                        "consulta de rotina",
                         2, false
                )
                aptsRepository.save(apt1)

                pet1.appointments = listOf(apt1)
                petRepository.saveAll(listOf(pet1, pet2))

                shift1.appointments = listOf(apt1)
                shiftRepository.save(shift1)
            }
}

fun main(args: Array<String>) {
    runApplication<VetClinicApplication>(*args)
}
