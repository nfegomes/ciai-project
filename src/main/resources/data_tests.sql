DROP TABLE IF EXISTS Appointment;
DROP TABLE IF EXISTS Schedule;
DROP TABLE IF EXISTS Pet;
DROP TABLE IF EXISTS Employee;
DROP TABLE IF EXISTS Client;
DROP TABLE IF EXISTS User;

CREATE TABLE Pet (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(24) REFERENCES Client(username),
  species VARCHAR(30),
  age INT,
  ownerId BIGINT,
  appointments VARCHAR(250),
  physicalDescription VARCHAR(200),
  healthStatus VARCHAR(100)
);

CREATE TABLE User (
  username VARCHAR(24) PRIMARY KEY,
  password VARCHAR(24),
  name VARCHAR(48),
  email VARCHAR(320),
  cellphone INT,
  address VARCHAR(250)
);

CREATE TABLE Client (
  username VARCHAR(24) REFERENCES User(username) PRIMARY KEY,
  pictureID INT
);

CREATE TABLE Employee (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(24) REFERENCES User(username) UNIQUE,
  type ENUM('VET', 'ADMIN'),
  pictureID INT NOT NULL
);

CREATE TABLE Schedule(
  startDateTime TIMESTAMP PRIMARY KEY,
  endDateTime TIMESTAMP PRIMARY KEY,
  vetID BIGINT REFERENCES Employee(id) PRIMARY KEY
);

CREATE TABLE Appointment (
  date DATE,
  startingTime TIME,
  endingTime TIME,
  state ENUM('ACCEPTED', 'DENIED', 'COMPLETED', 'PENDING'),
  justification VARCHAR(250),
  startDateTimeSchedule TIMESTAMP REFERENCES Schedule(startDateTime),
  endDateTimeSchedule TIMESTAMP REFERENCES Schedule(endDateTime),
  vetID BIGINT REFERENCES Schedule(vetID),
  petID BIGINT REFERENCES Pet(id),
  client VARCHAR(24) REFERENCES Client(username),
  description VARCHAR(320)
);



--INSERT INTO Pet (id, species, age, ownerId, appointments, physicalDescription, healthStatus) VALUES
--    (1, "dog", 10, 1, "lots of appointments", "dog description", "sick"),
--    (2, "dog", 10, 1, "lots of appointments", "dog description", "sick");